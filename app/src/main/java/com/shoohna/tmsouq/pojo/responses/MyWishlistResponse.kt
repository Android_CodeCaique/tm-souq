package com.shoohna.tmsouq.pojo.responses

data class MyWishlistResponse(
    val data: List<MyWishlistData>,
    val message: String,
    val status: Int
)

data class MyWishlistData(
    val colors: List<MyWishlistColor>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<MyWishlistImage>,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price_product: String,
    val quantity: Any,
    val rate: String,
    val sizes: List<MyWishlistSize>
)

data class MyWishlistColor(
    val color_code: String,
    val id: Int
)

data class MyWishlistImage(
    val id: Int,
    val image: String
)

data class MyWishlistSize(
    val id: Int,
    val name: String
)