package com.shoohna.tmsouq.pojo.model

import com.google.gson.annotations.SerializedName

data class ProductSelectionCart (
    @SerializedName("product_id")
    val product_id: Int,
    @SerializedName("color_id")
    val color_id: Int,
    @SerializedName("size_id")
    val size_id: Int,
    @SerializedName("quantity")
    val quantity: Int
)