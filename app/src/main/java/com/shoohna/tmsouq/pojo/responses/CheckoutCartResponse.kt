package com.shoohna.tmsouq.pojo.responses

data class CheckoutCartResponse(
    val data: CheckoutCartData,
    val message: String,
    val status: Boolean
)

data class CheckoutCartData(
    val address: Any,
    val address_2: Any,
    val email: Any,
    val id: Int,
    val name: Any,
    val payment_method: Any,
    val phone: Any,
    val products: List<CheckoutCartProduct>,
    val rate: Any,
    val report: Any,
    val shipping_price: Int,
    val status: Int,
    val total_price: Int,
    val user: CheckoutCartUser
)

data class CheckoutCartProduct(
    val color: CheckoutCartColor,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price: Int,
    val quantity: Int,
    val rate: Any,
    val size: CheckoutCartSize
)

data class CheckoutCartUser(
    val email: String,
    val frist_name: String,
    val id: Int,
    val image: String,
    val lang: Any,
    val last_name: String,
    val lat: String,
    val lng: String,
    val message: Int,
    val notification: Int,
    val phone: String,
    val social: String,
    val status: String
)

data class CheckoutCartColor(
    val color_code: String,
    val id: Int
)

data class CheckoutCartSize(
    val id: Int,
    val name: String
)