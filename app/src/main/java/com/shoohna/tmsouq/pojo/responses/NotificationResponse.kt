package com.shoohna.tmsouq.pojo.responses

data class NotificationResponse(
    val data: List<NotificationData>,
    val message: String,
    val status: Int
)

data class NotificationData(
    val desc: String,
    val id: Int,
    val image: String,
    val name: String
)