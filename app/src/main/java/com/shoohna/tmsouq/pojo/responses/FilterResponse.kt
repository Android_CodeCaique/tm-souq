package com.shoohna.tmsouq.pojo.responses

data class FilterResponse(
    val data: List<FilteredData>,
    val message: String,
    val status: Int
)

data class FilteredData(
    val colors: List<Any>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<Any>,
    val is_favorite: Boolean,
    val is_offer: Int,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price_product: String,
    val quantity: Any,
    val rate: String,
    val sizes: List<Any>
)