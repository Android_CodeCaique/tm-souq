package com.shoohna.tmsouq.pojo.responses

data class ProductCategoryResponse (val data: List<Project1>, val message: String, val status: Int)

data class Project1(
    val colors: List<Color>,
    val desc: String,
    val distance: String,
    val id: Int,
    val image: String,
    val images: List<Image>,
    val is_favorite: Boolean,
    val is_offer: Int,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price_product: String,
    val price_after_offer: String,
    val quantity: Any,
    val rate: Int,
    val sizes: List<Size>
)
