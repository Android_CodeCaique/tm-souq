package com.shoohna.tmsouq.pojo.request

import com.shoohna.tmsouq.pojo.responses.RegisterResponse

data class LoginRequest(val message: String, val status: Boolean, val token: String, val user: RegisterResponse)

