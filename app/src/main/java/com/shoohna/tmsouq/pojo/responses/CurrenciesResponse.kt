package com.shoohna.tmsouq.pojo.responses

data class CurrenciesResponse(
    val data: List<CurrenciesData>,
    val message: String,
    val status: Int
)

data class CurrenciesData(
    val id: Int,
    val name:String
)