package com.shoohna.tmsouq.pojo.responses

data class CheckoutShippingResponse(
    val data: CheckoutShippingData,
    val message: String,
    val status: Int
)

data class CheckoutShippingData(
    val address: Any,
    val email: Any,
    val id: Int,
    val name: Any,
    val payment_method: Any,
    val phone: Any,
    val products: List<CheckoutShippingProduct>,
    val rate: Any,
    val report: Any,
    val shipping_price: Int,
    val status: Int,
    val total_price: Int,
    val user: CheckoutShippingUser
)

data class CheckoutShippingProduct(
    val color: CheckoutShippingColor,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price: String,
    val quantity: Int,
    val rate: String,
    val size: CheckoutShippingSize
)

data class CheckoutShippingUser(
    val code: Any,
    val created_at: String,
    val currency_id: Any,
    val email: String,
    val fire_base_token: Any,
    val frist_name: String,
    val id: Int,
    val image: Any,
    val lang: Any,
    val last_name: String,
    val lat: String,
    val lng: String,
    val message: Any,
    val notification: Any,
    val phone: String,
    val shop_id: String,
    val social: String,
    val status: String,
    val updated_at: String
)

data class CheckoutShippingColor(
    val color_code: String,
    val id: Int
)

data class CheckoutShippingSize(
    val id: Int,
    val name: String
)