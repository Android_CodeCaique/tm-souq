package com.shoohna.tmsouq.pojo.responses

data class AboutUsResponse2(
    val data: List<AboutUsData2>,
    val message: String,
    val status: Int
)

data class AboutUsData2(
    val desc: String,
    val id: Int,
    val title: String
)