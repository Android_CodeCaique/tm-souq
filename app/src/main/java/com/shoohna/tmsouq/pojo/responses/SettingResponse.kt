package com.shoohna.tmsouq.pojo.responses

data class SettingResponse(
    val data: SettingData,
    val message: String,
    val status: Int
)

data class SettingData(
    val currency_id: Int,
    val email: String,
    val fire_base_token: Any,
    val frist_name: String,
    val id: Int,
    val image: String,
    val lang: String,
    val last_name: String,
    val lat: Any,
    val lng: Any,
    val message: Int,
    val notification: Int,
    val phone: String,
    val social: Int,
    val status: Int,
    val token: Any
)