package com.shoohna.tmsouq.pojo.responses

data class AboutUsResponse(
    val `data`: AboutUsData,
    val message: String,
    val status: Int
)

data class AboutUsData(
    val about: String,
    val address: String,
    val email: String,
    val facebook: String,
    val id: Int,
    val instagram: String,
    val logo: String,
    val name: String,
    val phone: String,
    val snap: String,
    val twitter: String
)