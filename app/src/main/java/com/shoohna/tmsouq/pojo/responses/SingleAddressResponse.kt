package com.shoohna.tmsouq.pojo.responses

data class SingleAddressResponse(
    val data: SingleAddressData,
    val message: String,
    val status: Int
)

data class SingleAddressData(
    val address: String,
    val id: Int,
    val lat: Double,
    val lng: Double,
    val name: String
)