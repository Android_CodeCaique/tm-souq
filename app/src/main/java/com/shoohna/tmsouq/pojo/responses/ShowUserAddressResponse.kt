package com.shoohna.tmsouq.pojo.responses

data class ShowUserAddressResponse(
    val data: List<UserAddressData>,
    val message: String,
    val status: Int
)

data class UserAddressData(
    val address: String,
    val id: Int,
    val lat: Double,
    val lng: Double,
    val name:String,
    var isChecked:Boolean = false

)