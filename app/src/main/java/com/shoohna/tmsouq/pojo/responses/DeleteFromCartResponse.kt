package com.shoohna.tmsouq.pojo.responses

data class DeleteFromCartResponse(
    val data: Any,
    val message: String,
    val status: Boolean
)