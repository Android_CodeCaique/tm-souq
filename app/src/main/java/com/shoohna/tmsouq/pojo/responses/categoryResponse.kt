package com.shoohna.tmsouq.pojo.responses

data class categoryResponse (val data: List<Category>, val message: String, val status: Int)

data class Category(
        val id : Int,
        val name : String,
        val desc : String,
        val image : String,
        var check : Boolean = false,
        var isChecked:Boolean = false

)
