package com.shoohna.tmsouq.pojo.responses

data class MyOrdersResponse(
    val data: List<MyOrdersData>,
    val message: String,
    val status: Int
)

data class MyOrdersData(
    val address: MyOrdersAddress,
    val date: String,
    val id: Int,
    val payment_method: String,
    val products: List<MyOrdersProduct>,
    val rate: Any,
    val report: String,
    val shipping_price: Int,
    val status: Int,
    val total_price: Int
)

data class MyOrdersAddress(
    val address: String,
    val id: Int,
    val lat: Int,
    val lng: Int,
    val name: String
)

data class MyOrdersProduct(
    val color: MyOrdersColor,
    val desc: String,
    val id: Int,
    val image: String,
    val is_favorite: Boolean,
    val is_offer: String,
    val lat: Double,
    val lng: Double,
    val name: String,
    val offer_amount: String,
    val price: String,
    val quantity: Int,
    val rate: String,
    val size: MyOrdersSize
)

data class MyOrdersColor(
    val color_code: String,
    val id: Int
)

data class MyOrdersSize(
    val id: Int,
    val name: String
)