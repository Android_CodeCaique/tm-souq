package com.shoohna.tmsouq.pojo.responses

data class AddToCartResponse(
    val data: String,
    val message: String,
    val status: Boolean
)

