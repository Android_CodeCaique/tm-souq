package com.shoohna.tmsouq.pojo.responses

data class MakeDiscountResponse(
    val data: MakeDiscountData,
    val message: String,
    val status: Int
)

data class MakeDiscountData(
    val code_id: Int,
    val total_price: Double
)