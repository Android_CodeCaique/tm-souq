package com.shoohna.tmsouq.ui.home.ui.aboutUs

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.tmsouq.databinding.AboutUsItemRowBinding
import com.shoohna.tmsouq.databinding.ShippingInformationItemRowBinding
import com.shoohna.tmsouq.pojo.responses.AboutUsData2

class AboutUsRecyclerAdapter (private var dataList: LiveData<List<AboutUsData2>>, private val context: Context?) : RecyclerView.Adapter<AboutUsRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            AboutUsItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])


    }


    class ViewHolder(private var binding: AboutUsItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: AboutUsData2) {
            binding.model = item
            binding.executePendingBindings()

        }

    }
}