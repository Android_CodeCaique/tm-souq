package com.shoohna.e_commercehappytimes.ui.home.ui.productDetails

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.tmsouq.databinding.ProductDetailsRowBinding
import com.shoohna.tmsouq.pojo.responses.ProductImage
import com.squareup.picasso.Picasso

class ProductDetailsRecyclerViewAdapter (private var dataList: LiveData<List<ProductImage>>, private val context: Context?,var image:ImageView) : RecyclerView.Adapter<ProductDetailsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProductDetailsRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position],image)
    }

    class ViewHolder(private var binding: ProductDetailsRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductImage , image:ImageView) {
            binding.model = item
            binding.executePendingBindings()
//            Picasso.get().load(item.image).into(image)

            binding.productImgId.setOnClickListener {
                Log.i("productImg","Clicked")
                Picasso.get().load(item.image).into(image)
            }
        }

    }

}