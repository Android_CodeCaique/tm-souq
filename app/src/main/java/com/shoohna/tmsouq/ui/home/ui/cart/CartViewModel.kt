package com.shoohna.tmsouq.ui.home.ui.cart

import ApiClient.sharedHelper
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.pojo.model.ProductEntity
import com.shoohna.tmsouq.pojo.model.ProductSelectionCart
import com.shoohna.tmsouq.pojo.model.products
import com.shoohna.tmsouq.util.base.AppDatabase
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.HttpException


class CartViewModel(private val serviceGeneral: Home) : BaseViewModel() {
    var myCartList: MutableLiveData<List<ProductEntity>> = MutableLiveData<List<ProductEntity>>()
//    val myCartStringList = ArrayList<ProductEntity>()
    var baseFragment: BaseFragment = BaseFragment()
//    var currentQuantity = MutableLiveData<Int>(0)
    var gson:Gson = Gson()

    var loader = MutableLiveData<Boolean>(false)

    var noDataTxt = MutableLiveData<Boolean>(false)
    lateinit var list : List<ProductEntity>
    fun loadDataFromRoom(context: Context)
    {
//        checkEmptyRoomData(context)

        try {
            loader.value = true

            val db = Room.databaseBuilder(context, AppDatabase::class.java, "product_db").build()
        CoroutineScope(Dispatchers.IO).launch {
            myCartList.postValue(db.productDao().getAllProducts())
//            CoroutineScope(Dispatchers.Main).launch {  }
        }
            loader.value = false

        }catch (e:Exception)
        {
            CoroutineScope(Dispatchers.Main).launch { Toast.makeText(context,"Exception Load ${e.message.toString()}",Toast.LENGTH_SHORT).show() }
        }
    }

//    fun checkEmptyRoomData(context: Context)
//    {
//
//        val db = Room.databaseBuilder(context, AppDatabase::class.java, "product_db").build()
//        CoroutineScope(Dispatchers.IO).launch {
//            list  = db.productDao().getAllProducts()
//            CoroutineScope(Dispatchers.Main).launch {
//                noDataTxt.value = list.isEmpty()
//            }
//        }
////        noDataTxt.value = myCartList.value?.isEmpty()!!
//    }



    fun deleteFromCartRoom(context: Context , id:Int)
    {
        try {
            loader.value = true

            val db = Room.databaseBuilder(context, AppDatabase::class.java, "product_db").build()

            CoroutineScope(Dispatchers.IO).async {
                db.productDao().deleteProductById(id)
                withContext(Dispatchers.Main){
                    Toast.makeText(context,context.getString(R.string.dataDeleted),Toast.LENGTH_SHORT).show()
                    loader.value = false
                    loadDataFromRoom(context)

                }
//                myCartStringList.clear()

            }

        }catch (e:Exception)
        {
            CoroutineScope(Dispatchers.Main).launch { Toast.makeText(context,"Exception Delete ${e.message.toString()}",Toast.LENGTH_SHORT).show() }

        }
    }



    fun sendCartToServer(view:View) {


        if (sharedHelper.getKey(view.rootView.context, Constants.getToken())?.isEmpty()!!) {
            baseFragment.showAlert(view.rootView.context)
        } else {

            try {
                val db = Room.databaseBuilder(
                    view.rootView.context,
                    AppDatabase::class.java,
                    "product_db"
                ).build()

                Log.i("Token", sharedHelper.getKey(view.rootView.context, Constants.getToken()))

                CoroutineScope(Dispatchers.IO).async {

                    val productsOfflineList: List<ProductEntity> = db.productDao().getAllProducts()
                    val productOnlineList: MutableList<ProductSelectionCart> =
                        mutableListOf<ProductSelectionCart>()

                    for (i in productsOfflineList) {
                        val product: ProductSelectionCart =
                            ProductSelectionCart(i.product_id, i.color_id, i.size_id, i.quantity)
                        productOnlineList.add(product)
                    }
                    if (productOnlineList.size > 0) {
                        CoroutineScope(Dispatchers.Main).launch { loader.value = true }

                        Log.i("inside if ", "true")
                        val pro: products = products(productOnlineList)

                        val json: String = gson.toJson(pro).toString()

                        val body: RequestBody =
                            RequestBody.create(
                                MediaType.parse("application/json; charset=utf-8"),
                                json
                            )


                        val sharedHelper : SharedHelper = SharedHelper()
                        val lang : String? = sharedHelper.getKey(view!!.context , "MyLang" )

                        CoroutineScope(Dispatchers.IO).async {
                            runCatching {
                                serviceGeneral.updateCart(
                                    lang.toString() ,
                                    "Bearer ${sharedHelper.getKey(view.context, Constants.getToken())}",
                                    body
                                )
                            }.onSuccess {
                                withContext(Dispatchers.Main) {
                                    try {

                                        if (it.isSuccessful) {
                                            if (it.body()!!.status == 1) {
                                                Log.i(
                                                    "ResponseSuccess",
                                                    it.body()!!.data.toString()
                                                )
                                                Toast.makeText(
                                                    view.rootView.context,
                                                    it.body()!!.message,
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                                view.let {
                                                    Navigation.findNavController(it)
                                                        .navigate(R.id.action_cartFragment_to_checkoutProcessFragment)
                                                }
                                                sharedHelper.putKey(
                                                    view.context,
                                                    "TOTAL",
                                                    it.body()!!.data.products_price.toString()
                                                )
                                                sharedHelper.putKey(
                                                    view.context,
                                                    "SHIPPING",
                                                    it.body()!!.data.shipping_price.toString()
                                                )

                                                loader.value = false

                                            } else if (it.body()!!.status == 2) {
                                                Snackbar.make(
                                                    view,
                                                    it.body()?.message!!,
                                                    Snackbar.LENGTH_SHORT
                                                ).show()
                                                loader.value = false

                                            } else if (it.body()!!.status == 0) {
                                                Snackbar.make(
                                                    view,
                                                    "${it.body()!!.message}",
                                                    Snackbar.LENGTH_SHORT
                                                ).show()
                                                loader.value = false
                                            } else {

                                            }
                                        } else {

                                        }

                                    } catch (e: HttpException) {
                                        Snackbar.make(
                                            view,
                                            "Exception ${e.message}",
                                            Snackbar.LENGTH_SHORT
                                        )
                                            .show()
                                        loader.value = false
                                    } catch (e: Throwable) {
                                        Log.d("ERROR", "ERRORE" + e.message)
                                        //  Snackbar.make(v, "Ooops: Something else went wrong", Snackbar.LENGTH_SHORT).show();
                                        loader.value = false
                                    }
                                }
                            }.onFailure {
                                Snackbar.make(view, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                            }
                        }



//                        val service = ApiClient.makeRetrofitServiceHome()


//                        val response = service.updateCart(
//                            lang.toString() ,
//                            "Bearer ${sharedHelper.getKey(view.context, Constants.getToken())}",
//                            body
//                        )
//                        withContext(Dispatchers.Main) {
//
//                            Log.d("UPDATECART", "UPDATECART" + response)
//
//                        }

                    } else {
                        Log.i("inside if ", "false")
                        CoroutineScope(Dispatchers.Main).launch {
                            Toast.makeText(
                                view.rootView.context,
                                view.rootView.context.resources.getString(R.string.noDataInCart),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }


            } catch (e: java.lang.Exception) {
            }
        }
    }
}
//    fun loadData(context: Context)
//    {
//        baseFragment.showProgressDialog(context,context.getString(R.string.loading),context.resources.getString(R.string.pleaseWaitUntilLoading),false)
//
//        val service = ApiClient.makeRetrofitServiceHome()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.getMyCart("Bearer ${sharedHelper.getKey(context, Constants.getToken())}")
//            withContext(Dispatchers.Main) {
//                try{
//                    if(response.isSuccessful && response.body()?.status == 1){
//                        Log.i("response",response.body()!!.message)
//                        myCartStringList.addAll(response.body()!!.data)
//                        myCartList!!.postValue(myCartStringList)
//                        baseFragment.dismissProgressDialog()
//
//                    }
//                    else {
//                        Log.i("loadData1",response.message().toString())
//                        baseFragment.dismissProgressDialog()
//                    }
//                }catch (e:Exception){
//                    Log.i("loadData2",e.message.toString())
//                    baseFragment.dismissProgressDialog()
//
//                }
//



//
//    fun deleteFromCart(context: Context , id:Int)
//    {
//        baseFragment.showProgressDialog(context,context.getString(R.string.loading),context.resources.getString(R.string.pleaseWaitUntilLoading),false)
//
//        val service = ApiClient.makeRetrofitServiceHome()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.deleteFromCart(id,"Bearer ${sharedHelper.getKey(context, Constants.getToken())}")
//            withContext(Dispatchers.Main) {
//                try{
//                    if(response.isSuccessful && response.body()?.status == true){
//                        Log.i("response",response.body()!!.message)
//                        Toast.makeText(context,context.resources.getString(R.string.itemDeletedFromCart),Toast.LENGTH_SHORT).show()
//                        baseFragment.dismissProgressDialog()
//                        myCartStringList.clear()
//                        loadDataFromRoom(context)
//                    }
//                    else {
//                        Log.i("loadData1",response.message().toString())
//                        baseFragment.dismissProgressDialog()
//                        Toast.makeText(context,response.message().toString(),Toast.LENGTH_SHORT).show()
//
//                    }
//                }catch (e:Exception){
//                    Log.i("loadData2",e.message.toString())
//                    baseFragment.dismissProgressDialog()
//                    Toast.makeText(context,"Exception ${e.message.toString()}",Toast.LENGTH_SHORT).show()
//
//                }
//
//            }
//        }
//    }


//    internal fun getFilterList(context: Context): MutableLiveData<List<ProductEntity>> {
//        if (myCartList == null) {
//            myCartList = MutableLiveData()
//            loadDataFromRoom(context)
//        }
//        return myCartList as MutableLiveData<List<ProductEntity>>
//    }


//}