package com.shoohna.e_commercehappytimes.ui.home.ui.home

import ApiClient.sharedHelper
import android.content.Context
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.pojo.responses.*
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class HomeViewModel(private val serviceGeneral: Home) : BaseViewModel() {

    private var _tintTopRated = MutableLiveData<Boolean>(false)
    val tintTopRated: LiveData<Boolean> = _tintTopRated
    
    private var _tintNearestMe = MutableLiveData<Boolean>(false)
    val tintNearestMe: LiveData<Boolean> = _tintNearestMe

    private var _tintCost = MutableLiveData<Boolean>(false)
    val tintCost: LiveData<Boolean> = _tintCost


    fun allowTintTop(view: View)
    {
        _tintTopRated.value = true
        _tintNearestMe.value = false
        _tintCost.value = false
    }

    fun allowTintNearestMe(view: View)
    {
        if(CheckGpsStatus(view)) {
            _tintTopRated.value = false
            _tintNearestMe.value = true
            _tintCost.value = false
            getCurrentLocation(view.rootView.context)
        }
    }

    fun allowTintCost(view: View)
    {
        _tintTopRated.value = false
        _tintNearestMe.value = false
        _tintCost.value = true
    }

    fun resetTint(context: Context)
    {
        _tintTopRated.value = false
        _tintNearestMe.value = false
        _tintCost.value = false
    }

    var bestSalesList: MutableLiveData<List<BestSeller>> = MutableLiveData()
    var categoryList : MutableLiveData<List<Category>> = MutableLiveData()
    var sliderList : MutableLiveData<List<sliders>> = MutableLiveData()
    var productsList : MutableLiveData<List<Project1>> = MutableLiveData()

    var category = MutableLiveData<Boolean>(false)

    var offersList: MutableLiveData<List<Offer>> = MutableLiveData()
    var highRatedList: MutableLiveData<List<HighRated>> = MutableLiveData()

    var baseFragment:BaseFragment = BaseFragment()

    var range_from = MutableLiveData<Int>(0)
    var range_to = MutableLiveData<Int>(0)
    var filterCatId = MutableLiveData<Int>(0)

    var loader = MutableLiveData<Boolean>(false)

    var dismissSheet = MutableLiveData<Boolean>(false)


    var locationManager: LocationManager? = null
    var GpsStatus = false
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var geocoder: Geocoder
    var lat = MutableLiveData<Double>(0.0)
    var lon = MutableLiveData<Double>(0.0)

    var productId = MutableLiveData<Int>(0)


    fun loadData(context:Context) {
//        baseFragment.showProgressDialog(context,context.getString(R.string.loading),context.resources.getString(R.string.pleaseWaitUntilLoading),false)
        loader.value = true
        try {
            Log.i("SharedToken",sharedHelper.getKey(context, Constants.getToken()))

            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )
//        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).async {
            runCatching { serviceGeneral.getHomeData(lang.toString()) }
                .onSuccess {
//                delay(3000)
//            if(!response.isSuccessful)
//                this.cancel()
//            else {
                    withContext(Dispatchers.Main) {

                        if (it.isSuccessful && it.body()?.status == 1) {
                            Log.i("response", it.body()!!.message)

                            bestSalesList.value = it.body()!!.data.best_seller
                            offersList.value = it.body()!!.data.Offers
                            highRatedList.value = it.body()!!.data.high_rated
                            sliderList.value = it.body()!!.data.sliders
//                        baseFragment.dismissProgressDialog()
                            loader.value = false


                        } else {
                            Log.i("loadData1", it.message().toString())
//                        baseFragment.dismissProgressDialog()
                            loader.value = false

                        }


                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
//            }

        }
        } catch (e: Exception) {
            Log.i("loadData2", e.message.toString())
//                    baseFragment.dismissProgressDialog()
            loader.value = false


        }

    }


    fun applyFilter(v:View) {
        if (!_tintTopRated.value!! && !_tintNearestMe.value!! && !_tintCost.value!!)
        Toast.makeText(v.context, v.resources.getString(R.string.pleaseSelectFilterItem), Toast.LENGTH_SHORT).show()
//        else if (filterCatId.value!! == 0){
//            Toast.makeText(v.context, v.resources.getString(R.string.pleaseSelectCategoireItem), Toast.LENGTH_SHORT).show()
//        }
        else {
//            bestSalesStringList.clear()
//            offersStringList.clear()
//            highRatedStringList.clear()
            Log.i("Range From",range_from.value.toString())
            Log.i("Range To",range_to.value.toString())
//            baseFragment.showProgressDialog(v.rootView.context,v.rootView.context.getString(R.string.loading),v.rootView.context.resources.getString(R.string.pleaseWaitUntilLoading),false)
            loader.value = true
            try {


                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
//                val service = ApiClient.makeRetrofitServiceHome()
                CoroutineScope(Dispatchers.IO).async {
                    runCatching {
                    serviceGeneral.filterData(
                        lang.toString() ,
                        range_from.value?.toInt()!!,
                        range_to.value?.toInt()!!,
                        getFilterItem(),
                       lat.value!!,
                        lon.value!!,
                        filterCatId.value?.toInt()!!
                    )}.onSuccess {
                        withContext(Dispatchers.Main) {
                            if (it.isSuccessful) {
                                Log.i("filterData", "Success")
                                Log.i("response", it.body()!!.data.toString())
//                            Toast.makeText(v.context, response.body()!!.message, Toast.LENGTH_SHORT)
//                                .show()

                                if (it.body()!!.data.best_seller.isEmpty()
                                    && it.body()!!.data.Offers.isEmpty()
                                    && it.body()!!.data.high_rated.isEmpty()
                                ) {
                                    Toast.makeText(
                                        v.rootView.context,
                                        v.rootView.context.resources.getString(R.string.noProductsAfterFilter),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    loader.value = false
                                } else {
                                    bestSalesList.postValue(it.body()!!.data.best_seller)
                                    offersList.postValue(it.body()!!.data.Offers)
                                    highRatedList.postValue(it.body()!!.data.high_rated)
                                    loader.value = false
                                    dismissSheet.value = true
                                }
//                            Log.i("bestSaled", bestSalesList.value.toString())

                            } else {
                                Log.i("filterData", "Fail")
                                Toast.makeText(v.context, it.message(), Toast.LENGTH_SHORT)
                                    .show()
                                loader.value = false

                            }
                        }
                    }.onFailure {
                        Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                    }

                }
            }catch (e:java.lang.Exception)
            {
                Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();

            }
        }
    }

    private fun getFilterItem() : Int
    {
        return when {
            _tintTopRated.value == true -> 1
            _tintNearestMe.value == true -> 2
            else -> 3
        }
    }

    fun getCurrentLocation(context: Context)
    {

            try {
                fusedLocationClient =
                    LocationServices.getFusedLocationProviderClient(context)
                Log.i("Location1", "BeforeOnSuccess")

                fusedLocationClient.lastLocation
                    .addOnSuccessListener { location: Location? ->
                        Log.i("Location1", "AfterOnSuccess")
                        Log.i("Location1", location?.latitude.toString())
                        lat.value = location?.latitude
                        Log.i("Location1", location?.longitude.toString())
                        lon.value = location?.longitude
                        Log.i("Location1", geocoder.getFromLocation(location?.latitude!!, location.longitude, 1)[0].getAddressLine(0))
//                        addressFromGeoCoder.value = geocoder.getFromLocation(lat.value!!, lon.value!!, 1)[0].getAddressLine(0)
//                        Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.currentAddressSelected),Toast.LENGTH_SHORT).show()
//                        userCurrentAddressCheck.value = true
                    }
            }catch (e:Exception) {
                Toast.makeText(context, e.message.toString(), Toast.LENGTH_SHORT).show()
            }


    }

    private fun CheckGpsStatus(v:View) : Boolean {
        locationManager = v.rootView.context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        assert(locationManager != null)
        GpsStatus = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        return if (!GpsStatus) {
            Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.GPSDisabled), Toast.LENGTH_SHORT).show()
            false
        } else
            true
    }



    fun loadCategoryData(context:Context){
        loader.value = true

        Log.d("HERE In Category" ,"AMMMM")
        try {
            Log.i("SharedToken",sharedHelper.getKey(context, Constants.getToken()))

            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )
            CoroutineScope(Dispatchers.IO).async {
                runCatching { serviceGeneral.getCategory(lang.toString()) }
                        .onSuccess {
                            withContext(Dispatchers.Main) {

                                if (it.isSuccessful && it.body()?.status == 1) {
                                    Log.i("response", it.body()!!.message)
                                    Log.d("HERE In Category" ,"AMMMM1")

                                    categoryList.value = it.body()!!.data
                                    loader.value = false

                                } else {
                                    Log.i("loadData1", it.message().toString())
                                    loader.value = false

                                }


                            }
                        }.onFailure {
                            loader.value=false
                            Log.d("HHH" ,"HHH")
                            Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                        }
//            }

            }
        } catch (e: Exception) {
            Log.i("loadData2", e.message.toString())
            loader.value = false
        }

    }

    fun loadCategoryProductData(context: Context , categoryId : Int) {
        loader.value = true

        Log.d("HERE In Category" ,"AMMMM222222222222222222222222222222222222222")
        try {
            Log.i("SharedToken",sharedHelper.getKey(context, Constants.getToken()))

            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )
            CoroutineScope(Dispatchers.IO).async {
                runCatching { serviceGeneral.getCategoryProduct(lang.toString() ,categoryId) }
                        .onSuccess {
                            withContext(Dispatchers.Main) {

                                if (it.isSuccessful && it.body()?.status == 1) {
                                    Log.i("response", it.body()!!.message)
                                    Log.d("HERE In Category" ,"AMMMM2222222")
                                    productsList.value = it.body()!!.data
                                    loader.value = false

                                } else {
                                    Log.i("loadData1", it.message().toString())
                                    loader.value = false

                                }


                            }
                        }.onFailure {
                            loader.value=false
                            Log.d("HHH" ,"HHH")
                            Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                        }
//            }

            }
        } catch (e: Exception) {
            Log.i("loadData2", e.message.toString())
            loader.value = false
        }
    }

    fun addToWishlist(context: Context)
    {
        loader.value = true
        try {
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )

//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.addOrDeleteFromMyWishlist(
                        lang.toString(),
                        productId.value?.toInt()!!,
                        "Bearer ${sharedHelper.getKey(context, Constants.getToken())}"
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
                                Toast.makeText(
                                    context,
                                    it.body()!!.message,
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                loader.value = false
//                        if(response.body()!!.message.equals("Product deleted from Whishlist")){
//                            isFavorite.value = false
//                        }else if(response.body()!!.message.equals("Product added in Whishlist")) {
//                            isFavorite.value = true
//                        }
                                loadData(context)

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                                Toast.makeText(context, it.message(), Toast.LENGTH_SHORT)
                                    .show()
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false
                            Toast.makeText(
                                context,
                                "Exception ${e.message.toString()}",
                                Toast.LENGTH_SHORT
                            ).show()

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                }
            }
        }catch (e:Exception)
        {

        }
    }
}