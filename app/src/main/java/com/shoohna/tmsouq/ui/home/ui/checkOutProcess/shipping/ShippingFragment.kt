package com.shoohna.tmsouq.ui.home.ui.checkOutProcess.shipping

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shoohna.tmsouq.databinding.FragmentShippingBinding
import com.shoohna.tmsouq.pojo.responses.UserAddressData
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class ShippingFragment : Fragment() {
    lateinit var binding: FragmentShippingBinding

    var userAddressData= MutableLiveData<List<UserAddressData>>()
    private val shippingViewModel: ShippingViewModel by inject()   // 1



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentShippingBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        binding.vm = shippingViewModel

        shippingViewModel.getUserAddressList(requireActivity()).observe(viewLifecycleOwner, Observer {
            userAddressData.value = it
            binding.shippingRecycvlerViewId.adapter = ShippingRecyclerViewAdapter(userAddressData, context?.applicationContext , shippingViewModel , viewLifecycleOwner , binding )
        })


        return binding.root
    }





}
