package com.shoohna.tmsouq.ui.home

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.pojo.responses.ProductData
import com.shoohna.tmsouq.ui.welcome.WelcomeActivity
import com.shoohna.tmsouq.util.base.AppDatabase
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException

class MainActivityViewModel(private val serviceGeneral: Home) : BaseViewModel() {

    var finalDataList: MutableLiveData<ProductData> = MutableLiveData()
    var productId = MutableLiveData<Int>(0)

    fun loadProductData(context: Context)
    {

        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(context , "MyLang" )

//        val service = ApiClient.makeRetrofitServiceHome()

        CoroutineScope(Dispatchers.IO).async {
            runCatching { serviceGeneral.productDetails(lang.toString() , productId.value!!) }
                .onSuccess {

                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)

                                finalDataList.value = it.body()!!.data


                            } else {
                                Log.i("loadData1", it.message().toString())
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
        }
    }

    fun logout (context: Context)
    {
        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(context , "MyLang" )
        val db = Room.databaseBuilder(context, AppDatabase::class.java, "product_db").build()

        val alert: Dialog? = Dialog(context)
        alert?.setContentView(R.layout.exit_alert_layout)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<Button>(R.id.logOutBtnId)?.setOnClickListener {
            try {
                CoroutineScope(Dispatchers.IO).async {
                    runCatching {
                        serviceGeneral.logoutFunction(lang.toString() , "Bearer ${sharedHelper.getKey(context, Constants.getToken())}")
                    }.onSuccess {
                        withContext(Dispatchers.Main) {
                            try {
                                if (it.isSuccessful) {
                                    if (it.body()!!.status == 1) {
                                        //Do something with response e.g show to the UI.

                                        Toast.makeText(context,it.body()?.message!!,Toast.LENGTH_SHORT).show()
                                        sharedHelper.putKey(context, Constants.getToken(), "")
                                        sharedHelper.putKey(context, "USER_ID", "")
                                        sharedHelper.putKey(context, "USER_PHOTO", "")
                                        sharedHelper.putKey(context, "USER_NAME", "")
                                        sharedHelper.putKey(context, "USER_EMAIL", "")
                                        sharedHelper.putKey(context, "OPEN", "OPEN")

                                        val intent: Intent =
                                            Intent(context, WelcomeActivity::class.java)
                                        context.startActivity(intent)
                                        (context as Activity).finish()
                                        CoroutineScope(Dispatchers.IO).launch {
                                            db.productDao().deleteAllProductDatabase()
                                        }
                                    } else if (it.body()!!.status == 2) {
                                        Toast.makeText(context,it.body()?.message!!,Toast.LENGTH_SHORT).show()

                                    } else if (it.body()!!.status == 0) {
                                        Toast.makeText(context,it.body()?.message!!,Toast.LENGTH_SHORT).show()

                                        //baseFragment.dismissProgressDialog()
                                    } else {
                                    }
                                }

                            } catch (e: HttpException) {

                                Toast.makeText(context,"Exception ${e.message}",Toast.LENGTH_SHORT).show()


                            } catch (e: Throwable) {
                                //   Snackbar.make(v, "Ooops: Something else went wrong ${e.message}", Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    }.onFailure {
                        Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                    }
                }




//            val service = ApiClient.makeRetrofitService()





//                CoroutineScope(Dispatchers.IO).async {
//                    val response = service.logoutFunction(lang.toString() ,
//                        "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}"
//                    )
//                    withContext(Dispatchers.Main) {
//
//                    }
//                }
            }catch (e: java.lang.Exception)
            {
                Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

            }
        }
        alert?.findViewById<Button>(R.id.BackBtnId)?.setOnClickListener {
            alert.dismiss()
        }
        alert?.show()


    }

}