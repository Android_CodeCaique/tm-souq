package com.shoohna.tmsouq.ui.home.ui.home.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.e_commercehappytimes.ui.home.ui.home.HomeViewModel
import com.shoohna.tmsouq.databinding.FragmentHomeBinding
import com.shoohna.tmsouq.databinding.FragmentProductDetailsBinding
import com.shoohna.tmsouq.databinding.ItemCategoryBinding
import com.shoohna.tmsouq.pojo.responses.Category


class CategoriesRecyclerAdapter() : RecyclerView.Adapter<CategoriesRecyclerAdapter.ViewHolder>(){



    private lateinit var dataList: LiveData<List<Category>>
    private var context: Context? = null
    private var mListener: OnItemClickListener? = null
    private lateinit var viewModel:HomeViewModel
    private lateinit var lifecycleOwner: LifecycleOwner
    private lateinit var MainBinding: FragmentHomeBinding
    var makeLoop = MutableLiveData<Boolean>(false)


    constructor(dataList: LiveData<List<Category>>,
                context: Context?,
                viewModel: HomeViewModel,
                lifecycleOwner: LifecycleOwner,
                 MainBinding : FragmentHomeBinding) : this() {
        this.dataList = dataList
        this.context = context
        this.viewModel = viewModel
        this.lifecycleOwner = lifecycleOwner
        this.MainBinding = MainBinding
    }

    fun loopIsChecked(dataList: List<Category>)
    {
        for (i in dataList) {
            i.check = false
        }
        makeLoop.value = false
    }


    class ViewHolder(private var binding: ItemCategoryBinding, private var listener: OnItemClickListener? , var makeLoop:MutableLiveData<Boolean>) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Category , viewModel: HomeViewModel , context: Context? , MainBinding: FragmentHomeBinding) {
            binding.model = item

            binding.executePendingBindings()

            binding.content.setOnClickListener {
                if (listener != null) {
                    val position: Int = this.adapterPosition
                    if (position != -1) {
                        listener!!.onItemClick(position)
                    }
                }
//                binding.content.setTextColor(Color.parseColor("#617590"))
                binding.content.setTypeface(binding.content.typeface , Typeface.BOLD)
                makeLoop.value = true
                item.check = true

                MainBinding.categorieRecyclerViewId.adapter?.notifyDataSetChanged()
            }

//            binding.linearLayoutId.setOnClickListener {
//                Toast.makeText(context,context?.getString(R.string.sizeSelected), Toast.LENGTH_SHORT).show()
//                binding.content.setTextColor(Color.parseColor("#617590"))
//                makeLoop.value = true
//                item.isChecked = true
//                MainBinding.categorieRecyclerViewId.adapter?.notifyDataSetChanged()
//            }
        }

    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.mListener = listener
    }

    interface OnItemClickListener {
        fun onItemClick(i: Int)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false) , this.mListener , makeLoop

        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] , viewModel , context,MainBinding)
        makeLoop.observe(lifecycleOwner, Observer {
            if(it)
            {
                loopIsChecked(dataList.value!!)
            }
        })
    }





}