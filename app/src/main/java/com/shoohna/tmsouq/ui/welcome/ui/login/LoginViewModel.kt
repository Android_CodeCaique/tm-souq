package com.shoohna.tmsouq.ui.welcome.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.auth
import com.shoohna.tmsouq.ui.home.MainActivity
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.util.concurrent.TimeUnit

class LoginViewModel(private val serviceGeneral: auth) : BaseViewModel() {
    var accountOrPhone = MutableLiveData<String>("")
    var password = MutableLiveData<String>("")
    var baseFragment:BaseFragment = BaseFragment()
    var sharedHelper:SharedHelper = SharedHelper()
    var loader = MutableLiveData<Boolean>(false)

    fun login(v:View, emailLayout: TextInputLayout,  passwordLayout: TextInputLayout)
    {

        if(validate(v ,emailLayout,passwordLayout))
            return

        if(accountOrPhone.value?.isNotEmpty()!! && password.value?.isNotEmpty()!!) {
            loader.value = true

            try {
                //Added onNewToken method
                    // Get updated InstanceID token.
                    val token: String? = FirebaseInstanceId.getInstance().token
                    Log.d("TOKEN", "TOKEN : " + token)


                    val sharedHelper : SharedHelper = SharedHelper()
                    val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )

                    CoroutineScope(Dispatchers.IO).async {
                        runCatching {
                            serviceGeneral.loginFunction(lang.toString() , accountOrPhone.value!!, password.value!!, token)
                        }.onSuccess {
                            withContext(Dispatchers.Main) {
                                try {
                                    if (it.isSuccessful) {
                                        if (it.body()!!.status == 1) {
                                            //Do something with response e.g show to the UI.
                                            Log.d("HHHHHH", "HHHH" + it)
                                            Snackbar.make(
                                                v,
                                                it.body()?.message!!,
                                                Snackbar.LENGTH_SHORT
                                            ).show();
                                            //baseFragment.dismissProgressDialog()
                                            accountOrPhone.value = ""
                                            password.value = ""
                                            sharedHelper.putKey(
                                                v.context,
                                                Constants.getToken(),
                                                it.body()!!.data.token
                                            )
                                            Log.i(
                                                "SharedToken",
                                                sharedHelper.getKey(v.context, Constants.getToken())
                                            )
                                            sharedHelper.putKey(
                                                v.context,
                                                "USER_ID",
                                                it.body()!!.data.id.toString()
                                            )
                                            sharedHelper.putKey(
                                                v.context,
                                                "USER_PHOTO",
                                                it.body()!!.data.image.toString()
                                            )
                                            sharedHelper.putKey(
                                                v.context,
                                                "USER_NAME",
                                                it.body()!!.data.frist_name
                                            )
                                            sharedHelper.putKey(
                                                v.context,
                                                "USER_EMAIL",
                                                it.body()!!.data.email
                                            )
                                            sharedHelper.putKey(v.context, "OPEN", "OPEN")

                                            loader.value = false

//                                showDialog(response.body()?.message!! , v)

                                            val intent: Intent =
                                                Intent(v!!.context, MainActivity::class.java)
                                            v!!.context.startActivity(intent)
                                            (v.context as Activity).finish()

//                                    }
                                        } else if (it.body()!!.status == 2) {
                                            Snackbar.make(
                                                v,
                                                it.body()?.message!!,
                                                Snackbar.LENGTH_SHORT
                                            ).show()
                                            loader.value = false
                                        } else if (it.body()!!.status == 0) {
                                            Snackbar.make(
                                                v,
                                                "${it.body()!!.message}",
                                                Snackbar.LENGTH_SHORT
                                            ).show()
                                            loader.value = false
                                            //baseFragment.dismissProgressDialog()
                                        } else {

                                        }
                                    }

                                } catch (e: HttpException) {
                                    Snackbar.make(
                                        v,
                                        "Exception ${e.message}",
                                        Snackbar.LENGTH_SHORT
                                    )
                                        .show();
                                    //baseFragment.dismissProgressDialog()
                                    loader.value = false


                                } catch (e: Throwable) {
                                    Snackbar.make(
                                        v,
                                        "Ooops: Something else went wrong",
                                        Snackbar.LENGTH_SHORT
                                    ).show();
                                    //baseFragment.dismissProgressDialog()
                                    loader.value = false

                                }
                            }
                        }.onFailure {
                            Snackbar.make(
                                v,
                                it.message.toString(),
                                Snackbar.LENGTH_SHORT
                            ).show();
                            //baseFragment.dismissProgressDialog()
                            loader.value = false
                        }


                }
            }catch (e:Exception)
            {
                Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                    .show();
            }
        }
        else
            Snackbar.make(v, v.resources.getString(R.string.pleaseInsertAllData), Snackbar.LENGTH_SHORT).show();

    }
    fun validate(v:View, emailLayout:TextInputLayout, passwordLayout:TextInputLayout): Boolean
    {
        var stop = false
        if (checkEmpty(passwordLayout , password.value.toString(), v.resources.getString(R.string.required) ))
        {
            stop = true

        }
        if (checkEmpty(emailLayout , accountOrPhone.value.toString(),v.resources.getString(R.string.required) ))
        {
            stop = true

        }
        else if (checkValidEmail(emailLayout , accountOrPhone.value.toString(), v.resources.getString(R.string.notValidEmail)))
        {
            stop = true

        }


        if (stop)
            return true
        else
            return false
    }
    fun getActivationCode(v: View , phone : String)
    {
        val activity = v!!.context as Activity
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+20"+phone , // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            activity , // Activity (for callback binding)
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                    Log.d("PHONE", "onVerificationCompleted:$credential")
                    //signInWithPhoneAuthCredential(credential)
                }
                override fun onVerificationFailed(e: FirebaseException) {
                    Log.w("PHONE", "onVerificationFailed", e)

                    if (e is FirebaseAuthInvalidCredentialsException) {

                    } else if (e is FirebaseTooManyRequestsException) {

                    }
                }
                override fun onCodeSent(
                    verificationId: String,
                    token: PhoneAuthProvider.ForceResendingToken
                )
                {
                    loader.value = false
                    Log.d("PHONE", "onCodeSent:$verificationId")
                    sharedHelper.putKey(v!!.context , "ActiveCode"  , verificationId)
//                    v?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_loginFragment_to_verifyCodeFragment) }
                }
            })
    }

    fun login( context: Context , email : String?) {
        val token :String? = FirebaseInstanceId.getInstance().token

//        val service = ApiClient.makeRetrofitService()
        loader.value = true

        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(context , "MyLang" )
        CoroutineScope(Dispatchers.IO).async {
            runCatching {
            serviceGeneral.loginSocialFunction(lang.toString() , email , token) }
                .onSuccess {
                    withContext(Dispatchers.Main) {
                        try {

                            Log.i("status Outside", it.body()!!.status.toString())
                            if (it.isSuccessful) {
                                if (it.body()!!.status == 1) {
                                    //Do something with response e.g show to the UI.
                                    Toast.makeText(context,it.body()?.message!!,Toast.LENGTH_SHORT).show()

                                    //baseFragment.dismissProgressDialog()
//                                    Toast.makeText(context,it.message(),Toast.LENGTH_SHORT).show()
                                    loader.value = false

                                    sharedHelper.putKey(
                                        context,
                                        Constants.getToken(),
                                        it.body()!!.data.token
                                    )
                                    Log.i(
                                        "SharedToken",
                                        sharedHelper.getKey(context, Constants.getToken())
                                    )
                                    sharedHelper.putKey(
                                        context,
                                        "USER_ID",
                                        it.body()!!.data.id.toString()
                                    )
                                    sharedHelper.putKey(
                                        context,
                                        "USER_PHOTO",
                                        it.body()!!.data.image.toString()
                                    )
                                    sharedHelper.putKey(
                                        context,
                                        "USER_NAME",
                                        it.body()!!.data.frist_name
                                    )
                                    sharedHelper.putKey(
                                        context,
                                        "USER_EMAIL",
                                        it.body()!!.data.email
                                    )
                                    sharedHelper.putKey(context, "OPEN", "OPEN")

                                    loader.value = false

//                                showDialog(response.body()?.message!! , v)

                                    val intent: Intent =
                                        Intent(context, MainActivity::class.java)
                                     context.startActivity(intent)
                                    (context as Activity).finish()


                                } else if (it.body()!!.status == 2) {

                                    Toast.makeText(context,it.body()?.message!!,Toast.LENGTH_SHORT).show()
                                    loader.value = false

                                } else {

                                    Toast.makeText(context,it.body()?.message!!,Toast.LENGTH_SHORT).show()
                                    loader.value = false

                                    //baseFragment.dismissProgressDialog()
                                }
                            }

                        } catch (e: HttpException) {
                            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()
                            loader.value = false

                            //baseFragment.dismissProgressDialog()

                        } catch (e: Throwable) {

                            Toast.makeText(context,"Ooops: Something else went wrong",Toast.LENGTH_SHORT).show()
                            loader.value = false

                            //baseFragment.dismissProgressDialog()
                        }
                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
        }

    }

}