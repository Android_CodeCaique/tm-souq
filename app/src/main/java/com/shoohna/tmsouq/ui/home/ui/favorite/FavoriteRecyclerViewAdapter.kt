package com.shoohna.tmsouq.ui.home.ui.favorite

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FavoriteViewRowBinding
import com.shoohna.tmsouq.pojo.responses.MyWishlistData
import com.shoohna.tmsouq.ui.home.MainActivity


class FavoriteRecyclerViewAdapter  (private var dataList: LiveData<List<MyWishlistData>>, private val context: Context?,var model: FavoriteViewModel) : RecyclerView.Adapter<FavoriteRecyclerViewAdapter.ViewHolder>() {

//    private var items: List<ProductListModel>? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FavoriteViewRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] , this.model , context)
    }

    fun filterList(filteredList: LiveData<List<MyWishlistData>>) {
        dataList = filteredList
    }

    class ViewHolder(private var binding: FavoriteViewRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MyWishlistData , model: FavoriteViewModel , context: Context?) {
            binding.model = item
            binding.executePendingBindings()

            binding.mainConstraintLayoutId.setOnClickListener {
                val action =
                    FavoriteFragmentDirections.actionFavoriteFragmentToProductDetailsFragment2(
                        item.id, item.image, item.desc,
                        item.price_product
                    )
                Navigation.findNavController(it).navigate(action)
                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }

            binding.addToCartImgId.setOnClickListener {

                    val action =
                        FavoriteFragmentDirections.actionFavoriteFragmentToProductDetailsFragment2(
                            item.id, item.image, item.desc,
                            item.price_product
                        )
                    Navigation.findNavController(it).navigate(action)
                    (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                        View.GONE

            }

            binding.heartImgId.setOnClickListener {
                model.productId.value = item.id
                model.addToWishlist(context!!)
//                binding.heartImgId.background = R.drawable.ic_heart_white
            }




        }

    }
}
