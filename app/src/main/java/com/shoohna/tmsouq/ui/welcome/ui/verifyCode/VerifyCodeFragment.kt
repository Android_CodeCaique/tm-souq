package com.shoohna.tmsouq.ui.welcome.ui.verifyCode

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.shoohna.tmsouq.databinding.FragmentVerifyCodeBinding
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class VerifyCodeFragment : Fragment() {

    lateinit var binding:FragmentVerifyCodeBinding
    private val verifyCodeViewModel: VerifyCodeViewModel by inject()   // 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentVerifyCodeBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
//        val viewModel = ViewModelProvider(this).get(VerifyCodeViewModel::class.java)
        binding.vm = verifyCodeViewModel

        verifyCodeViewModel._otp1.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty())
                binding.editText11.requestFocus()
        })
        verifyCodeViewModel._otp2.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty())
                binding.editText12.requestFocus()
        })
        verifyCodeViewModel._otp3.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty())
                binding.editText13.requestFocus()
        })
        verifyCodeViewModel._otp4.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty())
                binding.editText14.requestFocus()
        })
        verifyCodeViewModel._otp5.observe(viewLifecycleOwner, Observer {
            if(it.isNotEmpty())
                binding.editText15.requestFocus()
        })

//        binding.verifyBtnId.setOnClickListener {
//            view?.let { Navigation.findNavController(it).navigate(R.id.action_verifyCodeFragment_to_changePasswordFragment) }
//        }

        return binding.root

    }

}
