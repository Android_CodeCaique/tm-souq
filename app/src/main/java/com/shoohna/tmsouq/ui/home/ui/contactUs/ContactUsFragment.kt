package com.shoohna.tmsouq.ui.home.ui.contactUs

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.shoohna.tmsouq.databinding.FragmentContactUsBinding
import org.koin.android.ext.android.inject
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class ContactUsFragment : Fragment() {

    lateinit var binding: FragmentContactUsBinding
    private val contactUsViewModel: ContactUsViewModel by inject()   // 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentContactUsBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val viewModel = ViewModelProvider(this).get(ContactUsViewModel::class.java)

        binding.vm = contactUsViewModel

        contactUsViewModel.loadData(binding.root)

        binding.phone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contactUsViewModel.phoneCompany.value))
            requireActivity().startActivity(intent)
        }

        binding.email.setOnClickListener {
            val intent = Intent().apply {
                action = Intent.ACTION_SEND
                data = Uri.parse("mailto:")
                type = "text/plain"
                putExtra(Intent.EXTRA_EMAIL, contactUsViewModel.emailCompany.value.toString())
                putExtra(Intent.EXTRA_SUBJECT, "Subject of Email")
            }
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                intent.setPackage("com.google.android.gm")
                startActivity(intent)
            } else {
                Toast.makeText(requireActivity(),"No app available to send email.",Toast.LENGTH_SHORT).show()
            }
        }

        binding.address.setOnClickListener {
            val uri: String =
                java.lang.String.format(Locale.ENGLISH, "geo:%f,%f (%s)", 30.0613467,31.3521906,"Mountasher Development")
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            requireContext().startActivity(intent)
        }

        binding.facebookIconId.setOnClickListener {
            openBrowser(contactUsViewModel.facebookLink.value!!)
        }

        binding.instagramIconId.setOnClickListener {
            openBrowser(contactUsViewModel.instagramLink.value!!)

        }

        binding.snapchatIconId.setOnClickListener {
            openBrowser(contactUsViewModel.snapchatLink.value!!)

        }

        binding.twitterIconId.setOnClickListener {
            openBrowser(contactUsViewModel.twitterLink.value!!)

        }



        return binding.root
    }

    fun openBrowser( link:String)
    {
        try{
            Log.i("openBroweser",link)
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse("https://$link")
            startActivity(Intent.createChooser(openURL, "Browse with"));
        }
        catch (e:Exception)
        {
            Toast.makeText(requireActivity(),e.message,Toast.LENGTH_SHORT).show()
        }
    }

}
