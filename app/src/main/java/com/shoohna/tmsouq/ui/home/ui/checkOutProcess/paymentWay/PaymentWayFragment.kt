package com.shoohna.tmsouq.ui.home.ui.checkOutProcess.paymentWay

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FragmentPaymentWayBinding
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class PaymentWayFragment : Fragment() {

    lateinit var binding: FragmentPaymentWayBinding
    private val paymentWayViewModel: PaymentWayViewModel by inject()   // 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentPaymentWayBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
//        val paymentWayViewModel = ViewModelProvider(this).get(PaymentWayViewModel::class.java)
        binding.vm = paymentWayViewModel


        binding.nextBtnId.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_paymentWayFragment_to_recietFragment) }
        }

        return binding.root
    }



}
