package com.shoohna.tmsouq.ui.home.ui.cart

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shoohna.tmsouq.databinding.FragmentCartBinding
import com.shoohna.tmsouq.pojo.model.ProductEntity
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class CartFragment : Fragment() {
    lateinit var binding: FragmentCartBinding
    var myCartData= MutableLiveData<List<ProductEntity>>()
    private val cartViewModel: CartViewModel by inject()   // 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCartBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val model = ViewModelProvider(this).get(CartViewModel::class.java)
        binding.vm = cartViewModel

        cartViewModel.loadDataFromRoom(requireActivity())
        cartViewModel.myCartList.observe(viewLifecycleOwner, Observer {
            if(it.isEmpty())
            {
                binding.noDataTxtId.visibility = View.VISIBLE
                binding.cartRecyclerViewId.visibility = View.GONE
                binding.checkOutBtnId.visibility = View.GONE
            }
            else {
                binding.noDataTxtId.visibility = View.GONE
                binding.cartRecyclerViewId.visibility = View.VISIBLE
                binding.checkOutBtnId.visibility = View.VISIBLE

                myCartData.value = it
                binding.cartRecyclerViewId.adapter =
                    CartRecyclerViewAdapter(myCartData, requireActivity(), cartViewModel)
            }
        })

//        model.checkEmptyRoomData(activity!!)
//        model.noDataTxt.observe(viewLifecycleOwner, Observer {
//            if(it) {
//                binding.noDataTxtId.visibility = View.VISIBLE
//                binding.cartRecyclerViewId.visibility = View.GONE
//                binding.checkOutBtnId.visibility = View.GONE
//            }
//            else {
//                binding.noDataTxtId.visibility = View.GONE
//                binding.cartRecyclerViewId.visibility = View.VISIBLE
//                binding.checkOutBtnId.visibility = View.VISIBLE
//            }
//
//        })

//        binding.checkOutBtnId.setOnClickListener {
//            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_cartFragment_to_checkoutProcessFragment) }
//            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//        }
//        binding.statusBtnId.setOnClickListener {
//            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_cartFragment_to_orderStatusFragment) }
//            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//        }



        return binding.root
    }


}
