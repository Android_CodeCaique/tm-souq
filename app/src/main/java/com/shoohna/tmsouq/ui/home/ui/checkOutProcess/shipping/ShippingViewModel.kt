package com.shoohna.tmsouq.ui.home.ui.checkOutProcess.shipping

import ApiClient.sharedHelper
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Geocoder
import android.location.LocationManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.material.snackbar.Snackbar
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.pojo.responses.UserAddressData
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ShippingViewModel(private val serviceGeneral: Home) :BaseViewModel() {

    private var userAddressList: MutableLiveData<List<UserAddressData>>? = null
    var baseFragment: BaseFragment = BaseFragment()
    var locationManager: LocationManager? = null
    var GpsStatus = false
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var geocoder: Geocoder

    var lat = MutableLiveData<Double>(0.0)
    var lon = MutableLiveData<Double>(0.0)
    var addressFromUser = MutableLiveData<String>("")

    var userCurrentAddressCheck = MutableLiveData<Boolean>(false)
    var userAddressChecked = MutableLiveData<Int>(0)

    var loader = MutableLiveData<Boolean>(false)


    fun loadData(context: Context)
    {
        loader.value = true
//        val service = ApiClient.makeRetrofitServiceHome()
        CoroutineScope(Dispatchers.IO).launch {
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )
            runCatching { serviceGeneral.showUserAddress(lang.toString() , "Bearer ${sharedHelper.getKey(context, Constants.getToken())}") }
                .onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
                                userAddressList!!.postValue(it.body()!!.data)
                                loader.value = false

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
        }
    }

    internal fun getUserAddressList(context: Context): MutableLiveData<List<UserAddressData>> {
        if (userAddressList == null) {
            userAddressList = MutableLiveData()
            loadData(context)

        }
        return userAddressList as MutableLiveData<List<UserAddressData>>
    }

//    fun getCurrentLocation(v:View)
//    {
//        if(CheckGpsStatus(v))
//        {
//            try {
//                fusedLocationClient =
//                    LocationServices.getFusedLocationProviderClient(v.rootView.context)
//                geocoder = Geocoder(v.rootView.context, Locale.getDefault())
//
//                fusedLocationClient.lastLocation
//                    .addOnSuccessListener { location: Location? ->
//                        Log.i("Location1", location?.latitude.toString())
//                        lat.value = location?.latitude
//                        Log.i("Location1", location?.longitude.toString())
//                        lon.value = location?.longitude
//                        Log.i("Location1", geocoder.getFromLocation(location?.latitude!!, location.longitude, 1)[0].getAddressLine(0))
//                        addressFromGeoCoder.value = geocoder.getFromLocation(lat.value!!, lon.value!!, 1)[0].getAddressLine(0)
//                        Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.currentAddressSelected),Toast.LENGTH_SHORT).show()
//                        userCurrentAddressCheck.value = true
//                    }
//            }catch (e:Exception) {
//                Toast.makeText(
//                    v.rootView.context,
//                    v.rootView.context.resources.getString(R.string.currentLocationAdded),
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//        }
//
//    }
//
//    private fun CheckGpsStatus(v:View) : Boolean {
//        locationManager = v.rootView.context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//        assert(locationManager != null)
//        GpsStatus = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
//        return if (!GpsStatus) {
//            Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.GPSDisabled), Toast.LENGTH_SHORT).show()
//            false
//        } else
//            true
//    }

    fun sendAddressToApi(v:View)
    {

        if(userAddressChecked.value == 0)
        {
            Toast.makeText(v.rootView.context,v.rootView.context.resources.getString(R.string.PleaseSelectAddress),Toast.LENGTH_SHORT).show()
        }
//        else if(userCurrentAddressCheck.value== true)
//        {
//            loader.value = true
//            val service = ApiClient.makeRetrofitServiceHome()
//            CoroutineScope(Dispatchers.IO).launch {
//                val response = service.orderAddressWithoutAddressId(sharedHelper.getKey(v.rootView.context, "PRODUCT_ID")?.toInt()!!,
//                    "Bearer ${sharedHelper.getKey(v.rootView.context, Constants.getToken())}"
//                , 0.0
//                , 0.0
//                , addressFromUser.value!!)
//                withContext(Dispatchers.Main) {
//                    try{
//                        if(response.isSuccessful && response.body()?.status == 1){
//                            Log.i("response",response.body()!!.message)
//                            Toast.makeText(v.rootView.context,response.body()!!.message,Toast.LENGTH_SHORT).show()
//                            loader.value = false
//                            Navigation.findNavController(v).navigate(R.id.action_shippingFragment_to_paymentWayFragment)
//                        }
//                        else {
//                            Log.i("loadData1",response.message().toString())
//                            Log.i("loadData1",response.code().toString())
//                            loader.value = false
//                        }
//                    }catch (e:Exception){
//                        Log.i("loadData2",e.message.toString())
//                        loader.value = false
//
//                    }
//
//                }
//            }
//        }
        else
        {

            sharedHelper.putKey(v.context,"ADDRESS_ID_SHIPPING" , userAddressChecked.value.toString())
            Navigation.findNavController(v).navigate(R.id.action_shippingFragment_to_paymentWayFragment)

//            loader.value = true
//            val service = ApiClient.makeRetrofitServiceHome()
//            CoroutineScope(Dispatchers.IO).launch {
//                val response = service.orderAddressWithAddressId(
//                    sharedHelper.getKey(v.rootView.context, "PRODUCT_ID")?.toInt()!!,
//                    userAddressChecked.value!!,
//                    "Bearer ${sharedHelper.getKey(v.rootView.context, Constants.getToken())}")
//                withContext(Dispatchers.Main) {
//                    try{
//                        if(response.isSuccessful && response.body()?.status == 1){
//                            Log.i("response",response.body()!!.message)
//                            Toast.makeText(v.rootView.context,response.body()!!.message,Toast.LENGTH_SHORT).show()
//                            loader.value = false
//                            Navigation.findNavController(v).navigate(R.id.action_shippingFragment_to_paymentWayFragment)
//
//                        }
//                        else {
//                            Log.i("loadData1",response.message().toString())
//                            Log.i("loadData1",response.code().toString())
//                            loader.value = false
//                        }
//                    }catch (e:Exception){
//                        Log.i("loadData2",e.message.toString())
//                        loader.value = false
//
//                    }
//
//                }
//            }
        }

    }

    fun showLocationAlert(v:View)
    {
        val alert: Dialog? =Dialog(v.rootView.context)
        alert?.setContentView(R.layout.user_location)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<Button>(R.id.confirmBtnId)?.setOnClickListener {
            addressFromUser.value =
                alert?.findViewById<EditText>(R.id.addressEditTextId).text.toString()
            if (addressFromUser.value!!.isEmpty())
                Toast.makeText(v.rootView.context, v.rootView.context.resources.getString(R.string.pleaseWriteAddress), Toast.LENGTH_SHORT).show()
            else {
//            userCurrentAddressCheck.value = true
                loader.value = true
//                val service = ApiClient.makeRetrofitServiceHome()
                CoroutineScope(Dispatchers.IO).launch {
                    val sharedHelper : SharedHelper = SharedHelper()
                    val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
                    runCatching {
                    serviceGeneral.addSingleAddress(
                        lang.toString() ,
                        "Bearer ${sharedHelper.getKey(v.rootView.context, Constants.getToken())}"
                        , 0.0, 0.0, addressFromUser.value!!)}.onSuccess {
                        withContext(Dispatchers.Main) {
                            try {
                                if (it.isSuccessful && it.body()?.status == 1) {
                                    Log.i("response", it.body()!!.message)
                                    Toast.makeText(
                                        v.rootView.context,
                                        it.body()!!.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    loader.value = false
//                                Navigation.findNavController(v)
//                                    .navigate(R.id.action_shippingFragment_to_paymentWayFragment)
                                    loadData(v.rootView.context)
                                    alert.dismiss()
                                } else {
                                    Log.i("loadData1", it.message().toString())
                                    Log.i("loadData1", it.code().toString())
                                    loader.value = false
                                }
                            } catch (e: Exception) {
                                Log.i("loadData2", e.message.toString())
                                loader.value = false

                            }

                        }
                    }.onFailure {
                        Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                    }
                }
            }
        }
        alert?.findViewById<Button>(R.id.cancelBtnId)?.setOnClickListener {
            alert.dismiss()
        }
        alert?.show()
    }


}