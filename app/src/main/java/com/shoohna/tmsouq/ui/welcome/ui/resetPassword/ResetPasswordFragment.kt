package com.shoohna.tmsouq.ui.welcome.ui.resetPassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.shoohna.tmsouq.databinding.FragmentForgetPasswordBinding
import com.shoohna.tmsouq.databinding.FragmentResetOasswordBinding
import com.shoohna.tmsouq.util.base.SharedHelper
import org.koin.android.ext.android.inject

class ResetPasswordFragment : Fragment() {


    lateinit var binding: FragmentResetOasswordBinding
    private val resetPasswordViewModel: ResetPasswordViewModel by inject()   // 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentResetOasswordBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

        val sharedHelper = SharedHelper()
        sharedHelper.putKey(requireActivity(), "OPEN", "OPEN")


//        val viewModel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)

        binding.vm = resetPasswordViewModel

        return binding.root
    }


}
