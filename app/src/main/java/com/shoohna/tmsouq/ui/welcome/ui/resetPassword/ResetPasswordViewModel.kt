package com.shoohna.tmsouq.ui.welcome.ui.resetPassword

import android.app.Activity
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.auth
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException

class ResetPasswordViewModel(private val serviceGeneral: auth) : BaseViewModel(){

    var code = MutableLiveData<String>("")
    var password = MutableLiveData<String>("")
    var baseFragment: BaseFragment = BaseFragment()
    var loader = MutableLiveData<Boolean>(false)
    public lateinit var nav: NavController

    fun resetPassword(v: View, codeLayouyt: TextInputLayout, passwordLayout: TextInputLayout)
    {

        if(validate(v ,codeLayouyt , passwordLayout))
            return

        if(code.value?.isNotEmpty()!! && password.value?.isNotEmpty()!!) {
            loader.value = true
//            val service = ApiClient.makeRetrofitService()


            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )



            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.resetPasswordFunction(lang.toString() , code.value!! , password.value!!)
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful) {
                                Log.d("HHHHH", it.body()!!.status.toString())
                                if (it.body()!!.status == 1) {
                                    //Do something with response e.g show to the UI.

                                    Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                        .show();
                                    loader.value = false
//                                    Snackbar.make(v, it.message(), Snackbar.LENGTH_SHORT).show();
                                    code.value = ""
                                    password.value = ""
                                    nav = Navigation.findNavController(
                                        v!!.context as Activity,
                                        R.id.nav_host_fragment
                                    )
                                    nav.navigate(R.id.loginFragment)
                                } else if (it.body()!!.status == 2) {
                                    Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                        .show()
                                    loader.value = false
                                } else if (it.body()!!.status == 0) {
                                    Snackbar.make(
                                        v,
                                        "${it.body()!!.message}",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                    loader.value = false
                                    //baseFragment.dismissProgressDialog()
                                } else {
                                    loader.value = false
                                }
                            }
                            else
                            {
                                Snackbar.make(v, it.message(), Snackbar.LENGTH_SHORT)
                                    .show();
                                Log.d("ERROR", "AMMMMM")
                                loader.value = false
                            }
                        } catch (e: HttpException) {
                            Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                                .show();
                            loader.value = false

                        } catch (e: Throwable) {
                            Snackbar.make(
                                v,
                                "Ooops: Something else went wrong",
                                Snackbar.LENGTH_SHORT
                            ).show();
                            loader.value = false
                        }
                    }
                }.onFailure {
                    loader.value = false
                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()
                }
            }


//            CoroutineScope(Dispatchers.IO).launch {
//
//                val response = service.resetPasswordFunction(lang.toString() , code.value!! , password.value!!)
//                withContext(Dispatchers.Main) {
//
//                }
//            }
        }
        else
            Snackbar.make(v, v.resources.getString(R.string.pleaseInsertAllData), Snackbar.LENGTH_SHORT).show();

    }

    fun validate(v: View, codeLayouyt: TextInputLayout , passordLayout: TextInputLayout): Boolean
    {
        var stop = false
        if (checkEmpty(passordLayout , password.value.toString(),v.resources.getString(R.string.required) ))
        {
            stop = true
        }
        else if (checkValidPassword(passordLayout , password.value.toString(),v.resources.getString(R.string.notValidPassword) ))
        {
            stop = true
        }
        if (checkEmpty(codeLayouyt , code.value.toString(),v.resources.getString(R.string.required) ))
        {
            stop = true
        }

        if (stop)
            return true
        else
            return false
    }
}


