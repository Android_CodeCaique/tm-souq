package com.shoohna.tmsouq.ui.home.ui.checkOutProcess.myCart

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.tmsouq.databinding.CheckoutMycartItemRowBinding
import com.shoohna.tmsouq.pojo.model.ProductEntity

class MyCartRecyclerViewAdapter (private var dataList: LiveData<List<ProductEntity>>, private val context: Context?) : RecyclerView.Adapter<MyCartRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CheckoutMycartItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])


    }


    class ViewHolder(private var binding: CheckoutMycartItemRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductEntity) {
            binding.modelRoom = item
            binding.executePendingBindings()

        }

    }
}
