package com.shoohna.tmsouq.ui.home.ui.home.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.e_commercehappytimes.ui.home.ui.home.HomeFragmentDirections
import com.shoohna.e_commercehappytimes.ui.home.ui.home.HomeViewModel
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.ItemProductCategoryBinding
import com.shoohna.tmsouq.pojo.responses.Project1
import com.shoohna.tmsouq.ui.home.MainActivity

class ProductsCategoryAdapter (private var dataList: LiveData<List<Project1>>,
                               private val context: Context? , private val model: HomeViewModel) : RecyclerView.Adapter<ProductsCategoryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProductCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false) , model , context
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])


    }


    class ViewHolder(private var binding: ItemProductCategoryBinding , private val model: HomeViewModel , private val context: Context?) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Project1) {
            binding.model = item
            binding.executePendingBindings()
//            binding.distanceId.setPaintFlags(binding.distanceId.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)
            binding.mainConstraintLayoutId.setOnClickListener {
//                Navigation.findNavController(itemView).navigate(R.id.action_homeFragment_to_productDetailsFragment)
                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
                val action = HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(item.id,item.image,item.desc,item.price_product)
                Navigation.findNavController(it).navigate(action)

                Log.i("Item Id",item.id.toString())
            }

            binding.cartImgId.setOnClickListener {

                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
                val action = HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(item.id,item.image,item.desc,item.price_product)
                Navigation.findNavController(it).navigate(action)

                Log.i("Item Id",item.id.toString())

            }

            binding.heartImgId.setOnClickListener {
                model.productId.value = item.id
                model.addToWishlist(context!!)
//                binding.heartImgId.background = R.drawable.ic_heart_white
            }
        }

    }

}