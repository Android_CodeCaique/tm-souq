package com.shoohna.tmsouq.ui.home.ui.orderStatus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shoohna.tmsouq.databinding.FragmentOrderStatusBinding
import com.shoohna.tmsouq.pojo.responses.MyOrdersData
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class OrderStatusFragment : Fragment() {
    lateinit var binding: FragmentOrderStatusBinding

    var orderStatuesList = ArrayList<MyOrdersData>()
    lateinit var orderStatuesAdapter: OrderStatusRecyclerAdapter
    var myOrdersData= MutableLiveData<List<MyOrdersData>>()
    private val orderStatusViewModel: OrderStatusViewModel by inject()   // 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentOrderStatusBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val model = ViewModelProvider(this).get(OrderStatusViewModel::class.java)
        binding.vm = orderStatusViewModel
        orderStatusViewModel.geMyOrdersList(requireActivity()).observe(viewLifecycleOwner, Observer {

            myOrdersData.value = it

            binding.orderStatusRecycvlerViewId.adapter =
                OrderStatusRecyclerAdapter(myOrdersData, requireActivity())

        })


//        setUpStepView()
//        addData()

        return binding.root
    }

//    private fun addData() {
//        orderStatuesList.add(OrderStatusModel(1,"تم الوصول","يجب عليك الذهاب للمطار لكي تتلقي شحنتك"))
//        orderStatuesList.add(OrderStatusModel(3,"في وضع الشحن","لا يوجد ملاحظات"))
//        orderStatuesList.add(OrderStatusModel(3,"في وضع الشحن","لا يوجد ملاحظات"))
//         orderStatuesAdapter.notifyDataSetChanged()
//
//    }

}
