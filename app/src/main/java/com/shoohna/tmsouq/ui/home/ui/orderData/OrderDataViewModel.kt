package com.shoohna.tmsouq.ui.home.ui.orderData

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.pojo.responses.MyOrderProductsProduct
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class OrderDataViewModel(private val serviceGeneral: Home) : BaseViewModel() {

    var shopId = MutableLiveData<Int>(0)

    var myOrderProductList: MutableLiveData<List<MyOrderProductsProduct>>? = null
    var loader = MutableLiveData<Boolean>(false)

    fun loadData(context: Context)
    {
        loader.value = true
        try{
//            val service = ApiClient.makeRetrofitServiceHome()

            CoroutineScope(Dispatchers.IO).async {
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(context , "MyLang" )
                runCatching {
                    serviceGeneral.myOrdersProducts(
                        lang.toString(),
                        "Bearer ${sharedHelper.getKey(context, Constants.getToken())}",
                        shopId.value?.toInt()!!
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {

                        if (it.isSuccessful && it.body()?.status == 1) {
                            myOrderProductList!!.postValue(it.body()!!.data.products)
                            loader.value = false

                        } else {
                            Log.i("loadData1", it.message().toString())
                            loader.value = false
                        }


                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
            }
        }catch (e:Exception){
            Log.i("loadData2",e.message.toString())
            loader.value = false

        }
    }

    internal fun geMyOrderProductsList(context: Context): MutableLiveData<List<MyOrderProductsProduct>> {
        if (myOrderProductList == null) {
            myOrderProductList = MutableLiveData()
            loadData(context)

        }
        return myOrderProductList as MutableLiveData<List<MyOrderProductsProduct>>
    }

}