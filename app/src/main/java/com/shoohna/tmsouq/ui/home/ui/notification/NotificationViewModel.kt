package com.shoohna.tmsouq.ui.home.ui.notification

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.pojo.responses.NotificationData
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import kotlinx.coroutines.*

class NotificationViewModel(private val serviceGeneral: Home)  : ViewModel() {
    private var notificationList: MutableLiveData<List<NotificationData>>? = null
    var loader = MutableLiveData<Boolean>(false)


    private fun loadData(context: Context) {

        loader.value = true

        try {
//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                val sharedHelper : SharedHelper = SharedHelper()
                val lang : String? = sharedHelper.getKey(context , "MyLang" )
                runCatching {
                 serviceGeneral.getNotification(
                        lang.toString(),
                        "Bearer ${sharedHelper.getKey(
                            context,
                            Constants.getToken()
                        )}"
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
//                            Toast.makeText(context, response.body()!!.message, Toast.LENGTH_SHORT)
//                                .show()
//                        baseFragment.showDialog(context,response.body()!!.message)
                                notificationList!!.postValue(it.body()!!.data)

                                loader.value = false
                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                                Toast.makeText(context, it.message(), Toast.LENGTH_SHORT)
                                    .show()
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false
                            Toast.makeText(
                                context,
                                "Exception ${e.message.toString()}",
                                Toast.LENGTH_SHORT
                            ).show()

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
            }
        }catch (e:Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

        }

    }

    internal fun getNotificationList(context: Context): MutableLiveData<List<NotificationData>> {
        if (notificationList == null) {
            notificationList = MutableLiveData()
            loadData(context)
        }
        return notificationList as MutableLiveData<List<NotificationData>>
    }
}