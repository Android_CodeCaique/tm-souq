package com.shoohna.e_commercehappytimes.ui.home.ui.home

import ApiClient.sharedHelper
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.gauravk.bubblenavigation.BubbleNavigationConstraintView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.navigation.NavigationView
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FragmentHomeBinding
import com.shoohna.tmsouq.pojo.responses.*
import com.shoohna.tmsouq.ui.home.ui.home.*
import com.shoohna.tmsouq.ui.home.ui.home.adapters.*
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.shoohna.util.base.BaseFragment
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.activity_main.view.*

import kotlinx.android.synthetic.main.bottom_sheet_layout.*
import kotlinx.android.synthetic.main.fragment_more.view.*
import org.koin.android.ext.android.inject
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    lateinit var binding: FragmentHomeBinding

    var bestSaleData= MutableLiveData<List<BestSeller>>()
    var productsData= MutableLiveData<List<Project1>>()
    var offersData= MutableLiveData<List<Offer>>()
    var highRatedData= MutableLiveData<List<HighRated>>()
    var categoryData= MutableLiveData<List<Category>>()
    var sliderList : ArrayList<sliders> = ArrayList()
    lateinit var baseFragment:BaseFragment
    private val homeViewModel: HomeViewModel by inject()   // 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        Log.i("SharedToken",sharedHelper.getKey(requireActivity(), Constants.getToken()))
        Log.i("USER_PHOTO",sharedHelper.getKey(requireActivity(), "USER_PHOTO").toString())
        activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
//        try {
//            if(sharedHelper.getKey(requireActivity(), "USER_PHOTO").toString() == "http://api.shoohna.com/images/1.png")
//                Glide.with(requireActivity()).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
//            else
//                Glide.with(requireActivity()).load(sharedHelper.getKey(requireActivity(), "USER_PHOTO")).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
//
//        }catch (e:Exception)
//        {}

        try {

            val headerView: View = activity?.findViewById<NavigationView>(R.id.nav_view_id)!!.getHeaderView(0)
            headerView.nameTxtId.text = sharedHelper.getKey(requireActivity(),"USER_NAME")
            headerView.emailTxtId.text = sharedHelper.getKey(requireActivity(),"USER_EMAIL")
//            Picasso.get().load(sharedHelper.getKey(activity!!, "USER_PHOTO")).into(binding.circleImageView)
            if(sharedHelper.getKey(requireActivity(), "USER_PHOTO").toString() == "http://api.shoohna.com/images/1.png")
                Glide.with(requireActivity()).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(headerView.circleImageView)
            else
                Glide.with(requireActivity()).load(sharedHelper.getKey(requireActivity(), "USER_PHOTO")).error(R.drawable.ic_profile_upload).into(headerView.circleImageView)

        }catch (e: Exception)
        {}

        baseFragment = BaseFragment()
        binding.vm = homeViewModel
        if(baseFragment.verifyAvailableNetwork(requireActivity())) {
            homeViewModel.loadData(requireActivity())
            homeViewModel.loadCategoryData(requireActivity())

            homeViewModel.offersList.observe(viewLifecycleOwner, Observer {
                Log.i("OfferObserve",it.toString())
                offersData.value = it
                binding.offersRecyclerViewId.adapter = OffersRecyclerAdapter(offersData, context?.applicationContext)
                (binding.offersRecyclerViewId.adapter as OffersRecyclerAdapter).notifyDataSetChanged()

            })
            homeViewModel.highRatedList.observe(viewLifecycleOwner, Observer {
                Log.i("highRatedObserve",it.toString())
                highRatedData.value = it
                binding.highRelatedRecyclerViewId.adapter = HighRatedRecyclerAdapter(highRatedData, context?.applicationContext)
                (binding.highRelatedRecyclerViewId.adapter as HighRatedRecyclerAdapter).notifyDataSetChanged()
            })

            homeViewModel.bestSalesList.observe(viewLifecycleOwner, Observer {
                Log.i("bestSalesObserve",it.toString())
                bestSaleData.value = it
                binding.bestSaleRecyclerViewId.adapter = BestSalesRecyclerAdapter(bestSaleData, context?.applicationContext)
                (binding.bestSaleRecyclerViewId.adapter as BestSalesRecyclerAdapter).notifyDataSetChanged()
            })

            homeViewModel.productsList.observe(viewLifecycleOwner, Observer {
                Log.i("productsObserve", it.toString())
                productsData.value = it
                var gridLayoutManager : GridLayoutManager = GridLayoutManager(requireContext() ,2)
                binding.categoryRecyclerViewId.layoutManager = gridLayoutManager
                binding.categoryRecyclerViewId.adapter = ProductsCategoryAdapter(productsData, context?.applicationContext , homeViewModel)
                (binding.categoryRecyclerViewId.adapter as ProductsCategoryAdapter).notifyDataSetChanged()
            })

            homeViewModel.categoryList.observe(viewLifecycleOwner , Observer {
                Log.i("CategoryDataObserve",it.toString())
                categoryData.value = it
                var adapter : CategoriesRecyclerAdapter = CategoriesRecyclerAdapter(this.categoryData, context?.applicationContext,homeViewModel,viewLifecycleOwner,binding)
                binding.categorieRecyclerViewId.adapter = adapter
                adapter.setOnItemClickListener(object : CategoriesRecyclerAdapter.OnItemClickListener {
                    override fun onItemClick(position: Int) {
                        Log.d("HHHHH" , categoryData.value!!.get(position).name)
                        if (position == 0)
                        {
                            homeViewModel.category.value = false
                        }
                        else
                        {
                            homeViewModel.category.value = true
                            homeViewModel.loadCategoryProductData(requireContext() , categoryData.value!!.get(position).id)
                        }
                    }
                })



                (binding.categorieRecyclerViewId.adapter as CategoriesRecyclerAdapter).notifyDataSetChanged()
            })

            homeViewModel.sliderList.observe(viewLifecycleOwner , Observer {
                Log.i("CategoryDataObserve",it.toString())
                sliderList = it as ArrayList<sliders>


                val adapter = sliderImagesAdapter(context)
                adapter.renewItems(sliderList)


                binding.imageSlider.setSliderAdapter(adapter)

                binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
                binding.imageSlider.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH)

                binding.imageSlider.setScrollTimeInSec(4) //set scroll delay in seconds :

                binding.imageSlider.startAutoCycle()
            })

        }else
            Toast.makeText(requireActivity(),getString(R.string.noIntenetConnection),Toast.LENGTH_SHORT).show()


        binding.notificationImgId.setOnClickListener {
            if(sharedHelper.getKey(requireActivity(),Constants.getToken())?.isEmpty()!!){
                baseFragment.showAlert(it.rootView.context)
            }else {
                view?.let { it1 ->
                    Navigation.findNavController(it1)
                        .navigate(R.id.action_homeFragment_to_notificationFragment)
                }
                activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }
        }

        binding.messangerImgId.setOnClickListener {
            if(sharedHelper.getKey(requireActivity(),Constants.getToken())?.isEmpty()!!){
                baseFragment.showAlert(it.rootView.context)
            }else {
                view?.let { it1 ->
                    Navigation.findNavController(it1)
                        .navigate(R.id.action_homeFragment_to_chatFragment)
                }
                activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }
        }

        binding.bestSaleTxtViewId.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToProductFragment(resources.getString(R.string.bestSale),null)
            view?.let { it1 -> Navigation.findNavController(it1).navigate(action) }
        }

        binding.offersTxtViewId.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToProductFragment(resources.getString(R.string.offers),null)
            view?.let { it1 -> Navigation.findNavController(it1).navigate(action) }
        }

        binding.highRatedTxtViewId.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToProductFragment(resources.getString(R.string.highRated),null)
            view?.let { it1 -> Navigation.findNavController(it1).navigate(action) }
        }



//        binding.searchEditTextID.setOnFocusChangeListener { _, hasFocus ->
//            if(hasFocus)
//                activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        }

        binding.filterImgId.setOnClickListener { showBottomSheetDialogFragment() }

        binding.searchEditTextID.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
//                model.searchByName(activity!!,binding.searchEditTextID)
                if(binding.searchEditTextID.text.isEmpty())
                    Toast.makeText(requireActivity(),getString(R.string.pleaseWriteSearchWord),Toast.LENGTH_SHORT).show()
                else {
                    val action = HomeFragmentDirections.actionHomeFragmentToProductFragment(
                        resources.getString(R.string.productSearch),
                        binding.searchEditTextID.text.toString()
                    )
                    view?.let { it1 -> Navigation.findNavController(it1).navigate(action) }
                    binding.searchEditTextID.text.clear()
                }
                return@OnKeyListener true
            }
            false
        })


        binding.drawerIconId.setOnClickListener {
            activity?.findViewById<DrawerLayout>(R.id.drawer)!!.drawer.openDrawer(GravityCompat.START)
        }

        return binding.root
    }

    private fun slideUpDownBottomSheet() {
        var bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback()
        {
            override fun onSlide(p0: View, p1: Float) {
            }

            override fun onStateChanged(p0: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE

                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE

                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }
                }
            }

        })

//        var homeHeigth :Int = activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)?.layoutParams!!.height
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
        } else {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED;
            activity?.findViewById<BubbleNavigationConstraintView>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE

        }
    }

    private fun showBottomSheetDialogFragment() {
        val bottomSheetFragment = FilterBottomSheet(homeViewModel)
        activity?.supportFragmentManager?.let { bottomSheetFragment.show(it, bottomSheetFragment.tag) }
//        bottomSheetFragment.binding.doneBtnId.setOnClickListener {
//        }
//        bottomSheetFragment.doneBtnId.setOnClickListener {
//            Log.i("doneBtn","Clicked")
//            bottomSheetFragment.dismiss()
//        }
    }


}
