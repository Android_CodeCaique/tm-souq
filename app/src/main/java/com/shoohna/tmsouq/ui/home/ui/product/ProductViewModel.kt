package com.shoohna.tmsouq.ui.home.ui.product

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.pojo.responses.ProductByTypeData
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*

class ProductViewModel(private val serviceGeneral: Home) :BaseViewModel() {

    private var productTypeList: MutableLiveData<List<ProductByTypeData>>? = null
    private var productSearchList: MutableLiveData<List<ProductByTypeData>>? = null
    var baseFragment: BaseFragment = BaseFragment()

    var typeId = MutableLiveData<Int>(0)
    var productId = MutableLiveData<Int>(0)
    var searchWord= MutableLiveData<String>("")

    var isFavorite = MutableLiveData<Boolean>(false)
    var loader = MutableLiveData<Boolean>(false)

    var searchEditText = MutableLiveData<String>("")

    fun loadData(context: Context) {
        loader.value = true
        try {
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )

//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                serviceGeneral.getAllProductByType(
                    lang.toString() ,
                    typeId.value?.toInt()!!,
                    "Bearer ${sharedHelper.getKey(context, Constants.getToken())}"
                )}.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
                                productTypeList!!.postValue(it.body()!!.data)
                                loader.value = false

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
            }
        }catch (e:java.lang.Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

        }
    }

    fun loadDataFromSearchByName(context: Context)
    {
        loader.value = true

        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(context , "MyLang" )
        Log.i("SearchWordVM",searchWord.value.toString())
        try {
//            val service = ApiClient.makeRetrofitServiceHome()

            CoroutineScope(Dispatchers.IO).async {
                runCatching { serviceGeneral.searchByName(lang.toString() , searchWord.value!!.toString())}
                    .onSuccess {
                        withContext(Dispatchers.Main) {
                            try {
                                if (it.isSuccessful && it.body()?.status == 1) {
                                    Log.i("response", it.body()!!.message)
                                    Log.i("response", it.body()!!.data.toString())

                                    productSearchList!!.postValue(it.body()!!.data)
                                    loader.value = false

                                } else {
                                    Log.i("loadData1", it.message().toString())
                                    Toast.makeText(
                                        context,
                                        it.message().toString(),
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    loader.value = false
                                }
                            } catch (e: Exception) {
                                Log.i("loadData2", e.message.toString())
                                Toast.makeText(
                                    context,
                                    "Exception ${e.message.toString()}",
                                    Toast.LENGTH_SHORT
                                ).show()
                                loader.value = false

                            }

                        }
                    }.onFailure {
                        Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                    }
            }
        }catch (e:java.lang.Exception)
        {
            Toast.makeText(context,e.message.toString(),Toast.LENGTH_SHORT).show()

        }
    }

    fun addToWishlist(context: Context)
    {
        loader.value = true
        try {
            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(context , "MyLang" )

//            val service = ApiClient.makeRetrofitServiceHome()
            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.addOrDeleteFromMyWishlist(
                        lang.toString(),
                        productId.value?.toInt()!!,
                        "Bearer ${sharedHelper.getKey(context, Constants.getToken())}"
                    )
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {
                            if (it.isSuccessful && it.body()?.status == 1) {
                                Log.i("response", it.body()!!.message)
                                Toast.makeText(
                                    context,
                                    it.body()!!.message,
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                                loader.value = false
//                        if(response.body()!!.message.equals("Product deleted from Whishlist")){
//                            isFavorite.value = false
//                        }else if(response.body()!!.message.equals("Product added in Whishlist")) {
//                            isFavorite.value = true
//                        }
                                loadData(context)

                            } else {
                                Log.i("loadData1", it.message().toString())
                                loader.value = false
                                Toast.makeText(context, it.message(), Toast.LENGTH_SHORT)
                                    .show()
                            }
                        } catch (e: Exception) {
                            Log.i("loadData2", e.message.toString())
                            loader.value = false
                            Toast.makeText(
                                context,
                                "Exception ${e.message.toString()}",
                                Toast.LENGTH_SHORT
                            ).show()

                        }

                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()

                }
            }
        }catch (e:Exception)
        {

        }
    }



    internal fun getProductTypeList(context: Context): MutableLiveData<List<ProductByTypeData>> {
        if (productTypeList == null && typeId.value != 0) {
            productTypeList = MutableLiveData()
            loadData(context)
        }
        return productTypeList as MutableLiveData<List<ProductByTypeData>>
    }

    internal fun getPorductDataBySearchName(context: Context): MutableLiveData<List<ProductByTypeData>> {
        if (productSearchList == null ) {
            productSearchList = MutableLiveData()
            loadDataFromSearchByName(context)
        }
        return productSearchList as MutableLiveData<List<ProductByTypeData>>

    }
}