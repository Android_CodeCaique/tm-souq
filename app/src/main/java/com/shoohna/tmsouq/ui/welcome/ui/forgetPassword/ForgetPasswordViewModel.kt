package com.shoohna.tmsouq.ui.welcome.ui.forgetPassword

import android.app.Activity
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.auth
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException

class ForgetPasswordViewModel(private val serviceGeneral: auth) : BaseViewModel() {

    var email = MutableLiveData<String>("")
    var baseFragment: BaseFragment = BaseFragment()
    var sharedHelper: SharedHelper = SharedHelper()
    var loader = MutableLiveData<Boolean>(false)
    public lateinit var nav: NavController

    fun forgetPassword(v: View, emailLayout: TextInputLayout)
    {

        if(validate(v ,emailLayout))
            return

        if(email.value?.isNotEmpty()!!) {
            loader.value = true

//            baseFragment.showProgressDialog(v.context,v.resources.getString(R.string.login),v.resources.getString(R.string.pleaseWaitUntilLogin),false)
//            val service = ApiClient.makeRetrofitService()

            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )


            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.forgetPasswordFunction(lang.toString() , email.value!!)
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        try {

                            if (it.isSuccessful) {
                                if (it.body()!!.status == 1) {
                                    //Do something with response e.g show to the UI.

//                                    Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
//                                        .show();
                                    loader.value = false

                                  //  Snackbar.make(v, it.message(), Snackbar.LENGTH_SHORT).show();
                                    email.value = ""

                                    sharedHelper.putKey(v.context, "OPEN", "OPEN")

                                    nav = Navigation.findNavController(
                                        v!!.context as Activity,
                                        R.id.nav_host_fragment
                                    )
                                    nav.navigate(R.id.resetPasswordFragment)
                                } else if (it.body()!!.status == 2) {
                                    Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                        .show()
//                                baseFragment.dismissProgressDialog()
                                    loader.value = false

                                } else if (it.body()!!.status == 0) {
                                    Snackbar.make(
                                        v,
                                        "${it.body()!!.message}",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
//                                baseFragment.dismissProgressDialog()
                                    loader.value = false
                                } else {

                                }
                            }

                        } catch (e: HttpException) {
                            Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                                .show();
                            //baseFragment.dismissProgressDialog()
                            loader.value = false


                        } catch (e: Throwable) {
                            Snackbar.make(
                                v,
                                "Ooops: Something else went wrong",
                                Snackbar.LENGTH_SHORT
                            ).show();
//                        baseFragment.dismissProgressDialog()
                            loader.value = false

                        }
                    }
                }.onFailure {
                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                }
            }



//            CoroutineScope(Dispatchers.IO).launch {
//
//                val response = service.forgetPasswordFunction(lang.toString() , email.value!!)
//                withContext(Dispatchers.Main) {
//
//                }
//            }
        }
        else
            Snackbar.make(v, v.resources.getString(R.string.pleaseInsertAllData), Snackbar.LENGTH_SHORT).show();

    }

    fun validate(v: View, emailLayout: TextInputLayout): Boolean
    {
        if (checkEmpty(emailLayout , email.value.toString(),v.resources.getString(R.string.required) ))
            return true
        return false
    }
}