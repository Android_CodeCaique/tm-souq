package com.shoohna.tmsouq.ui.home.ui.orderStatus

data class OrderStatusModel(val orderId:Int , val orderStatus :String , val notes:String)