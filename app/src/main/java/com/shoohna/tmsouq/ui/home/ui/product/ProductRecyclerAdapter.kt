package com.shoohna.tmsouq.ui.home.ui.product

import ApiClient.sharedHelper
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FragmentProductBinding
import com.shoohna.tmsouq.databinding.ProductBestSaleRowBinding
import com.shoohna.tmsouq.pojo.responses.ProductByTypeData
import com.shoohna.tmsouq.ui.home.MainActivity
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.shoohna.util.base.BaseFragment

class ProductRecyclerAdapter  (private var dataList: LiveData<List<ProductByTypeData>>,
                               private val context: Context?,
                               var model: ProductViewModel, private val viewLifecycleOwner: LifecycleOwner,
                               var MainBinding: FragmentProductBinding) : RecyclerView.Adapter<ProductRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProductBestSaleRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position],context , model , viewLifecycleOwner , MainBinding)

    }



    fun filterList(filteredList: LiveData<List<ProductByTypeData>>) {
        dataList = filteredList
    }


    class ViewHolder(private var binding: ProductBestSaleRowBinding) : RecyclerView.ViewHolder(binding.root) {
        var baseFragment : BaseFragment = BaseFragment()

        fun bind(item: ProductByTypeData ,context: Context?, model: ProductViewModel , viewLifecycleOwner: LifecycleOwner ,MainBinding: FragmentProductBinding) {
            binding.model = item
            binding.executePendingBindings()
            binding.cartImgId.setOnClickListener {

                    val action =
                        ProductFragmentDirections.actionProductFragmentToProductDetailsFragment2(
                            item.id,
                            item.image,
                            item.desc,
                            item.price_product
                        )
                    Navigation.findNavController(it).navigate(action)
                    (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                        View.GONE

            }

            binding.mainConstraintId.setOnClickListener {
                val action =
                    ProductFragmentDirections.actionProductFragmentToProductDetailsFragment2(
                        item.id,
                        item.image,
                        item.desc,
                        item.price_product
                    )
                Navigation.findNavController(it).navigate(action)
                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }
            binding.heartImgId.setOnClickListener {
                if(sharedHelper.getKey(context!!, Constants.getToken())?.isEmpty()!!){
                    baseFragment.showAlert(it.rootView.context)
                }else {
                    model.productId.value = item.id
                    Log.i("Pro_Id", item.id.toString())
                    model.addToWishlist(context!!)
                    model.loadData(context!!)

                }

//                model.isFavorite.observe(viewLifecycleOwner, Observer {
//                    if (it) {
//                        binding.heartImgId.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_heart_circle_fill))
////                        Picasso.get().load(R.drawable.ic_heart_circle_fill).into(binding.heartImgId)
//                        MainBinding.productViewRecyclerViewId.adapter?.notifyItemChanged(item.id)
//
//                        Log.i("ObserveIsFavorite",it.toString())
//                    }
//                    else {
//
//
//                        binding.heartImgId.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_heart_circle))
////                        Picasso.get().load(R.drawable.ic_heart_circle).into(binding.heartImgId)
//                        Log.i("ObserveIsFavorite",it.toString())
//                        MainBinding.productViewRecyclerViewId.adapter?.notifyItemChanged(item.id)
//
//                    }
//
//
//                })
//                if(model.isFavorite.value == true) {
//                    binding.heartImgId.setBackgroundResource( R.drawable.ic_heart_circle_fill )
//                }
//                else
//                    binding.heartImgId.setBackgroundResource( R.drawable.ic_heart_circle )

            }


        }

    }

}