package com.shoohna.tmsouq.ui.home.ui.contactUs

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.General
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*


class ContactUsViewModel(private val serviceGeneral: General) :BaseViewModel(){
    var phoneCompany = MutableLiveData<String>("")
    var addressCompany = MutableLiveData<String>("")
    var emailCompany = MutableLiveData<String>("")
    var email = MutableLiveData<String>("")
    var phone = MutableLiveData<String>("")
    var message = MutableLiveData<String>("")
    var baseFragment: BaseFragment = BaseFragment()
    var loader = MutableLiveData<Boolean>(false)
    var facebookLink = MutableLiveData<String>("")
    var snapchatLink = MutableLiveData<String>("")
    var twitterLink = MutableLiveData<String>("")
    var instagramLink = MutableLiveData<String>("")

//    val serviceAPI: ApiClientGeneral = get()
//    val gitHubService: General = serviceAPI.makeRetrofitServiceGeneral()

    fun loadData(v: View) {
        loader.value = true

        try {

            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )

//            val service = ApiClient.makeRetrofitServiceGeneral()

            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.aboutUsFunction(lang.toString() , "1")
                }.onSuccess {
                    withContext(Dispatchers.Main) {
                        if (it.body()!!.status == 1) {
                            phoneCompany.value = it.body()!!.data.phone
                            addressCompany.value = it.body()!!.data.address
                            emailCompany.value = it.body()!!.data.email
                            facebookLink.value = it.body()!!.data.facebook
                            snapchatLink.value = it.body()!!.data.snap
                            twitterLink.value = it.body()!!.data.twitter
                            instagramLink.value = it.body()!!.data.instagram
                            loader.value = false
                        } else if (it.body()!!.status == 2) {
                            Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                .show()
                            loader.value = false
                        } else if (it.body()!!.status == 0) {
                            Snackbar.make(
                                v,
                                "${it.body()!!.message}",
                                Snackbar.LENGTH_SHORT
                            ).show()
                            loader.value = false
                            //baseFragment.dismissProgressDialog()
                        }
                    }
                }.onFailure {
                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                }
            }

            // With ViewModelScope

//            viewModelScope.async {
//                runCatching {
//                }.onSuccess {
////                    emitUiState(movies = Event(it.movies))
//                    if (it.body()!!.status == 1) {
//                        phoneCompany.value = it.body()!!.data.phone
//                        addressCompany.value = it.body()!!.data.address
//                        emailCompany.value = it.body()!!.data.email
//                        facebookLink.value = it.body()!!.data.facebook
//                        snapchatLink.value = it.body()!!.data.snap
//                        twitterLink.value = it.body()!!.data.twitter
//                        instagramLink.value = it.body()!!.data.instagram
//                        loader.value = false
//                    }
//                    else if (it.body()!!.status == 2) {
//                                Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
//                                    .show()
//                                loader.value = false
//                            } else if (it.body()!!.status == 0) {
//                                Snackbar.make(
//                                    v,
//                                    "${it.body()!!.message}",
//                                    Snackbar.LENGTH_SHORT
//                                ).show()
//                                loader.value = false
//                                //baseFragment.dismissProgressDialog()
//                            }
//
//                }.onFailure {
//
//                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()
//                }
//            }

//            CoroutineScope(Dispatchers.IO).async {
//                val response = service.aboutUsFunction(lang.toString() , "1")
//                withContext(Dispatchers.Main) {
//                    try {
//                        if (response.isSuccessful) {
//                            if (response.body()!!.status == 1) {
//                                //Do something with response e.g show to the UI.
//                                phoneCompany.value = response.body()!!.data.phone
//                                addressCompany.value = response.body()!!.data.address
//                                emailCompany.value = response.body()!!.data.email
//                                facebookLink.value = response.body()!!.data.facebook
//                                snapchatLink.value = response.body()!!.data.snap
//                                twitterLink.value = response.body()!!.data.twitter
//                                instagramLink.value = response.body()!!.data.instagram
//                                loader.value = false
//                            } else if (response.body()!!.status == 2) {
//                                Snackbar.make(v, response.body()?.message!!, Snackbar.LENGTH_SHORT)
//                                    .show()
//                                loader.value = false
//                            } else if (response.body()!!.status == 0) {
//                                Snackbar.make(
//                                    v,
//                                    "${response.body()!!.message}",
//                                    Snackbar.LENGTH_SHORT
//                                ).show()
//                                loader.value = false
//                                //baseFragment.dismissProgressDialog()
//                            } else {
//
//                            }
//                        }
//
//                    } catch (e: HttpException) {
//                        Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT).show();
//                        //baseFragment.dismissProgressDialog()
//
//                    } catch (e: Throwable) {
//                        Snackbar.make(v, "Ooops: Something else went wrong", Snackbar.LENGTH_SHORT)
//                            .show();
//                        //baseFragment.dismissProgressDialog()
//                    }
//                }
//            }
        }catch (e:Exception)
        {
            Snackbar.make(v, e.message.toString(), Snackbar.LENGTH_SHORT).show();

        }
    }


    fun sendData(v: View, emailLayout: TextInputLayout, phoneLayout: TextInputLayout, messageLayout: TextInputLayout) {


        if(validate(v ,emailLayout,phoneLayout,messageLayout))
            return


        if( phone.value?.isNotEmpty()!! && email.value?.isNotEmpty()!! && message.value?.isNotEmpty()!!) {
            //baseFragment.showProgressDialog(v.context,v.resources.getString(R.string.login),v.resources.getString(R.string.pleaseWaitUntilLogin),false)
            Log.d("HERE" , "HERE")
            loader.value = true


            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )





            CoroutineScope(Dispatchers.IO).async {
                runCatching {
                    serviceGeneral.contactUsFunction(lang.toString() , "1" , phone.value!! , message.value!! , email.value!!  )
                }.onSuccess {
//                    emitUiState(movies = Event(it.movies))
                    withContext(Dispatchers.Main) {
                        if (it.body()!!.status == 1) {
                            Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT).show();
                            phone.value = ""
                            message.value = ""
                            email.value = ""
                            loader.value = false
                        } else if (it.body()!!.status == 2) {
                            Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                .show()
                            loader.value = false
                        } else if (it.body()!!.status == 0) {
                            Snackbar.make(
                                v,
                                "${it.body()!!.message}",
                                Snackbar.LENGTH_SHORT
                            ).show()
                            loader.value = false
                            //baseFragment.dismissProgressDialog()
                        } else {

                        }
                    }
                }.onFailure {

                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()
                }
            }



//            val service = ApiClient.makeRetrofitServiceGeneral()
//            CoroutineScope(Dispatchers.IO).launch {
//                val response = service.contactUsFunction(lang.toString() , "1" , phone.value!! , message.value!! , email.value!!  )
//                withContext(Dispatchers.Main) {
//                    try {
//
//                        if(response.isSuccessful)
//                        {
//                            if (response.body()!!.status ==1 ) {
//                                //Do something with response e.g show to the UI.
//                                Snackbar.make(v, response.body()?.message!!, Snackbar.LENGTH_SHORT).show();
//                                //baseFragment.dismissProgressDialog()
////                                Snackbar.make(v, response.message(), Snackbar.LENGTH_SHORT).show();
//                                phone.value= ""
//                                message.value = ""
//                                email.value = ""
//                                loader.value = false
//
//                            } else if (response.body()!!.status == 2) {
//                                Snackbar.make(v, response.body()?.message!!, Snackbar.LENGTH_SHORT)
//                                    .show()
//                                loader.value = false
//                            } else if (response.body()!!.status == 0) {
//                                Snackbar.make(
//                                    v,
//                                    "${response.body()!!.message}",
//                                    Snackbar.LENGTH_SHORT
//                                ).show()
//                                loader.value = false
//                                //baseFragment.dismissProgressDialog()
//                            } else {
//
//                            }
//                        }
//                    } catch (e: HttpException) {
//                        Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT).show();
//                        //baseFragment.dismissProgressDialog()
//
//                    }
//                    catch (e: Throwable) {
//                        Snackbar.make(v, "Ooops: Something else went wrong ${e.message}", Snackbar.LENGTH_SHORT).show();
//                        //baseFragment.dismissProgressDialog()
//
//                    }
//                }
//            }
        }
        else
            Snackbar.make(v, v.resources.getString(R.string.pleaseInsertAllData), Snackbar.LENGTH_SHORT).show();



    }

    fun validate(v: View,  emailLayout: TextInputLayout, phoneLayout: TextInputLayout,  messageLayout: TextInputLayout): Boolean{
        var cancel : Boolean = false
        if (checkEmpty(messageLayout , message.value.toString(),  v.resources.getString(R.string.required)))
        {
            cancel = true
        }
        if (checkEmpty(phoneLayout , phone.value.toString(), v.resources.getString(R.string.required)))
        {
            cancel = true
        }
        if (checkEmpty(emailLayout , email.value.toString(), v.resources.getString(R.string.required)))
        {
            cancel = true
        }
        else if (checkValidEmail(emailLayout , email.value.toString(), v.resources.getString(R.string.notValidEmail)))
        {
            cancel = true
        }


        if (cancel)
            return  true
        else
            return false
    }

}