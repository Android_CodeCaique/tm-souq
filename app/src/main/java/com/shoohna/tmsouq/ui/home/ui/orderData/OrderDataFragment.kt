package com.shoohna.tmsouq.ui.home.ui.orderData

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.shoohna.tmsouq.databinding.FragmentOrderDataBinding
import com.shoohna.tmsouq.pojo.responses.MyOrderProductsProduct
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 */
class OrderDataFragment : Fragment() {

    lateinit var binding: FragmentOrderDataBinding

//    var finalData= MutableLiveData<List<ProductEntity>>()
    var myOrdersData= MutableLiveData<List<MyOrderProductsProduct>>()
    private val orderDataViewModel: OrderDataViewModel by inject()   // 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentOrderDataBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val model = ViewModelProvider(this).get(OrderDataViewModel::class.java)
        binding.vm = orderDataViewModel

        arguments?.let { val safeArgs = OrderDataFragmentArgs.fromBundle(it)
            orderDataViewModel.shopId.value = safeArgs.shopId
        }

        orderDataViewModel.geMyOrderProductsList(requireActivity()).observe(viewLifecycleOwner, Observer {

            myOrdersData.value = it

            binding.orderStatusRecycvlerViewId.adapter =
                OrderDataRecyclerAdapter(myOrdersData, requireActivity())

        })




//        model.loadDataFromRoom(activity!!)
//        model.finalDataList.observe(viewLifecycleOwner, Observer {
//            finalData.value = it
//            binding.recietRecyclerViewId.adapter =RecietRecyclerViewAdapter(finalData, context?.applicationContext)
//        })

        return  binding.root
    }

}
