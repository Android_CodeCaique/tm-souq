package com.shoohna.tmsouq.ui.home.ui.productDetails

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.tmsouq.databinding.FragmentProductDetailsBinding
import com.shoohna.tmsouq.databinding.SizeItemRowBinding
import com.shoohna.tmsouq.pojo.responses.ProductSize

class SizeRecyclerViewAdapter (val data: LiveData<List<ProductSize>>,
                               private val context: Context?,
                               val viewModel: ProductDetailsViewModel,
                               var lifecycleOwner: LifecycleOwner,
                               var MainBinding : FragmentProductDetailsBinding) : RecyclerView.Adapter<SizeRecyclerViewAdapter.ViewHolder>() {

    var makeLoop = MutableLiveData<Boolean>(false)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            SizeItemRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),makeLoop
        )
    }

    override fun getItemCount(): Int {
        return data.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data.value!![position] , this.viewModel , context,MainBinding)
        makeLoop.observe(lifecycleOwner, Observer {
            if(it)
            {
                loopIsChecked(data.value!!)
            }
        })
    }

    fun loopIsChecked(dataList: List<ProductSize>)
    {
        for (i in dataList) {
            i.isChecked = false
        }
        makeLoop.value = false
    }

    class ViewHolder(private var binding: SizeItemRowBinding, var makeLoop:MutableLiveData<Boolean>) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductSize, viewModel: ProductDetailsViewModel , context: Context? , MainBinding: FragmentProductDetailsBinding) {
            binding.model = item
            binding.executePendingBindings()
            binding.sizeConstraintId.setOnClickListener {
                Log.i("Constraint Clicked Size","True")
//                Toast.makeText(context,context?.getString(R.string.sizeSelected), Toast.LENGTH_SHORT).show()

                viewModel.sizeId.value = item.id
                viewModel.sizeName.value = item.name

                makeLoop.value = true
                item.isChecked = true
                MainBinding.include2.sizeRecyclerViewId.adapter?.notifyDataSetChanged()
            }
        }

    }

}