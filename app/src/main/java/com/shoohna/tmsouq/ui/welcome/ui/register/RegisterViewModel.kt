package com.shoohna.tmsouq.ui.welcome.ui.register

import ApiClient.sharedHelper
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.auth
import com.shoohna.tmsouq.ui.home.MainActivity
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.util.concurrent.TimeUnit


class RegisterViewModel(private val serviceGeneral: auth) : BaseViewModel() {

    var fName = MutableLiveData<String>("")
    var lName = MutableLiveData<String>("")
    var phone = MutableLiveData<String>("")
    var password = MutableLiveData<String>("")
    var confirmPasssword = MutableLiveData<String>("")
    var email = MutableLiveData<String>("")
    var baseFragment: BaseFragment = BaseFragment()
    var loader = MutableLiveData<Boolean>(false)


    fun register(v: View , fNameLayout: TextInputLayout , lNameLayout: TextInputLayout , emailLayout:TextInputLayout, phoneLayout:TextInputLayout, passwordLayout:TextInputLayout, confirmPasswordLayout:TextInputLayout )
    {


        if(validate(v ,fNameLayout,lNameLayout,emailLayout,phoneLayout,passwordLayout,confirmPasswordLayout))
            return


        if(fName.value?.isNotEmpty()!! && lName.value?.isNotEmpty()!! && phone.value?.isNotEmpty()!! && password.value?.isNotEmpty()!! && confirmPasssword.value?.isNotEmpty()!!) {
            //baseFragment.showProgressDialog(v.context,v.resources.getString(R.string.login),v.resources.getString(R.string.pleaseWaitUntilLogin),false)
            Log.d("HERE" , "HERE")
            loader.value = true



            val sharedHelper : SharedHelper = SharedHelper()
            val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
            val token :String? = FirebaseInstanceId.getInstance().token
            try {



                CoroutineScope(Dispatchers.IO).async {
                    runCatching {
                        serviceGeneral.registerFunction(
                            lang.toString() ,
                            phone.value!!,
                            email.value!!,
                            fName.value!!,
                            lName.value!!,
                            password.value!!,
                            confirmPasssword.value!!,
                            "1",
                            token
                        )
                    }.onSuccess {
                        withContext(Dispatchers.Main) {
                            try {
                                Log.d("HHHHHH", "HHHH" + it)
                                if (it.isSuccessful) {
                                    Log.d("BEBO",it.body()!!.status.toString())
                                    if (it.body()!!.status == 1) {
//                                        Snackbar.make(v, it.message(), Snackbar.LENGTH_SHORT)
//                                            .show();
                                        phone.value = ""
                                        password.value = ""
                                        confirmPasssword.value = ""
                                        fName.value = ""
                                        lName.value = ""
                                        email.value = ""
                                        sharedHelper.putKey(
                                            v.context,
                                            Constants.getToken(),
                                            it.body()!!.data.token
                                        )
                                        Log.i(
                                            "SharedToken",
                                            sharedHelper.getKey(v.context, Constants.getToken())
                                        )
                                        sharedHelper.putKey(
                                            v.context,
                                            "USER_ID",
                                            it.body()!!.data.id.toString()
                                        )
                                        sharedHelper.putKey(
                                            v.context,
                                            "USER_PHOTO",
                                            it.body()!!.data.image.toString()
                                        )
                                        sharedHelper.putKey(
                                            v.context,
                                            "USER_NAME",
                                            it.body()!!.data.frist_name
                                        )
                                        sharedHelper.putKey(
                                            v.context,
                                            "USER_EMAIL",
                                            it.body()!!.data.email
                                        )

                                        sharedHelper.putKey(v.context, "OPEN", "OPEN")


                                        loader.value = false


                                        val intent: Intent =
                                            Intent(v!!.context, MainActivity::class.java)
                                        v!!.context.startActivity(intent)
                                        (v.context as Activity).finish()

                                    } else if (it.body()!!.status == 2) {
                                        Snackbar.make(
                                            v,
                                            it.body()?.message!!,
                                            Snackbar.LENGTH_SHORT
                                        )
                                            .show()
                                        loader.value = false
                                    } else if (it.body()!!.status == 0) {
                                        Snackbar.make(
                                            v,
                                            "${it.body()!!.message}",
                                            Snackbar.LENGTH_SHORT
                                        ).show()
                                        loader.value = false
                                        //baseFragment.dismissProgressDialog()
                                    } else {

                                    }
                                } else {
                                    loader.value = false
                                    Snackbar.make(v, it.message(), Snackbar.LENGTH_SHORT)
                                        .show()
                                    Log.d("AMMMMM", "ERROR")

                                }

                            } catch (e: HttpException) {
                                Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                                    .show();
                                //baseFragment.dismissProgressDialog()
                                loader.value = false

                            } catch (e: Throwable) {
                                // Snackbar.make(v, "Ooops: Something else went wrong ${e.message}", Snackbar.LENGTH_SHORT).show();
                                //baseFragment.dismissProgressDialog()
                                loader.value = false
                                Log.d("HHHHHH", "HHHH" + e.message)

                            }
                        }
                    }.onFailure {
                        Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                    }
                }



//                val service = ApiClient.makeRetrofitService()
//                CoroutineScope(Dispatchers.IO).async {
//                    val response = service.registerFunction(
//                        lang.toString() ,
//                        phone.value!!,
//                        email.value!!,
//                        fName.value!!,
//                        lName.value!!,
//                        password.value!!,
//                        confirmPasssword.value!!,
//                        "1",
//                        token
//                    )
//
//                }
            }catch (e:Exception)
            {
                Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                    .show();
                loader.value = false
            }
        }
        else
            Snackbar.make(v, v.resources.getString(R.string.pleaseInsertAllData), Snackbar.LENGTH_SHORT).show();
    }

    fun validate(v: View ,fNameLayout: TextInputLayout , lNameLayout: TextInputLayout , emailLayout:TextInputLayout, phoneLayout:TextInputLayout, passwordLayout:TextInputLayout, confirmPasswordLayout:TextInputLayout): Boolean{
        var stop = false
        if (checkEmpty(confirmPasswordLayout , confirmPasssword.value.toString(), v.resources.getString(R.string.required)))
        {
            stop = true
        }
        else if (checkValidPassword(confirmPasswordLayout , confirmPasssword.value.toString(), v.resources.getString(R.string.notValidPassword)))
        {
            stop = true
        }
        else if (checkEquals(confirmPasswordLayout , confirmPasssword.value.toString(), password.value.toString(), v.resources.getString(R.string.notMatching)))
        {
            stop = true
        }

        if (checkEmpty(passwordLayout , password.value.toString(), v.resources.getString(R.string.required)))
        {
            stop = true
        }
        else if (checkValidPassword(passwordLayout , password.value.toString(), v.resources.getString(R.string.notValidPassword)))
        {
            stop = true
        }

        if (checkEmpty(phoneLayout , phone.value.toString(), v.resources.getString(R.string.required)))
        {
            stop = true
        }
        if (checkEmpty(emailLayout , email.value.toString(), v.resources.getString(R.string.required)))
        {
            stop = true
        }
        else if (checkValidEmail(emailLayout , email.value.toString(), v.resources.getString(R.string.notValidEmail)))
        {
            stop = true
        }
        if (checkEmpty(lNameLayout , lName.value.toString(), v.resources.getString(R.string.required)))
        {
            stop = true
        }
        if(checkEmpty(fNameLayout , fName.value.toString(), v.resources.getString(R.string.required)))
        {
            stop = true
        }





        if(stop)
        {
            return  true
        }
        else
        {
            return false
        }

    }

    fun getActivationCode(v: View , phone : String){

        Log.d("HEREPHONE" , "HEREPHONE : "+phone)
        val activity = v!!.context as Activity
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+20"+phone, // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            activity , // Activity (for callback binding)
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onVerificationCompleted(credential: PhoneAuthCredential) {

                    Log.d("PHONE", "onVerificationCompleted:$credential")

                    //signInWithPhoneAuthCredential(credential)
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    Log.w("PHONE", "onVerificationFailed", e)

                    if (e is FirebaseAuthInvalidCredentialsException) {

                    } else if (e is FirebaseTooManyRequestsException) {

                    }
                }
                override fun onCodeSent(
                    verificationId: String,
                    token: PhoneAuthProvider.ForceResendingToken
                )
                {
                    loader.value = false
                    Log.d("PHONE", "onCodeSent:$verificationId")
                    sharedHelper.putKey(v!!.context , "ActiveCode"  , verificationId)
//                    v?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_registerFragment_to_verifyCodeFragment) }
                }
            })
    }

    fun register(context: Context , email : String , fName : String , lName : String , image : String ) {
        val token :String? = FirebaseInstanceId.getInstance().token
        loader.value = true

        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(context , "MyLang" )
//        val service = ApiClient.makeRetrofitService()
        Log.i("RegisterData1","$email - $fName - $lName - $image")
        CoroutineScope(Dispatchers.IO).async {
            runCatching { serviceGeneral.registerSocialFunction(lang.toString() , email , "" , "" , fName , lName , "" , token ) }
                .onSuccess {
                    withContext(Dispatchers.Main) {
                        try {

                            if (it.isSuccessful) {
                                if (it.body()!!.status == 1) {
                                    //Do something with response e.g show to the UI.

//                                    Snackbar.make(
//                                        view!!.rootView,
//                                        it.body()?.message!!,
//                                        Snackbar.LENGTH_SHORT
//                                    ).show();
//                                    //baseFragment.dismissProgressDialog()
//                                    Snackbar.make(
//                                        view!!.rootView,
//                                        it.message(),
//                                        Snackbar.LENGTH_SHORT
//                                    ).show();
                                    Toast.makeText(context,it.body()!!.message,Toast.LENGTH_SHORT).show()
                                    loader.value = false
                                    Log.i("RegisterData2",it.message())
                                    Log.i("RegisterData21",it.body().toString())

                                } else if (it.body()!!.status == 2) {
//                                    Snackbar.make(
//                                        view!!.rootView,
//                                        it.body()?.message!!,
//                                        Snackbar.LENGTH_SHORT
//                                    ).show();
                                    Toast.makeText(context,it.body()?.message!!,Toast.LENGTH_SHORT).show()
                                    loader.value = false
                                    Log.i("RegisterData3",it.message())
                                    Log.i("RegisterData31",it.body().toString())


                                } else {
//                                    Snackbar.make(
//                                        view!!.rootView,
//                                        it.message(),
//                                        Snackbar.LENGTH_SHORT
//                                    ).show();
                                    //baseFragment.dismissProgressDialog()
                                    Toast.makeText(context,it.body()!!.message,Toast.LENGTH_SHORT).show()
                                    loader.value = false
                                    Log.i("RegisterData4",it.message())
                                    Log.i("RegisterData41",it.body().toString())

                                }
                            } else {
                                Log.i("RegisterData5",it.message())
                                Log.i("RegisterData51",it.body().toString())

                            }

                        } catch (e: HttpException) {
//                            Snackbar.make(
//                                view!!.rootView,
//                                "Exception ${e.message}",
//                                Snackbar.LENGTH_SHORT
//                            ).show();
                            //baseFragment.dismissProgressDialog()
                            Toast.makeText(context,"Exception ${e.message}",Toast.LENGTH_SHORT).show()
                            loader.value = false
                            Log.i("RegisterData6","Exception ${e.message}")

                        } catch (e: Throwable) {

                            Toast.makeText(context,"Ooops: Something else went wrong",Toast.LENGTH_SHORT).show()

                            Log.e("error 342", "error 342 " + e.message)
                            loader.value = false
                            Log.i("RegisterData7","Exception ${e.message}")

                            //baseFragment.dismissProgressDialog()
                        }
                    }
                }.onFailure {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_SHORT).show()
                }
        }

    }

    private fun activeAccountApi(v : View){
        val sharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )
//        val service = ApiClient.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).async {
            runCatching { serviceGeneral.ActiveFunction(lang.toString(), "Bearer ${sharedHelper.getKey(v.context, "PHONETOKEN")}") }
                .onSuccess {
                    withContext(Dispatchers.Main) {
                        try {

                            if (it.isSuccessful) {
                                Log.d("RESPONSE", it.toString())
                                if (it.body()!!.status == 1) {
                                    loader.value = false
                                    Snackbar.make(
                                        v,
                                        it.body()?.message!!,
                                        Snackbar.LENGTH_SHORT
                                    )
                                        .show()
                                } else if (it.body()!!.status == 2) {
                                    Snackbar.make(
                                        v,
                                        it.body()?.message!!,
                                        Snackbar.LENGTH_SHORT
                                    )
                                        .show()
                                    loader.value = false

                                } else if (it.body()!!.status == 0) {
                                    Snackbar.make(
                                        v,
                                        "${it.body()!!.message}",
                                        Snackbar.LENGTH_SHORT
                                    ).show()
                                    loader.value = false

                                    //baseFragment.dismissProgressDialog()
                                } else {

                                }
                            } else {
                            }

                        } catch (e: HttpException) {
                            Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT)
                                .show();
                            //baseFragment.dismissProgressDialog()
                            loader.value = false

                        } catch (e: Throwable) {
                            Log.d("ERROR", "ERRORE" + e.message)
                            //  Snackbar.make(v, "Ooops: Something else went wrong", Snackbar.LENGTH_SHORT).show();
                            //baseFragment.dismissProgressDialog()
                            loader.value = false
                        }
                    }
                }.onFailure {
                    Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

                }
        }
    }

}