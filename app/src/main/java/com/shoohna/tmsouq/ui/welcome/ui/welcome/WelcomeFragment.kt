package com.shoohna.tmsouq.ui.welcome.ui.welcome

import ApiClient.sharedHelper
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FragmentWelcomeBinding
import com.shoohna.tmsouq.ui.home.MainActivity
import com.shoohna.tmsouq.util.base.SharedHelper


class WelcomeFragment : Fragment() {
    lateinit var binding:FragmentWelcomeBinding
    private lateinit var nav: NavController

    // variable to track event time
    private var mLastClickTime: Long = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentWelcomeBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)

        Log.i("SharedHelperWelcome",sharedHelper.getKey(requireActivity(), "GoToLogin").toString())
        if(sharedHelper.getKey(requireActivity(), "GoToLogin").equals("true")) {
            Log.i("SharedHelperWelcome2",sharedHelper.getKey(requireActivity(), "GoToLogin").toString())

//            view?.let { it1 ->
//                Navigation.findNavController(it1)
//                    .navigate(R.id.action_welcomeFragment_to_loginFragment)
//            }

            nav.navigate(R.id.action_welcomeFragment_to_loginFragment)

//            if (nav.graph.startDestination != nav.currentDestination?.id) {
//                nav.popBackStack(R.id.splashFragment, false)
//                nav.navigate(R.id.action_welcomeFragment_to_loginFragment)
//            } else {
//                nav.navigate(R.id.action_welcomeFragment_to_loginFragment)
//            }
        }


        binding.signInBtnId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.loginFragment)
        }

        binding.signUpBtnId.setOnClickListener {
            nav = Navigation.findNavController(requireActivity(),R.id.nav_host_fragment)
            nav.navigate(R.id.registerFragment)
        }

        binding.skipTxtViewId.setOnClickListener {
            startActivity(Intent(activity,MainActivity::class.java))
            activity?.finish()
            val sharedHelper = SharedHelper()
            sharedHelper.putKey(requireActivity(), "OPEN", "OPEN")
        }


        return binding.root
    }





}
