package com.shoohna.tmsouq.ui.home.ui.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.e_commercehappytimes.ui.home.ui.home.HomeViewModel
import com.shoohna.tmsouq.databinding.BottomSheetLayoutBinding
import com.shoohna.tmsouq.databinding.FilterItemRowBinding
import com.shoohna.tmsouq.pojo.responses.FilerMainCategorieData

class FilterRecyclerViewAdapter (private var dataList: LiveData<List<FilerMainCategorieData>>,
                                 private val context: Context?,
                                 var homeViewModel: HomeViewModel, var lifecycleOwner: LifecycleOwner, var MainBinding : BottomSheetLayoutBinding
) : RecyclerView.Adapter<FilterRecyclerViewAdapter.ViewHolder>() {
    var makeLoop = MutableLiveData<Boolean>(false)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FilterItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false),makeLoop
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position] , homeViewModel , context,MainBinding)
        makeLoop.observe(lifecycleOwner, Observer {
            if(it)
            {
                loopIsChecked(dataList.value!!)
            }
        })
    }

    fun loopIsChecked(dataList: List<FilerMainCategorieData>)
    {
        for (i in dataList) {
            i.isChecked = false
        }
        makeLoop.value = false
    }

    class ViewHolder(private var binding: FilterItemRowBinding , var makeLoop:MutableLiveData<Boolean>) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: FilerMainCategorieData , homeViewModel: HomeViewModel , context: Context?,MainBinding : BottomSheetLayoutBinding) {
            binding.filterModel = item
            binding.executePendingBindings()
            binding.itemConstraintID.setOnClickListener {
                homeViewModel.filterCatId.value = item.id
                makeLoop.value = true
                item.isChecked = true
                MainBinding.recyclerView.adapter?.notifyDataSetChanged()
            }
        }

    }

}