package com.shoohna.tmsouq.ui.home.ui.favorite

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.shoohna.tmsouq.pojo.model.ProductListModel


class FavoriteViewModelFactory(var productListModel: MutableLiveData<ProductListModel>) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavoriteViewModel::class.java)) {
//            return FavoriteViewModel(productListModel) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}