package com.shoohna.tmsouq.ui.home.ui.moreSetting

import ApiClient.sharedHelper
import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FragmentMoreBinding
import com.shoohna.tmsouq.databinding.FragmentMoreSettingBinding
import com.shoohna.tmsouq.pojo.responses.CurrenciesData
import com.shoohna.tmsouq.ui.welcome.WelcomeActivity
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.LanguageHelper
import com.shoohna.shoohna.util.base.BaseFragment
import org.koin.android.ext.android.inject
import java.lang.Exception
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class MoreSettingFragment : Fragment(), AdapterView.OnItemSelectedListener {
    lateinit var binding: FragmentMoreSettingBinding
    lateinit var baseFragment:BaseFragment
//    lateinit var viewModel:MoreSettingViewModel
    val languageHelper = LanguageHelper ()
    private val moreSettingViewModel: MoreSettingViewModel by inject()   // 1

    lateinit var currencyData: List<CurrenciesData>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentMoreSettingBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
        baseFragment = BaseFragment()
//        viewModel = ViewModelProvider(this).get(MoreSettingViewModel::class.java)
        binding.vm = moreSettingViewModel
//        binding.spinner.onItemSelectedListener = this
//        activity?.let { ArrayAdapter.createFromResource(it, R.array.language, android.R.layout.simple_spinner_item)
//            .also { adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                binding.spinner.adapter = adapter } }




        try {
            binding.nameTxtId.text = sharedHelper.getKey(requireActivity(),"USER_NAME")
            binding.emailTxtId.text = sharedHelper.getKey(requireActivity(),"USER_EMAIL")
//            Picasso.get().load(sharedHelper.getKey(activity!!, "USER_PHOTO")).into(binding.circleImageView)
            if(sharedHelper.getKey(requireActivity(), "USER_PHOTO").toString() == "http://api.shoohna.com/images/1.png")
                Glide.with(requireActivity()).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
            else
                Glide.with(requireActivity()).load(sharedHelper.getKey(requireActivity(), "USER_PHOTO")).error(R.drawable.ic_profile_upload).into(binding.circleImageView)

        }catch (e: Exception) {}


        binding.notificationSwitchId.setOnCheckedChangeListener { view, isChecked ->
            if(isChecked)
            {
                if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                    baseFragment.showAlert(requireActivity())
                }else {
                    moreSettingViewModel.notification.value = 1
                    moreSettingViewModel.changeNotification(requireActivity())
                }
            }
            else
            {
                if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                    baseFragment.showAlert(requireActivity())
                }else {
                    moreSettingViewModel.notification.value = 0
                    moreSettingViewModel.changeNotification(requireActivity())
                }
            }

        }

        binding.messageSwitchId.setOnCheckedChangeListener { view, isChecked ->
            if(isChecked)
            {
                if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                    baseFragment.showAlert(requireActivity())

                }else {
                    moreSettingViewModel.message.value = 1
                    moreSettingViewModel.changeMessage(requireActivity())
                }
            }
            else
            {
                if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                    baseFragment.showAlert(requireActivity())
                }else {
                    moreSettingViewModel.message.value = 0
                    moreSettingViewModel.changeMessage(requireActivity())
                }
            }

        }

        moreSettingViewModel.getCurrency(requireActivity())

        if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isNotEmpty()!!){
            moreSettingViewModel.getUserSetting(requireActivity())
            moreSettingViewModel.currentUserSetting.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                Log.i("SettingMessage",moreSettingViewModel.currentUserSetting.value?.message.toString())
                Log.i("SettingNotification",moreSettingViewModel.currentUserSetting.value?.notification.toString())
                binding.notificationSwitchId.isChecked = it.notification != 0
                binding.messageSwitchId.isChecked = it.message != 0

            })

        }


        handleCurrencySpinner()
        handleLanguageSpinner()


        return binding.root
    }

    private fun handleLanguageSpinner() {
//        val languageHelper : LanguageHelper = LanguageHelper();

        try {
            binding.spinner.setItems("Language's", "English", "العربيه")
            binding.spinner.setOnItemSelectedListener { view, position, id, item ->
                if (position == 1) {
                    Log.i("Lang", "1")
                    if (sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!) {
                        Log.i("Lang", "2")
                        recreate("en")

//                        baseFragment.showAlert(activity!!)
//                        sharedHelper.putKey(activity!! , "MyLang" , "en")
//                        languageHelper.ChangeLang(this.resources , "en")
//                        val intent = Intent(context , WelcomeActivity::class.java)
//                        (context as Activity ).finish()
//                        activity!!.startActivity(intent)
                        Log.i("Lang", "3")
                    } else {
//                        setLocate("en")
                        languageHelper.ChangeLang(this.resources , "en")

                        moreSettingViewModel.lang.value = "en"
                        moreSettingViewModel.changeLang(requireActivity())
                    }
                }
                if (position == 2) {
                    if (sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!) {
//                        baseFragment.showAlert(activity!!)
                        recreate("ar")
//                        sharedHelper.putKey(activity!! , "MyLang" , "ar")
//                        languageHelper.ChangeLang(this.resources , "ar")
//                        val intent = Intent(context , WelcomeActivity::class.java)
//                        (context as Activity ).finish()
//                        activity!!.startActivity(intent)
                    } else {
//                        setLocate("ar")
                        languageHelper.ChangeLang(this.resources , "ar")

                        moreSettingViewModel.lang.value = "ar"
                        moreSettingViewModel.changeLang(requireActivity())
                    }
                }
            }
        }catch (e:Exception) {
            Toast.makeText(requireActivity(), e.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setLocate(Lang: String) {

        val locale = Locale(Lang)

        Locale.setDefault(locale)

        val config = Configuration()

        config.locale = locale
        activity?.resources?.updateConfiguration(config, requireActivity().resources.displayMetrics)

//        val editor = activity?.getSharedPreferences("Settings", Context.MODE_PRIVATE)?.edit()
//        editor?.putString("MyLang", Lang)
//        editor?.apply()
        languageHelper.ChangeLang(this.resources , Lang)
        sharedHelper.putKey(requireActivity(),"MyLang",Lang)

    }

    fun handleCurrencySpinner()
    {
//        val arrayAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, viewModel.currencySettingName)
//        binding.spinnerCurrency.adapter = arrayAdapter
//        binding.spinnerCurrency.onItemSelectedListener =
//            object : AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                    Log.i("onItemSelected", position.toString())
//                    Toast.makeText(activity!!, position.toString(), Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // Code to perform some action when nothing is selected
//                }
//            }
//        binding.spinnerCurrency.onItemSelectedListener = this
//
//        activity?.let { ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, viewModel.currencySettingName)
//            .also { adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                binding.spinnerCurrency.adapter = adapter } }
//                    object : AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                    if(position == 1) {
//                        Log.i("onItemSelected", position.toString())
//                        Toast.makeText(activity!!, position.toString(), Toast.LENGTH_SHORT).show()
//                    }
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // Code to perform some action when nothing is selected
//                }
//            }

        binding.spinnerCurrency.setItems(moreSettingViewModel.currencySettingName)
        binding.spinnerCurrency.setOnItemSelectedListener { view, position, id, item ->
//            Toast.makeText(activity!!,position.toString(),Toast.LENGTH_SHORT).show()
            Log.i("spinnerCurrency",item.toString())
            if (sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!) {
                baseFragment.showAlert(requireActivity())
            } else {
                moreSettingViewModel.currency_id.value = position
                moreSettingViewModel.changeCurrency(requireActivity())
            }
        }
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if(parent?.id == R.id.spinner) {
            if (position == 1) {
                setLocate("en")
                if (sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!) {
                    requireActivity().recreate()
                } else {
                    moreSettingViewModel.lang.value = "en"
                    moreSettingViewModel.changeLang(requireActivity())
                }
            }
            if (position == 2) {
                setLocate("ar")
                if (sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!) {
                    requireActivity().recreate()
                } else {
                    moreSettingViewModel.lang.value = "ar"
                    moreSettingViewModel.changeLang(requireActivity())
                }
            }
        }
        if(parent?.id == R.id.spinnerCurrency)
        {    Log.i("onItemSelected", "1")
            if(position == 1)
            {
                Log.i("onItemSelected", "1")
                Toast.makeText(requireActivity(), "Worked", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun recreate(lang:String)
    {
        sharedHelper.putKey(requireActivity(), "OPEN", "OPEN")
        sharedHelper.putKey(requireActivity() , "MyLang" , lang)

        languageHelper.ChangeLang(this.resources , lang)

        val intent = Intent(context , WelcomeActivity::class.java)
        (context as Activity ).finish()
        requireActivity().startActivity(intent)
    }
}



