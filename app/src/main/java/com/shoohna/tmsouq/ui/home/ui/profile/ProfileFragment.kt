package com.shoohna.tmsouq.ui.home.ui.profile

import android.app.Activity
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FragmentFavoriteBinding
import com.shoohna.tmsouq.databinding.FragmentProfileBinding
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import org.koin.android.ext.android.inject

/**binding.profileImageId
 * A simple [Fragment] subclass.
 */
class ProfileFragment : BaseFragment() {
    lateinit var binding: FragmentProfileBinding
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var nav: NavController
    private val profileViewModel: ProfileFragmentViewModel by inject()

    var language :String? = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

//        val viewModel = ViewModelProvider(this).get(ProfileFragmentViewModel::class.java)

        binding.vm = profileViewModel

        profileViewModel.loadData(binding.root , binding.profileImageId)

        sharedPreferences = context?.getSharedPreferences("Settings", Activity.MODE_PRIVATE)!!
        language = sharedPreferences.getString("MyLang", "")


        binding.editConstraintId.setOnClickListener {
            binding.changePasswordTxtId.visibility = View.VISIBLE
            binding.x.visibility = View.GONE
            binding.editConstraintId.visibility = View.GONE
            binding.correctImgId.visibility = View.VISIBLE
            binding.dontSaveImgId.visibility = View.VISIBLE
            binding.addImageBtnId.visibility = View.VISIBLE

            binding.fName.isEnabled = true
            binding.lName.isEnabled = true
            binding.phone.isEnabled = true
            binding.email.isEnabled = true


        }

        binding.dontSaveImgId.setOnClickListener {
            binding.changePasswordTxtId.visibility = View.GONE
            binding.x.visibility = View.VISIBLE
            binding.editConstraintId.visibility = View.VISIBLE
            binding.correctImgId.visibility = View.GONE
            binding.dontSaveImgId.visibility = View.GONE
            binding.addImageBtnId.visibility = View.GONE

            binding.fName.isEnabled = false
            binding.lName.isEnabled = false
            binding.phone.isEnabled = false
            binding.email.isEnabled = false

        }

        binding.changePasswordTxtId.setOnClickListener {

            nav = Navigation.findNavController(requireActivity() ,R.id.nav_host_fragment)
            nav.navigate(R.id.changePasswordProfileFragment)

            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

        }

        if(language.equals("ar"))
            binding.addImageBtnId.translationX = 20f
        else
            binding.addImageBtnId.translationX = -20f




        binding.addImageBtnId.setOnClickListener {
            val sharedHelper = SharedHelper()
            sharedHelper.putKey(requireActivity(), "OPEN", "OPEN")
            showPictureDialog(binding.profileImageId)
        }
        return binding.root
    }


}
