package com.shoohna.tmsouq.ui.home

import ApiClient.sharedHelper
import android.app.Dialog
import android.content.*
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.shoohna.e_commercehappytimes.ui.home.ui.home.HomeFragmentDirections
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.ActivityMainBinding
import com.shoohna.tmsouq.ui.welcome.WelcomeActivity
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.LanguageHelper
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener{
    lateinit var binding: ActivityMainBinding
    private lateinit var nav: NavController
    private lateinit var sharedPreferences: SharedPreferences
    lateinit var languageHelper : LanguageHelper
    var language :String? = ""
    lateinit var baseFragment : BaseFragment
    lateinit var model : MainActivityViewModel
    // For Forground
    private val mHandler: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            showNotificationDialog()
        }
    }

//    lateinit var viewModel:MainActivityViewModel

    private val mainActivityViewModel: MainActivityViewModel by inject()   // 1


    override fun onCreate(savedInstanceState: Bundle?) {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(mHandler, IntentFilter("FCM_MESSAGE"))


        super.onCreate(savedInstanceState)
//        sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE)


        language = sharedHelper.getKey(this, "MyLang" )

        languageHelper= LanguageHelper ()
        if (language.equals("ar")) {
            Log.i("LanguageShared","ar")
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL;
            languageHelper.ChangeLang(this.resources , "ar")
        } else if(language.equals("en")){
            Log.i("LanguageShared","en")

            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            languageHelper.ChangeLang(this.resources , "en")
        }



        baseFragment = BaseFragment()

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.vm = mainActivityViewModel
//        binding.vm = viewModel
//        viewModel.isShown.value = true
        nav = Navigation.findNavController(this,R.id.nav_host_fragment)
//        setUpController()

        iconHomeClicked()
//        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)


        var x : SharedHelper = SharedHelper()
        var token : String? =""
        token = this?.let { x.getKey(it, Constants.getToken()) }

//        if (token.isNullOrEmpty() || token.equals(""))
//        {
//            binding.bottomProfileConstraintId.visibility = View.GONE
//        }
//        else
//        {
//            binding.bottomProfileConstraintId.visibility = View.VISIBLE
//        }
        binding.bottomHomeConstraintId.setOnClickListener { iconHomeClickedFromHome() }
        binding.bottomCartConstraintId.setOnClickListener { iconCardClicked() }
        binding.bottomFavoriteConstraintId.setOnClickListener { iconFavoriteClicked() }
        binding.bottomMoreConstraintId.setOnClickListener { iconMoreClicked() }
        binding.bottomProfileImgId.setOnClickListener { iconProfileClicked() }
        binding.bottomNavigationConstraint.setOnClickListener { Log.i("bottomConstraint","clicked") }

        try {
            if(sharedHelper.getKey(this, "USER_PHOTO").toString() == "http://api.shoohna.com/images/1.png")
                Glide.with(this).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(binding.bottomProfileImgId)
            else
                Glide.with(this).load(sharedHelper.getKey(this, "USER_PHOTO")).error(R.drawable.ic_profile_upload).into(binding.bottomProfileImgId)

        }catch (e: Exception)
        {}

        binding.navViewId.setNavigationItemSelectedListener(this)
    }
    private fun setDataForNotification() {

        Log.d("HERE" , "HEREHOME")
        val id = intent.extras!!["redirect_id"].toString()
        val status : String = intent.extras!!["status"].toString()
        Log.d("LLL" , ":::"+status)

        if(status != null)
        {
            Log.d("error" , " ya welcome b success ")
            if (status.equals("1")) {
                Log.d("STATUS" , "1")
                if(nav.graph.startDestination != nav.currentDestination?.id) {
                    nav.popBackStack(R.id.homeFragment,false)
                    nav.navigate(R.id.action_homeFragment_to_notificationFragment)
                }
            } else if (status.equals("2")) {
                Log.d("STATUS" , "2")
                if(nav.graph.startDestination != nav.currentDestination?.id) {
                    nav.popBackStack(R.id.homeFragment,false)
                    mainActivityViewModel.productId.value = id.toInt()
                    mainActivityViewModel.loadProductData(applicationContext)
                    mainActivityViewModel.finalDataList.observe(this, Observer {
                        if(it.name.isNotEmpty()) {
                            this.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE

                            val action = HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(mainActivityViewModel.finalDataList.value!!.id,mainActivityViewModel.finalDataList.value!!.image,mainActivityViewModel.finalDataList.value!!.desc,mainActivityViewModel.finalDataList.value!!.price_product)
                            nav.navigate(action)
                        }
                    })



//                    nav.navigate(R.id.action_homeFragment_to_productDetailsFragment2)

//                    this.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
//
//                    val action = HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(item.id,item.image,item.desc,item.price_product)
//                    Navigation.findNavController(it).navigate(action)
                }
            } else if (status.equals("3")) {
                Log.d("Not Now" , "Not Now")
            } else {
                Log.d("Not Now" , "Not Now 22222222")
                Intent(this, WelcomeActivity::class.java)
            }
        }
        else
        {
            Log.d("error" , " ya welcome b error ")
        }
    }
    override fun onResume() {
        super.onResume()
        val sharedHelper = SharedHelper()
        sharedHelper.putKey(this ,"OPEN", "OPEN")

    }
    override fun onRestart() {
        super.onRestart()
        val sharedHelper = SharedHelper()
        sharedHelper.putKey(this ,"OPEN", "OPEN")
    }
    override fun onStop() {
        super.onStop()
        val sharedHelper = SharedHelper()
        sharedHelper.putKey(this ,"OPEN", "")
    }
    override fun onPause() {
        super.onPause()
        val sharedHelper = SharedHelper()
        sharedHelper.putKey(this ,"OPEN", "OPEN")
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler)
    }

    override fun onBackPressed() {
//        binding.bottomNavigationConstraint.setCurrentActiveItem(0)
//        iconHomeClicked()
        if (binding.drawer.isDrawerOpen(GravityCompat.START)) {
            binding.drawer.closeDrawer(GravityCompat.START);
        }else if(nav.currentDestination?.label?.equals("fragment_notification")!!
            || nav.currentDestination?.label?.equals("fragment_profile")!!
            || nav.currentDestination?.label?.equals("fragment_product")!!
            || nav.currentDestination?.label?.equals("fragment_more")!!
            || nav.currentDestination?.label?.equals("fragment_product_details")!!
            || nav.currentDestination?.label?.equals("fragment_cart")!!
            || nav.currentDestination?.label?.equals("fragment_favorite")!!
            || nav.currentDestination?.label?.equals("fragment_chat")!!
            || nav.currentDestination?.label?.equals("fragment_more_setting")!!
            || nav.currentDestination?.label?.equals("fragment_reciet")!! ){
            iconHomeClicked()
        }
        else
            super.onBackPressed()

    }

    fun activateHomeClicked()
    {
        binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_active)
        binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
        binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
        binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
    }

    fun iconHomeClickedFromHome()
    {
        activateHomeClicked()
        if (nav.graph.startDestination == nav.currentDestination?.id) {
            nav.navigate(R.id.action_homeFragment_self)
        } else {
            nav.popBackStack(R.id.homeFragment,false)
        }
    }

    private fun iconHomeClicked()
    {
//        nav.popBackStack(R.id.homeFragment,false)
        activateHomeClicked()
        nav.popBackStack(R.id.homeFragment,false)
    }

    fun iconCardClicked()
    {
        Log.i("iconCardClicked","true")
            binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
            binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_active)
            binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
            binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
            if (nav.graph.startDestination != nav.currentDestination?.id) {
                nav.popBackStack(R.id.homeFragment, false)
                nav.navigate(R.id.action_homeFragment_to_cartFragment)
            } else {
                nav.navigate(R.id.action_homeFragment_to_cartFragment)
            }

    }

    private fun iconFavoriteClicked()
    {
        if(sharedHelper.getKey(this,Constants.getToken())?.isEmpty()!!){
            baseFragment.showAlert(this)
        }else {
            binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
            binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
            binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_active)
            binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
            if (nav.graph.startDestination != nav.currentDestination?.id) {
                nav.popBackStack(R.id.homeFragment, false)
                nav.navigate(R.id.action_homeFragment_to_favoriteFragment)
            } else {
                nav.navigate(R.id.action_homeFragment_to_favoriteFragment)
            }
        }
    }

    private fun iconProfileClicked()
    {
        if(sharedHelper.getKey(this,Constants.getToken())?.isEmpty()!!){
            baseFragment.showAlert(this)
        }else {
            binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
            binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
            binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
            binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_inactive)
            if (nav.graph.startDestination != nav.currentDestination?.id) {
                nav.popBackStack(R.id.homeFragment, false)
                nav.navigate(R.id.action_homeFragment_to_profileFragment)
            } else {
                nav.navigate(R.id.action_homeFragment_to_profileFragment)
            }
        }
    }

    private fun iconMoreClicked()
    {
        binding.bottomHomeId.setBackgroundResource(R.drawable.ic_bottom_home_inactive)
        binding.bottomCartId.setBackgroundResource(R.drawable.ic_bottom_cart_inactive)
        binding.bottomFavoriteId.setBackgroundResource(R.drawable.ic_bottom_favorite_inactive)
        binding.bottomMoreId.setBackgroundResource(R.drawable.ic_bottom_more_active)
        if(nav.graph.startDestination != nav.currentDestination?.id) {
                    nav.popBackStack(R.id.homeFragment,false)
                    nav.navigate(R.id.action_homeFragment_to_moreFragment)
        } else { nav.navigate(R.id.action_homeFragment_to_moreFragment) }
    }

    private fun showNotificationDialog() {
        val Content: TextView
        val Title: TextView
        val Done: Button
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.foreground_notification_dialog)
        Title = dialog.findViewById(R.id.Title)
        Content = dialog.findViewById(R.id.Content)
        Done = dialog.findViewById(R.id.ShowPost)
        val sharedHelper = SharedHelper ()
        val TitleText: String? = sharedHelper.getKey(this, "Title")
        val ContentText: String? = sharedHelper.getKey(this, "Body")
        val IDText: String? = sharedHelper.getKey(this, "PostID")

        Title.text = TitleText
        Content.text = ContentText

        Done.setOnClickListener {
            dialog.dismiss()
            sharedHelper.putKey(this, "OPEN", "OPEN")
            if (sharedHelper.getKey(this, "Status").equals("1")) {
                Log.d("STATUS" , "1")
                if(nav.graph.startDestination != nav.currentDestination?.id) {
                    Log.d("GHO" , "1")
                    nav.popBackStack(R.id.homeFragment,false)
                    nav.navigate(R.id.action_homeFragment_to_notificationFragment)
                }
            } else if (sharedHelper.getKey(this, "Status").equals("2") ) {
                Log.d("STATUS" , "2")
                if(nav.graph.startDestination != nav.currentDestination?.id) {
                    Log.d("GHO" , "2")
                    nav.popBackStack(R.id.homeFragment, false)
                    mainActivityViewModel.productId.value = IDText!!.toInt()
                    mainActivityViewModel.loadProductData(applicationContext)
                    mainActivityViewModel.finalDataList.observe(this, Observer {
                        if (it.name.isNotEmpty()) {
                            this.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                                View.GONE

                            val action =
                                HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(
                                    mainActivityViewModel.finalDataList.value!!.id,
                                    mainActivityViewModel.finalDataList.value!!.image,
                                    mainActivityViewModel.finalDataList.value!!.desc,
                                    mainActivityViewModel.finalDataList.value!!.price_product
                                )
                            nav.navigate(action)
                        }
                    })
                }
            }  else if (sharedHelper.getKey(this, "Status").equals("3") ) {

            }
        }
        dialog.show()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        val id: Int = menuItem.itemId

        if(id == R.id.settingMenuId)
        {
            if (nav.graph.startDestination != nav.currentDestination?.id) {
                nav.popBackStack(R.id.homeFragment, false)
                nav.navigate(R.id.action_homeFragment_to_moreSettingFragment)
            } else {
                nav.navigate(R.id.action_homeFragment_to_moreSettingFragment)
            }
        }

        if(id == R.id.aboutUsMenuId)
        {
            if (nav.graph.startDestination != nav.currentDestination?.id) {
                nav.popBackStack(R.id.homeFragment, false)
                nav.navigate(R.id.action_homeFragment_to_aboutUsFragment)
            } else {
                nav.navigate(R.id.action_homeFragment_to_aboutUsFragment)
            }
        }
        if(id == R.id.contactUsMenuId)
        {
            if (nav.graph.startDestination != nav.currentDestination?.id) {
                nav.popBackStack(R.id.homeFragment, false)
                nav.navigate(R.id.action_homeFragment_to_contactUsFragment)
            } else {
                nav.navigate(R.id.action_homeFragment_to_contactUsFragment)
            }
        }
        if(id == R.id.helpMenuId)
        {
            if (nav.graph.startDestination != nav.currentDestination?.id) {
                nav.popBackStack(R.id.homeFragment, false)
                nav.navigate(R.id.action_homeFragment_to_helpFragment)
            } else {
                nav.navigate(R.id.action_homeFragment_to_helpFragment)
            }
        }

        if(id == R.id.logoutMenuId)
        {
            mainActivityViewModel.logout(this)
        }


        binding.drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
