package com.shoohna.tmsouq.ui.home.ui.chat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.channel.Channel
import com.shoohna.tmsouq.databinding.FragmentChatBinding
import com.shoohna.tmsouq.pojo.responses.PerviousChatData
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class ChatFragment : Fragment() {
    lateinit var binding: FragmentChatBinding
    lateinit var currentTime:String
    var chatList = ArrayList<MessageItemUi>()
    lateinit var chatAdapter: ChatAdapter

    lateinit var options:PusherOptions
    lateinit var pusher:Pusher
    lateinit var channel: Channel
    lateinit var jsonObject:JSONObject
    lateinit var jsonObject1:JSONObject
    var messageData= MutableLiveData<List<PerviousChatData>>()
    var messageDataF= MutableLiveData<List<MessageItemUi>>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentChatBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

        val model = ViewModelProvider(this).get(ChatViewModel::class.java)
        binding.vm = model

        binding.btnBackId.setOnClickListener { activity?.onBackPressed() }


        currentTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())

        model.loadDataFirebase(activity!! , binding.chatRecyclerViewId)
//        model.messageListF?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
//            messageDataF.value = it
//            if(it.isEmpty()) {
//                binding.showNoDataId.visibility = View.VISIBLE
//            }
//            else {
//                Log.d("FRAGMENT" , model.myMessages!!.size.toString())
//                chatAdapter= ChatAdapter(model.myMessages)
//                binding.chatRecyclerViewId.adapter = chatAdapter
//                binding.showNoDataId.visibility = View.GONE
//                chatAdapter.notifyDataSetChanged()
//            }
//        })
        /****************************************/
        // This Code belongs to Socket io
//        model.loadData(activity!!)
//        model.messageList?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
//            messageData.value = it
//            if(it.isEmpty()) {
//                binding.showNoDataId.visibility = View.VISIBLE
//            }
//            else {
//                binding.showNoDataId.visibility = View.GONE
//
//                for (i in it) {
//                    if (i.sender == 1)
//                        chatList.add(MessageItemUi(i.message, currentTime, TYPE_MY_MESSAGE))
//                    else if (i.sender == 2)
//                        chatList.add(
//                            MessageItemUi(
//                                i.message,
//                                currentTime,
//                                TYPE_APPLICATION_MESSAGE
//                            )
//                        )
//                    chatAdapter.notifyDataSetChanged()
//                }
//            }
//        })
//
//        options = PusherOptions()
//        options.setCluster("eu");
//        pusher = Pusher("f8b5083ff11e2ffd682d",options)
//
//        pusher.connect(object : ConnectionEventListener {
//            override fun onConnectionStateChange(change: ConnectionStateChange) {
//                Log.i("onConnectionStateChange","State changed from " + change.previousState + " to " + change.currentState)
//            }
//
//            override fun onError(message: String, code: String, e: Exception) {
//                Log.i("onErrorStateChange", "There was a problem connecting!code: $code: $message: $e".trimIndent())
//            }
//        }, ConnectionState.ALL)
//
//        channel = pusher.subscribe("my-channel")
//
//        channel.bind("my-event")
//        {
//            Log.i("PusherEvent", it.toString())
//
//            CoroutineScope(Dispatchers.Main).launch {
//
//                currentTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())
//
//                try {
//                    if(it.data.isEmpty())
//                        binding.showNoDataId.visibility = View.VISIBLE
//                    else {
//                        binding.showNoDataId.visibility = View.GONE
//
//                        jsonObject = JSONObject(it.data.toString())
//                        jsonObject1 = jsonObject.getJSONObject("text")
//                        if (jsonObject1.getString("user_id") == sharedHelper.getKey(
//                                activity!!,
//                                "USER_ID"
//                            )
//                        ) {
//                            if (jsonObject1.getInt("sender") == 1)
//                                chatList.add(
//                                    MessageItemUi(
//                                        jsonObject1.getString("message"),
//                                        currentTime,
//                                        TYPE_MY_MESSAGE
//                                    )
//                                )
//                            else if (jsonObject1.getInt("sender") == 2)
//                                chatList.add(
//                                    MessageItemUi(
//                                        jsonObject1.getString("message"),
//                                        currentTime,
//                                        TYPE_APPLICATION_MESSAGE
//                                    )
//                                )
//                            chatAdapter.notifyDataSetChanged()
//                        }
//                    }
//                }catch (e:Exception)
//                {
//                }
//            }
//        }

        return binding.root
    }

}
