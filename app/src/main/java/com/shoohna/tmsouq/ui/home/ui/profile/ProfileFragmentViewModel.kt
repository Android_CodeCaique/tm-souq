package com.shoohna.tmsouq.ui.home.ui.profile

import android.app.Activity
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.networking.interfaces.auth
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.tmsouq.util.base.SharedHelper
import com.shoohna.shoohna.util.base.BaseFragment
import com.shoohna.shoohna.util.base.BaseViewModel
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import java.io.File


class ProfileFragmentViewModel(private val serviceGeneral: auth) : BaseViewModel() {

    lateinit var nav: NavController

    var email = MutableLiveData<String>("")
    var phone = MutableLiveData<String>("")
    var fName = MutableLiveData<String>("")
    var lName = MutableLiveData<String>("")

    var changeImage : Boolean = false

    var sharedHelper: SharedHelper = SharedHelper()

    var baseFragment: BaseFragment = BaseFragment()
    var loader = MutableLiveData<Boolean>(false)

    fun loadData(v: View , circleImageView: CircleImageView)
    {
        loader.value = true

        val sharedHelper : SharedHelper = SharedHelper()
        val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )

        viewModelScope.async {
            runCatching {
                serviceGeneral.profileFunction(lang.toString() , "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}")
            }.onSuccess {
                try {

                    if (it.isSuccessful)
                    {
                        if (it.body()!!.status == 1) {
                            email.value =it.body()!!.data.email
                            fName.value =it.body()!!.data.frist_name
                            lName.value =it.body()!!.data.last_name
                            phone.value =it.body()!!.data.phone


                            val image = it.body()!!.data.image

                            Log.e("image link ", "profile image link "+image)


                            if(image.toString() == "http://api.shoohna.com/images/1.png")
                            {
                                Glide.with(v).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(circleImageView)
                                sharedHelper.putKey(v.context , "ChangeImage" , "0")
                            }
                            else
                            {
                                Glide.with(v).load(image.toString()).error(R.drawable.ic_profile_upload).into(circleImageView)
                                sharedHelper.putKey(v.context , "ChangeImage" , "1")
                            }




                            Log.i("USER_PHOTO",image.toString())
                            sharedHelper.putKey(v.context,"USER_PHOTO" ,image.toString())
                            sharedHelper.putKey(v.context,"USER_NAME" ,it.body()!!.data.frist_name)
                            sharedHelper.putKey(v.context,"USER_EMAIL" ,it.body()!!.data.email)

                            loader.value = false

                        }else if (it.body()!!.status == 2) {
                            Snackbar.make(v, it.body()?.message!!, Snackbar.LENGTH_SHORT)
                                .show()
                            loader.value = false

                        } else if (it.body()!!.status == 0) {
                            Snackbar.make(
                                v,
                                "${it.body()!!.message}",
                                Snackbar.LENGTH_SHORT
                            ).show()
                            loader.value = false

                            //baseFragment.dismissProgressDialog()
                        } else {

                        }
                    }
                    else
                    {}

                } catch (e: HttpException) {
                    Snackbar.make(v, "Exception ${e.message}", Snackbar.LENGTH_SHORT).show();
                    //baseFragment.dismissProgressDialog()
                    loader.value = false

                } catch (e: Throwable) {
                    Log.d("ERROR" , "ERRORE"+e.message)
                    //  Snackbar.make(v, "Ooops: Something else went wrong", Snackbar.LENGTH_SHORT).show();
                    //baseFragment.dismissProgressDialog()
                    loader.value = false

                }
            }.onFailure {
                Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()

            }
        }

    }


    fun getRealPathFromUri(context: Context, contentUri: Uri?): String? {
        var cursor: Cursor? = null
        return try {

            val proj =
                arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.getContentResolver().query(contentUri!!, proj, null, null, null)
            val column_index: Int = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        } finally {
            if (cursor != null) {
                cursor.close()
            }
        }
    }
    fun updateProfile (v : View , fNameET : EditText , lNameET: EditText , phoneET: EditText , emailET: EditText ,
                       changePasswordTxtId : TextView , editConstraintId : ConstraintLayout , correctImgId : ImageView ,
                       dontSaveImgId : ImageView , addImageBtnId : ImageView , bottom_constraint_id : ConstraintLayout , profileImg:CircleImageView)
    {

        try {
            val phonePart: RequestBody =
                RequestBody.create(MediaType.parse("text/plain"), phone.value!!)
            val fNamePart: RequestBody =
                RequestBody.create(MediaType.parse("text/plain"), fName.value!!)
            val lNamePart: RequestBody =
                RequestBody.create(MediaType.parse("text/plain"), lName.value!!)
            val emailPart: RequestBody =
                RequestBody.create(MediaType.parse("text/plain"), email.value!!)



            var myUri = Uri.parse(sharedHelper.getKey(v!!.context, "Image"))

            var file = File(getRealPathFromUri(v.context, myUri))

            var requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val body = MultipartBody.Part.createFormData("image", file.name, requestFile)



            if (validate(v, fNameET, lNameET, phoneET, emailET))
                return

            if (fName.value?.isNotEmpty()!! && lName.value?.isNotEmpty()!! && phone.value?.isNotEmpty()!! && email.value?.isNotEmpty()!!) {
                loader.value = true
                dontSaveImgId.isEnabled = false
                changePasswordTxtId.isEnabled = false
                try {

                    val sharedHelper : SharedHelper = SharedHelper()
                    val lang : String? = sharedHelper.getKey(v!!.context , "MyLang" )


                    if(sharedHelper.getKey(v!!.context ,"ChangeImage").equals("1"))
                    {
                        CoroutineScope(Dispatchers.IO).async {
                            runCatching {
                                serviceGeneral.editProfileFunction(lang.toString() ,
                                    "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}"
                                    ,  phonePart, emailPart, fNamePart, lNamePart, body
                                )
                            }.onSuccess {
                                withContext(Dispatchers.Main) {
                                    try {
                                        if (it.isSuccessful) {
                                            if (it.body()!!.status == 1) {

                                                Log.d("HHHHHHHH" , "HERE")
                                                //Do something with response e.g show to the UI.
                                                Snackbar.make(
                                                    v,
                                                    it.body()?.message!!,
                                                    Snackbar.LENGTH_SHORT
                                                ).show()
                                                loader.value = false

                                                changePasswordTxtId.visibility = View.GONE
                                                editConstraintId.visibility = View.VISIBLE
                                                bottom_constraint_id.visibility = View.VISIBLE
                                                correctImgId.visibility = View.GONE
                                                dontSaveImgId.visibility = View.GONE
                                                addImageBtnId.visibility = View.GONE

                                                fNameET.isEnabled = false
                                                lNameET.isEnabled = false
                                                phoneET.isEnabled = false
                                                emailET.isEnabled = false

                                                //loadData(v, profileImg)
                                            } else if (it.body()!!.status == 2) {
                                                Log.d("ERROR" , "Failure 1")
                                                Snackbar.make(
                                                    v,
                                                    it.body()?.message!!,
                                                    Snackbar.LENGTH_SHORT
                                                )
                                                    .show()
                                                loader.value = false
                                            } else if (it.body()!!.status == 0) {

                                                Log.d("ERROR" , "Failure 2")
                                                Snackbar.make(
                                                    v,
                                                    "${it.body()!!.message}",
                                                    Snackbar.LENGTH_SHORT
                                                ).show()
                                                loader.value = false
                                                //baseFragment.dismissProgressDialog()
                                            } else {

                                                Log.d("ERROR" , "Failure 3")
                                                loader.value = false
                                            }
                                        }
                                        else
                                        {
                                            Log.d("ERROR" , "Failure")
                                            loader.value = false
                                        }
                                    } catch (e: HttpException) {
                                        Log.d("ERROR" , "Failure 4")
                                        Snackbar.make(
                                            v,
                                            "Exception ${e.message}",
                                            Snackbar.LENGTH_SHORT
                                        )
                                            .show();
                                        loader.value = false

                                    } catch (e: Throwable) {
                                        Log.d("ERROR" , "Failure 5")
                                        loader.value = false
                                    }
                                }
                            }.onFailure {
                                Log.d("ERROR" , "Failure 6")
                                Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()
                                loader.value = false
                            }
                        }
                    }

                    else
                    {
                        CoroutineScope(Dispatchers.IO).async {
                            runCatching {
                                serviceGeneral.editProfileFunctionWithoutImage(lang.toString() ,
                                    "Bearer ${sharedHelper.getKey(v.context, Constants.getToken())}"
                                    ,   phone.value!!,  email.value!!,  fName.value!!,  lName.value!!
                                )
                            }.onSuccess {
                                withContext(Dispatchers.Main) {
                                    try {
                                        if (it.isSuccessful) {
                                            if (it.body()!!.status == 1) {

                                                Log.d("HHHHHHHH" , "HERE")
                                                //Do something with response e.g show to the UI.
                                                Snackbar.make(
                                                    v,
                                                    it.body()?.message!!,
                                                    Snackbar.LENGTH_SHORT
                                                ).show()
                                                loader.value = false

                                                changePasswordTxtId.visibility = View.GONE
                                                editConstraintId.visibility = View.VISIBLE
                                                bottom_constraint_id.visibility = View.VISIBLE
                                                correctImgId.visibility = View.GONE
                                                dontSaveImgId.visibility = View.GONE
                                                addImageBtnId.visibility = View.GONE

                                                fNameET.isEnabled = false
                                                lNameET.isEnabled = false
                                                phoneET.isEnabled = false
                                                emailET.isEnabled = false

                                                //loadData(v, profileImg)
                                            } else if (it.body()!!.status == 2) {
                                                Log.d("ERROR" , "Failure 1")
                                                Snackbar.make(
                                                    v,
                                                    it.body()?.message!!,
                                                    Snackbar.LENGTH_SHORT
                                                )
                                                    .show()
                                                loader.value = false
                                            } else if (it.body()!!.status == 0) {

                                                Log.d("ERROR" , "Failure 2")
                                                Snackbar.make(
                                                    v,
                                                    "${it.body()!!.message}",
                                                    Snackbar.LENGTH_SHORT
                                                ).show()
                                                loader.value = false
                                                //baseFragment.dismissProgressDialog()
                                            } else {

                                                Log.d("ERROR" , "Failure 3")
                                                loader.value = false
                                            }
                                        }
                                        else
                                        {
                                            Log.d("ERROR" , "Failure")
                                            loader.value = false
                                        }
                                    } catch (e: HttpException) {
                                        Log.d("ERROR" , "Failure 4")
                                        Snackbar.make(
                                            v,
                                            "Exception ${e.message}",
                                            Snackbar.LENGTH_SHORT
                                        )
                                            .show();
                                        loader.value = false

                                    } catch (e: Throwable) {
                                        Log.d("ERROR" , "Failure 5")
                                        loader.value = false
                                    }
                                }
                            }.onFailure {
                                Log.d("ERROR" , "Failure 6")
                                Snackbar.make(v, it.message.toString(), Snackbar.LENGTH_SHORT).show()
                                loader.value = false
                            }
                        }
                    }



                }catch (e:Exception)
                {
                    Snackbar.make(
                        v,
                        e.message.toString(),
                        Snackbar.LENGTH_SHORT
                    ).show();
                }
            } else
                Snackbar.make(
                    v,
                    v.resources.getString(R.string.pleaseInsertAllData),
                    Snackbar.LENGTH_SHORT
                ).show();
        }catch (e:Exception)
        {
            Snackbar.make(
                v,
                e.message.toString(),
                Snackbar.LENGTH_SHORT
            ).show();
        }

    }

    fun validate(v: View, fName : EditText , lName : EditText , phone : EditText , email : EditText ): Boolean{

        if (fName!!.text.equals(""))
        {
            v.fName.setError( v.resources.getString(R.string.required))
            return true
        }
        if (lName!!.text.equals(""))
        {
            v.lName.setError( v.resources.getString(R.string.required))
            return true
        }
        if (phone!!.text.equals(""))
        {
            v.phone.setError( v.resources.getString(R.string.required))
            return true
        }
        if (email!!.text.equals(""))
        {
            v.email.setError( v.resources.getString(R.string.required))
            return true
        }
        if (!(email.text.toString().contains("@") && email.text.toString().contains(".")))
        {
            v.email.setError( v.resources.getString(R.string.notValidEmail))
            return true
        }
        return false
    }

    fun share(v:View)
    {
        baseFragment.share(v)
    }

    fun moveToHelp(v:View)
    {
        Navigation.findNavController(v).navigate(R.id.action_profileFragment_to_helpFragment)
    }

    fun moveToOrderStatus(v:View)
    {
       // Navigation.findNavController(v).navigate(R.id.orderStatusFragment)

        nav = Navigation.findNavController(v!!.context as Activity ,R.id.nav_host_fragment)
        nav.navigate(R.id.orderStatusFragment)
    }



}