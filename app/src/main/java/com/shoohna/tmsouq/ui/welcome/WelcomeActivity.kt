package com.shoohna.tmsouq.ui.welcome

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64.encode
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.util.base.LanguageHelper
import com.shoohna.tmsouq.util.base.SharedHelper
import java.security.MessageDigest

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        val sharedHelper = SharedHelper()
        sharedHelper.putKey(this, "OPEN", "OPEN")

        printHashKey(this)
        if(sharedHelper.getKey(this , "MyLang")!!.isEmpty() || sharedHelper.getKey(this , "MyLang").equals(""))
        {
            sharedHelper.putKey(this , "MyLang" , "ar")
            var languageHelper : LanguageHelper = LanguageHelper()
            languageHelper.ChangeLang(resources , "ar")
        }
        else
        {
            var lan = sharedHelper.getKey(this , "MyLang" )
            var languageHelper : LanguageHelper = LanguageHelper()
            languageHelper.ChangeLang(resources , lan)
        }
    }

//    fun printKeyHash(context: Activity): String? {
//        val packageInfo: PackageInfo
//        var key: String? = null
//        try {
//            //getting application package name, as defined in manifest
//            val packageName = context.applicationContext.packageName
//
//            //Retriving package info
//            packageInfo = context.packageManager.getPackageInfo(
//                packageName,
//                PackageManager.GET_SIGNATURES
//            )
//            Log.e("Package Name=", context.applicationContext.packageName)
//            for (signature in packageInfo.signatures) {
//                val md: MessageDigest = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                key = String(encode(md.digest(), 0))
//
//                // String key = new String(Base64.encodeBytes(md.digest()));
//                Log.e("Key Hash=", key)
//            }
//        } catch (e1: PackageManager.NameNotFoundException) {
//            Log.e("Name not found", e1.toString())
//        } catch (e: NoSuchAlgorithmException) {
//            Log.e("No such an algorithm", e.toString())
//        } catch (e: Exception) {
//            Log.e("Exception Hash", e.toString())
//        }
//        return key
//    }

    fun printHashKey(context: Context) {
        try {
            val info = context.packageManager
                .getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(encode(md.digest(), 0))
                Log.i("AppLog11", "key:$hashKey=")
            }
        } catch (e: java.lang.Exception) {
            Log.e("AppLog22", "error:", e)
        }
    }
}