package com.shoohna.tmsouq.ui.home.ui.chat

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.pojo.model.Chat

class ChatFirebaseAdapter(var data: List<Chat>) : RecyclerView.Adapter<MessageViewHolder<*>>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder<*> {
        val context = parent.context

        return when (viewType) {
            0 -> {
                val view =
                    LayoutInflater.from(context).inflate(R.layout.my_chat_item, parent, false)
                MyMessageViewHolder(view)

            }
            1 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.application_chat_item, parent, false)
                ApplicationMessageViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder<*>, position: Int) {
        val item = data[position]
        Log.d("adapter View", position.toString() + item.Content)
        when (holder) {
            is MyMessageViewHolder -> holder.bind(item)
            is ApplicationMessageViewHolder -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int = data[position].SenderInt

    class MyMessageViewHolder(val view: View) : MessageViewHolder<Chat>(view) {
        private val messageContent = view.findViewById<TextView>(R.id.message)
        private val time = view.findViewById<TextView>(R.id.timeTxtViewId)

        override fun bind(item: Chat) {
            messageContent.text = item.Content
            time.text = item.Time
        }
    }

    class ApplicationMessageViewHolder(val view: View) : MessageViewHolder<Chat>(view) {
        private val messageContent = view.findViewById<TextView>(R.id.message)
        private val time = view.findViewById<TextView>(R.id.timeTxtViewId)

        override fun bind(item: Chat) {
            messageContent.text = item.Content
            time.text = item.Time

        }


    }
}