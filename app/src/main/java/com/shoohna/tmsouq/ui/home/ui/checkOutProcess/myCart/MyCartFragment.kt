package com.shoohna.tmsouq.ui.home.ui.checkOutProcess.myCart

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.shoohna.tmsouq.databinding.FragmentCheckoutProcessBinding
import com.shoohna.tmsouq.pojo.model.ProductEntity

/**
 * A simple [Fragment] subclass.
 */
class MyCartFragment : Fragment() {

    lateinit var binding: FragmentCheckoutProcessBinding

    var checkoutCartData= MutableLiveData<List<ProductEntity>>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentCheckoutProcessBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this

        val model = ViewModelProvider(this).get(MyCartViewModel::class.java)
        binding.vm = model


        model.loadDataFromRoom(activity!!)
        model.checkoutCartList!!.observe(viewLifecycleOwner, Observer {
            checkoutCartData.value = it
            binding.myCartRecycvlerViewId.adapter = MyCartRecyclerViewAdapter(checkoutCartData, activity!!)
        })

//        cartAdapter= MyCartRecyclerViewAdapter(cartList, context?.applicationContext)
//        binding.myCartRecycvlerViewId.adapter = cartAdapter


//        binding.nextBtnId.setOnClickListener {
//            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_checkoutProcessFragment_to_shippingFragment) }
//        }

        return binding.root
    }


}
