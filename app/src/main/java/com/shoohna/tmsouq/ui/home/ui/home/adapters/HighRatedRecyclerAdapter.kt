package com.shoohna.tmsouq.ui.home.ui.home.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.shoohna.e_commercehappytimes.ui.home.ui.home.HomeFragmentDirections
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.HighRatedListRowBinding
import com.shoohna.tmsouq.pojo.responses.HighRated
import com.shoohna.tmsouq.ui.home.MainActivity

class HighRatedRecyclerAdapter (private var dataList: LiveData<List<HighRated>>, private val context: Context?) : RecyclerView.Adapter<HighRatedRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            HighRatedListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.value!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList.value!![position])
    }


    class ViewHolder(private var binding: HighRatedListRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: HighRated) {
            binding.model = item
            binding.executePendingBindings()
            binding.mainConstraintLayoutId.setOnClickListener {
//                Navigation.findNavController(itemView).navigate(R.id.action_homeFragment_to_productDetailsFragment)
                (itemView.context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.GONE
                val action = HomeFragmentDirections.actionHomeFragmentToProductDetailsFragment2(item.id,item.image,item.desc,item.price_product)
                Navigation.findNavController(it).navigate(action)

                Log.i("Item Id",item.id.toString())
            }
        }

    }

}