package com.shoohna.tmsouq.ui.home.ui.product

import ApiClient.sharedHelper
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.databinding.FragmentProductBinding
import com.shoohna.tmsouq.pojo.responses.ProductByTypeData
import com.shoohna.tmsouq.util.base.Constants
import com.shoohna.shoohna.util.base.BaseFragment
import kotlinx.android.synthetic.main.activity_main.view.*
import org.koin.android.ext.android.inject


/**
 * A simple [Fragment] subclass.
 */
class ProductFragment : Fragment() {
    lateinit var binding:FragmentProductBinding
    lateinit var mainTitle:String
    lateinit var searchWord:String
    var productData= MutableLiveData<List<ProductByTypeData>>()
    private val productViewModel: ProductViewModel by inject()   // 1
    lateinit var baseFragment: BaseFragment
    lateinit var mAdapter: ProductRecyclerAdapter
    lateinit var filteredList: MutableLiveData<List<ProductByTypeData>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentProductBinding.inflate(layoutInflater, container, false)
        binding.lifecycleOwner = this
//        try {
////            Picasso.get().load(sharedHelper.getKey(activity!!, "USER_PHOTO")).into(binding.circleImageView)
//            if(sharedHelper.getKey(requireActivity(), "USER_PHOTO").toString() == "http://api.shoohna.com/images/1.png")
//                Glide.with(requireActivity()).load(R.drawable.ic_profile_upload).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
//            else
//                Glide.with(requireActivity()).load(sharedHelper.getKey(requireActivity(), "USER_PHOTO")).error(R.drawable.ic_profile_upload).into(binding.circleImageView)
//
//        }catch (e: Exception)
//        {}
//        val model = ViewModelProvider(this).get(ProductViewModel::class.java)
//        binding.vm = modelSearchByNameResponse
        filteredList =  MutableLiveData()
        arguments?.let { val safeArgs = ProductFragmentArgs.fromBundle(it)
            binding.productTypeTxtView.text = safeArgs.productType
            mainTitle = safeArgs.productType
            searchWord = safeArgs.productSearchName.toString()
        }
        baseFragment = BaseFragment()

        binding.vm = productViewModel

        if(mainTitle == getString(R.string.highRated)) {productViewModel.typeId.value = 1}
        if(mainTitle == getString(R.string.bestSale)) {productViewModel.typeId.value = 2}
        if(mainTitle == getString(R.string.offers)) {productViewModel.typeId.value = 3}
        if(mainTitle == getString(R.string.productSearch) && searchWord.isNotEmpty()){productViewModel.searchWord.value = searchWord}

        mAdapter = ProductRecyclerAdapter(productData, context, productViewModel,viewLifecycleOwner , binding)


        if(mainTitle == getString(R.string.highRated) || mainTitle == getString(R.string.bestSale) || mainTitle == getString(R.string.offers)) {
            productViewModel.getProductTypeList(requireActivity()).observe(viewLifecycleOwner, Observer {
                productData.value = it
                Log.i("ProductData",it.toString())
                binding.productViewRecyclerViewId.adapter = mAdapter
                filteredList.value = it
//                    ProductRecyclerAdapter(productData, context, productViewModel,viewLifecycleOwner , binding)
            })
        }
        if(mainTitle == getString(R.string.productSearch))
        {
            productViewModel.getPorductDataBySearchName(requireActivity()).observe(viewLifecycleOwner, Observer {
                productData.value = it
                binding.productViewRecyclerViewId.adapter = mAdapter
//                    ProductRecyclerAdapter(productData, context, productViewModel,viewLifecycleOwner , binding)
                filteredList.value = it

            })
        }

        binding.notificationImgId.setOnClickListener {
            if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                baseFragment.showAlert(it.rootView.context)
            }else {
                view?.let { it1 ->
                    Navigation.findNavController(it1)
                        .navigate(R.id.action_productFragment_to_notificationFragment)
                }
                activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }
        }

        binding.messangerImgId.setOnClickListener {
            if(sharedHelper.getKey(requireActivity(), Constants.getToken())?.isEmpty()!!){
                baseFragment.showAlert(it.rootView.context)
            }else {
                view?.let { it1 ->
                    Navigation.findNavController(it1)
                        .navigate(R.id.action_productFragment_to_chatFragment)
                }
                activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility =
                    View.GONE
            }
        }


//        binding.btnBackId.setOnClickListener {
//            activity?.onBackPressed()
//        }

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                filter(s.toString())

            }

            override fun afterTextChanged(s: Editable) {
                filter(s.toString())

            }
        })
//
//        productViewModel.searchEditText.observe(viewLifecycleOwner, Observer {
//            if(it.isNotEmpty()) {
//                Log.i("ObserverSearch",it.toString())
//                filter(it)
//            }
////            else if(it.isEmpty() && productData.value != null)
////                binding.productViewRecyclerViewId.adapter = ProductRecyclerAdapter(productData, context, productViewModel,viewLifecycleOwner , binding)
//
////                mAdapter.filter.filter(it)
//        })


        binding.drawerIconId.setOnClickListener {
            activity?.findViewById<DrawerLayout>(R.id.drawer)!!.drawer.openDrawer(GravityCompat.START)
        }

        return binding.root
    }

    private fun filter(text: String) {
        val listed = ArrayList<ProductByTypeData>()
        val listed1 : MutableLiveData<List<ProductByTypeData>> = MutableLiveData<List<ProductByTypeData>>()

        for (item in filteredList.value!!) {
            if (item.name.toLowerCase().contains(text.toLowerCase())) {
                listed.add(item)
            }
        }
        listed1.value = listed
//        filteredList.value = listed
        mAdapter.filterList(listed1)
        mAdapter.notifyDataSetChanged()
    }


}
