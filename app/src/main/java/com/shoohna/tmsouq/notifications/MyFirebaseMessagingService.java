package com.shoohna.tmsouq.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.shoohna.tmsouq.R;
import com.shoohna.tmsouq.ui.home.MainActivity;
import com.shoohna.tmsouq.ui.welcome.WelcomeActivity;
import com.shoohna.tmsouq.util.base.SharedHelper;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAGID = "InstanceIdService";
    NotificationManager notificationManager;
    public static String TAG;

    public static final String ANDROID_CHANNEL_ID = "com.shoohna.happytimes";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

                    // Foreground
                    Intent intentX = new Intent("FCM_MESSAGE");
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
                    localBroadcastManager.sendBroadcast(intentX);
                    //********************************************************************
                    Map<String, String> data = remoteMessage.getData();
                    String myCustomKey = data.get("redirect_id");
                    String status = data.get("status");
                    Log.d("STS" , status);

                    SharedHelper sharedHelper = new SharedHelper();
                    sharedHelper.putKey(this,"OPEN"  , "OPEN");

                    String Title = remoteMessage.getNotification().getTitle();
                    String Body = remoteMessage.getNotification().getBody();

                    sharedHelper.putKey(this,"PostID"  , myCustomKey);
                    sharedHelper.putKey(this,"Status"  , status);
                    sharedHelper.putKey(this,"Title"  , Title);
                    sharedHelper.putKey(this,"Body"  , Body);

                    Intent intent = new Intent();
                    if (status != null)
                    {
                        Log.d("error" , " ya welcome b success ");
                        if (status.equals("1"))
                        {
                            intent = new Intent(this , MainActivity.class);
                        }
                        else if (status.equals("2"))
                        {
                            intent = new Intent(this , MainActivity.class);
                        }
                        else if (status.equals("3"))
                        {
                            intent = new Intent(this , MainActivity.class);
                        }
                        else
                        {
                            intent = new Intent(this , WelcomeActivity.class);
                        }
                    }
                    else
                    {
                        Log.d("error" , " ya welcome b error ");
                    }

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        setupChannels();
                    }

                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon( R.drawable.new_logo)//a resource for your custom small icon
                            .setContentTitle(remoteMessage.getData().get("title"))
                            .setContentText(remoteMessage.getData().get("body")) //ditto
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                    R.drawable.new_logo));

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(0/* ID of notification */, notificationBuilder.build());

            }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(){
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ANDROID_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }
}