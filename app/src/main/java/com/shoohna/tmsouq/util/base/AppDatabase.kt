package com.shoohna.tmsouq.util.base

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shoohna.tmsouq.networking.interfaces.ProductDao
import com.shoohna.tmsouq.pojo.model.ProductEntity

@Database(entities = [ProductEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun productDao(): ProductDao

}