package com.shoohna.tmsouq.util.base

 data class TestReponse(
    val `data`: Any,
    val message: String,
    val status: Boolean
)