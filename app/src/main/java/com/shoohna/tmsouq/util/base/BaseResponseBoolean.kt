package com.shoohna.tmsouq.util.base

data class BaseResponseBoolean(
    val data: String,
    val message: String,
    val status: Boolean
)