package com.shoohna.shoohna.util.base

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import com.google.android.material.textfield.TextInputLayout
import com.shoohna.tmsouq.R
import com.shoohna.tmsouq.ui.home.MainActivity

open class BaseViewModel : ViewModel() {

    var modelData = MutableLiveData<String>()
    var liveList = MutableLiveData<ArrayList<String>>()


    fun checkEmpty(textInputLayout:TextInputLayout, data:String , message:String): Boolean {
        if(TextUtils.isEmpty(data)){
            textInputLayout.error = message

            textInputLayout.hasFocus()
            textInputLayout.requestFocus()
            return true
        }
        textInputLayout.error = ""
        textInputLayout.isErrorEnabled = false
        return false
    }

    fun checkValidEmail(textInputLayout:TextInputLayout, data:String , message:String): Boolean {
        if(!data.contains("@") || !data.contains(".")){
            textInputLayout.error = message;

            textInputLayout.hasFocus()
            textInputLayout.requestFocus()
            return true
        }
        textInputLayout.error = "";
        textInputLayout.isErrorEnabled = false
        return false
    }
    fun checkValidPassword(textInputLayout:TextInputLayout, data:String , message:String): Boolean {
        if(data.length < 7){
            textInputLayout.error = message;

            textInputLayout.hasFocus()
            textInputLayout.requestFocus()
            return true
        }
        textInputLayout.error = "";
        textInputLayout.isErrorEnabled = false
        return false
    }
    fun checkEquals(textInputLayout:TextInputLayout, data:String, data2:String, message: String): Boolean {
        if(!data.equals(data2)){
            textInputLayout.error = message;
            return true
        }

        //
        textInputLayout.error = "";
        textInputLayout.isErrorEnabled = false
        return false
    }

     fun showDialog(title: String , v : View) {
        val dialog = Dialog(v!!.context)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setCancelable(false)
//        dialog .setContentView(R.layout.foreground_notification_dialog)
//        val Title = dialog .findViewById(R.id.Title) as TextView
//        val GoToLogin = dialog .findViewById(R.id.GoToLogin) as Button
//         GoToLogin.setOnClickListener {
//             var intent : Intent = Intent (v!!.context , WelcomeActivity::class.java)
//             v!!.context.startActivity(intent)
//             (v.context as Activity).finish()
//         }
//        Title.text = title
        dialog .show()

    }



    fun showAlertProduct(view: View , title: String , context: Context)
    {
        val alert: Dialog? = Dialog(view.rootView.context)
        alert?.setContentView(R.layout.ask_user_choice_after_add_cart)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<TextView>(R.id.titleId)?.text = title
        alert?.findViewById<Button>(R.id.homeBtnId)?.setOnClickListener {
            Navigation.findNavController(view).popBackStack(R.id.homeFragment, false)
            alert.dismiss()
//            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
        }
        alert?.findViewById<Button>(R.id.cartBtnId)?.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_productDetailsFragment_to_cartFragment)
//            activity?.findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
            alert.dismiss()
            (context as MainActivity?)?.iconCardClicked()
            (context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE
        }

        alert?.show()
    }

    fun confirmOrderDialog(v: View , context :Context) {
        val alert: Dialog? = Dialog(v.rootView.context)
        alert?.setContentView(R.layout.confirm_order_layout)
        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alert?.findViewById<Button>(R.id.homeBtnId)?.setOnClickListener {
            Navigation.findNavController(v).popBackStack(R.id.homeFragment, false)
            alert.dismiss()

            (context as MainActivity?)?.iconHomeClickedFromHome()
            (context as MainActivity).findViewById<ConstraintLayout>(R.id.bottom_navigation_constraint)!!.visibility = View.VISIBLE

        }
        alert?.findViewById<Button>(R.id.dismissBtnId)?.setOnClickListener {
            alert.dismiss()

        }
        alert?.show()

    }


}