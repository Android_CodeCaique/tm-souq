package com.shoohna.tmsouq.util.base

data class BaseResponse(
    val data: String,
    val message: String,
    val status: Int
)