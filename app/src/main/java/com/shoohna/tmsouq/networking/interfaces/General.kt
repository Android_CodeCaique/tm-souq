package com.shoohna.tmsouq.networking.interfaces

import com.shoohna.tmsouq.pojo.responses.AboutUsResponse
import com.shoohna.tmsouq.pojo.responses.AboutUsResponse2
import com.shoohna.tmsouq.util.base.BaseResponse
import retrofit2.Response
import retrofit2.http.*

interface General {


    @GET("general/get_info")
    suspend fun aboutUsFunction(@Header("lang" ) lang:String ,
                                @Query("shop_id")shop_id:String): Response<AboutUsResponse>

    @FormUrlEncoded
    @POST("general/contact_us")
    suspend fun contactUsFunction(@Header("lang" ) lang:String ,
                                  @Field("shop_id")shop_id:String,
                                  @Field("phone")phone:String,
                                  @Field("message")message:String,
                                  @Field("email")email:String): Response<BaseResponse>


    @GET("general/about_us")
    suspend fun aboutUs(@Header("lang" ) lang:String ,
                        @Query("shop_id")shop_id:String): Response<AboutUsResponse2>

}