import com.shoohna.e_commercehappytimes.ui.home.ui.home.HomeViewModel
import com.shoohna.tmsouq.networking.interfaces.General
import com.shoohna.tmsouq.networking.interfaces.Home
import com.shoohna.tmsouq.networking.interfaces.auth
import com.shoohna.tmsouq.ui.home.MainActivityViewModel
import com.shoohna.tmsouq.ui.home.ui.aboutUs.AboutUsViewModel
import com.shoohna.tmsouq.ui.home.ui.cart.CartViewModel
import com.shoohna.tmsouq.ui.home.ui.checkOutProcess.paymentWay.PaymentWayViewModel
import com.shoohna.tmsouq.ui.home.ui.checkOutProcess.shipping.ShippingViewModel
import com.shoohna.tmsouq.ui.home.ui.contactUs.ContactUsViewModel
import com.shoohna.tmsouq.ui.home.ui.favorite.FavoriteViewModel
import com.shoohna.tmsouq.ui.home.ui.home.viewModel.FilterBottomViewModel
import com.shoohna.tmsouq.ui.home.ui.more.MoreViewModel
import com.shoohna.tmsouq.ui.home.ui.moreSetting.MoreSettingViewModel
import com.shoohna.tmsouq.ui.home.ui.notification.NotificationViewModel
import com.shoohna.tmsouq.ui.home.ui.orderData.OrderDataViewModel
import com.shoohna.tmsouq.ui.home.ui.orderStatus.OrderStatusViewModel
import com.shoohna.tmsouq.ui.home.ui.product.ProductViewModel
import com.shoohna.tmsouq.ui.home.ui.productDetails.ProductDetailsViewModel
import com.shoohna.tmsouq.ui.home.ui.profile.ProfileFragmentViewModel
import com.shoohna.tmsouq.ui.welcome.ui.forgetPassword.ForgetPasswordViewModel
import com.shoohna.tmsouq.ui.welcome.ui.login.LoginViewModel
import com.shoohna.tmsouq.ui.welcome.ui.register.RegisterViewModel
import com.shoohna.tmsouq.ui.welcome.ui.resetPassword.ResetPasswordViewModel
import com.shoohna.tmsouq.ui.welcome.ui.verifyCode.VerifyCodeViewModel
import com.shoohna.tmsouq.util.base.SharedHelper
import okhttp3.OkHttpClient
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


object ApiClient {
//   var BASE_URL:String="https://gist.githubusercontent.com/"
//    val getClient: ApiInterface
//        get() {
//
//            val gson = GsonBuilder()
//                .setLenient()
//                .create()
//            val interceptor = HttpLoggingInterceptor()
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
//            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
//
//            val retrofit = Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build()
//
//
//            return retrofit.create(ApiInterface::class.java)
//
//        }

    var sharedHelper:SharedHelper = SharedHelper()
    const val BASE_URL = "http://tmsouq.codecaique.com/api/"

    val welcomeModule = module {
        fun makeRetrofitService(): auth {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create().asLenient())
            .build().create(auth::class.java)
        }

        single { makeRetrofitService() }
    }
    val welcomeModuleLogin = module { viewModel { LoginViewModel(get()) } }
    val welcomeModuleRegister = module { viewModel { RegisterViewModel(get()) } }
    val welcomeModuleForgetPassword = module { viewModel { ForgetPasswordViewModel(get()) } }
    val welcomeModuleResetPassword = module { viewModel { ResetPasswordViewModel(get()) } }
    val welcomeModuleVerifyCode = module { viewModel { VerifyCodeViewModel(get()) } }
    val welcomeModuleChangePasswordHome = module { viewModel { com.shoohna.tmsouq.ui.home.ui.changePasswordProfile.ChangePasswordViewModel(get ()) } }
    val welcomeModuleMore = module {viewModel { MoreViewModel(get()) }}
    val welcomeModuleProfile = module { viewModel { ProfileFragmentViewModel(get()) } }

    val homeServiceModule = module {

        fun makeRetrofitServiceHome(): Home {

//        val client : OkHttpClient = OkHttpClient()
//        client.connectTimeoutMillis().and(1000)
//        client.readTimeoutMillis().and(1000)
//        client.setConnectTimeout(10, TimeUnit.SECONDS)
//        client.setReadTimeout(30, TimeUnit.SECONDS)
//        client.interceptors().add(Interceptor { chain -> onOnIntercept(chain) })
//

//        val httpClient = OkHttpClient
//            .connectTimeout(5, TimeUnit.MINUTES)
//            .readTimeout(5, TimeUnit.MINUTES)
//            .addInterceptor {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }

            val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .client(okHttpClient)
                .build()
                .create(Home::class.java) // create module after using Home class
        }
        single { makeRetrofitServiceHome() }
    }

    val homeServiceModuleCart = module { viewModel { CartViewModel(get()) } }
    val homeServiceModulePaymentWay = module { viewModel { PaymentWayViewModel(get()) } }
    val homeServiceModuleShipping = module { viewModel { ShippingViewModel(get()) } }
    val homeServiceModuleFavorite = module { viewModel { FavoriteViewModel(get()) } }
    val homeServiceModuleHome = module { viewModel { HomeViewModel(get()) } }
    val homeServiceModuleFilter = module { viewModel { FilterBottomViewModel(get()) } }
    val homeServiceModuleMoreSetting = module { viewModel { MoreSettingViewModel(get()) } }
    val homeServiceModuleNotification = module { viewModel { NotificationViewModel(get()) } }
    val homeServiceModuleOrderData = module { viewModel { OrderDataViewModel(get()) } }
    val homeServiceModuleOrderStatus = module { viewModel { OrderStatusViewModel(get()) } }
    val homeServiceModuleProduct = module { viewModel { ProductViewModel(get()) } }
    val homeServiceModuleProductDetails = module { viewModel { ProductDetailsViewModel(get()) } }
    val homeServiceModuleMainActivity = module { viewModel { MainActivityViewModel(get()) } }





    val homeGeneralModule = module {

        fun makeRetrofitServiceGeneral(): General{
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .build().create(General::class.java)
        }
        single { makeRetrofitServiceGeneral() }

    }

    val homeGeneralContactUs = module { viewModel { ContactUsViewModel(get()) } }
    val homeGeneralAboutUs = module { viewModel { AboutUsViewModel(get()) } }




//    val netModule = module {
//        fun provideCache(application: Application): Cache {
//            val cacheSize = 10 * 1024 * 1024
//            return Cache(application.cacheDir, cacheSize.toLong())
//        }
//
//        fun provideHttpClient(cache: Cache): OkHttpClient {
//            val okHttpClientBuilder = OkHttpClient.Builder()
//                .cache(cache)
//
//            return okHttpClientBuilder.build()
//        }
//
//        fun provideGson(): Gson {
//            return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create()
//        }


//        fun provideRetrofit(factory: Gson, client: OkHttpClient): Retrofit {
//            return Retrofit.Builder()
//                .baseUrl("https://newsapi.org/")
//                .addConverterFactory(GsonConverterFactory.create(factory))
//                .addCallAdapterFactory(CoroutineCallAdapterFactory())
//                .client(client)
//                .build()
//        }

//        single { provideCache(androidApplication()) }
//        single { provideHttpClient(get()) }
//        single { provideGson() }
//        single { provideRetrofit(get(), get()) }
//    }

}