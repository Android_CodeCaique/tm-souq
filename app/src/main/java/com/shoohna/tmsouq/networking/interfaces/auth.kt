package com.shoohna.tmsouq.networking.interfaces

import com.shoohna.tmsouq.pojo.responses.*
import retrofit2.Response
import retrofit2.http.*
import com.shoohna.tmsouq.util.base.BaseResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface auth {

    @FormUrlEncoded
    @POST("Auth_general/register")
    suspend fun registerFunction(@Header("lang" ) lang:String ,
                                 @Field("phone")phone:String ,
                                 @Field("email") email :String ,
                                 @Field("frist_name") frist_name :String ,
                                 @Field("last_name") last_name :String ,
//                                 @Field("lat") lat:String ,
//                                 @Field("lng") lng:String ,
                                 @Field("password") password:String ,
                                 @Field("password_confirmation") password_confirmation:String ,
                                 @Field("verify_type") verify_type:String ,
                                 @Field("fire_base_token") fire_base_token : String?): Response<RegisterResponse>

    @Multipart
    @POST("Auth_private/edit_profile")
    suspend fun editProfileFunction(@Header("lang" ) lang:String ,
                                    @Header("Authorization") Authorization:String  ,
                                    @Part("phone") phone:RequestBody,
                                    @Part("email") email:RequestBody,
                                    @Part("frist_name") frist_name:RequestBody,
                                    @Part("last_name") last_name:RequestBody,
                                    @Part part :  MultipartBody.Part): Response<RegisterResponse>


    @FormUrlEncoded
    @POST("Auth_private/edit_profile")
    suspend fun editProfileFunctionWithoutImage(@Header("lang" ) lang:String,
                                    @Header("Authorization") Authorization:String,
                                    @Field("phone") phone:String,
                                    @Field("email") email:String,
                                    @Field("frist_name") frist_name:String,
                                    @Field("last_name") last_name:String): Response<RegisterResponse>

    @FormUrlEncoded
    @POST("Auth_general/login")
    suspend fun loginFunction(@Header("lang" ) lang:String ,
                              @Field("eamilOrPhone")eamilOrPhone:String ,
                              @Field("password") password :String ,
                              @Field("fire_base_token") fire_base_token : String?): Response<RegisterResponse>

    @FormUrlEncoded
    @POST("Auth_private/change_password")
    suspend fun changePasswordFunction(@Header("lang" ) lang:String ,
                                       @Field("oldPassword")oldPassword:String ,
                                       @Field("newPassword") newPassword :String ,
                                       @Header("Authorization") Authorization:String): Response<BaseResponse>

    @FormUrlEncoded
    @POST("Auth_general/register_social")
    suspend fun registerSocialFunction(@Header("lang" ) lang:String ,
                                       @Field("email")email:String ,
                                       @Field("lat") lat :String ,
                                       @Field("lng") lng :String,
                                       @Field("frist_name") frist_name :String,
                                       @Field("last_name") last_name :String,
                                       @Field("image") image :String,
                                       @Field("fire_base_token") fire_base_token : String?
                                       ): Response<RegisterResponse>


    @FormUrlEncoded
    @POST("Auth_general/login_social")
    suspend fun loginSocialFunction(@Header("lang" ) lang:String ,
                                    @Field("email")email:String? ,
                                    @Field("fire_base_token") fire_base_token : String? ): Response<RegisterResponse>



    @FormUrlEncoded
    @POST("Auth_general/forget_password")
    suspend fun forgetPasswordFunction(@Header("lang" ) lang:String ,
                                       @Field("email")email:String  ): Response<BaseResponse>

    @FormUrlEncoded
    @POST("Auth_general/reset_password")
    suspend fun resetPasswordFunction(@Header("lang" ) lang:String ,
                                      @Field("code")code:String ,
                                      @Field("password")password:String  ): Response<BaseResponse>



    @GET("Auth_private/my_info")
    suspend fun profileFunction( @Header("lang" ) lang:String ,
                                 @Header("Authorization") Authorization:String ): Response<RegisterResponse>

    @POST("Auth_private/activate_account")
    suspend fun ActiveFunction(@Header("lang" ) lang:String ,
                               @Header("Authorization") Authorization:String ): Response<RegisterResponse>

    @GET("Auth_private/logout")
    suspend fun logoutFunction( @Header("lang" ) lang:String ,
                                @Header("Authorization") Authorization:String ): Response<BaseResponse>





}