package com.shoohna.tmsouq.networking.interfaces

import com.shoohna.tmsouq.pojo.responses.*
import com.shoohna.tmsouq.util.base.BaseResponse
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface Home {

    @GET("Home/get_main_cat")
    suspend fun getCategory(@Header("lang" ) lang:String ): Response<categoryResponse>


    @GET("Product/products_by_category/{id}")
    suspend fun getCategoryProduct(@Header("lang" ) lang:String ,  @Path("id") id:Int): Response<ProductCategoryResponse>

    @GET("Home/home")
    suspend fun getHomeData(@Header("lang" ) lang:String ): Response<HomeResponse>

   @GET("Home/filter")
    suspend fun filterData(@Header("lang" ) lang:String ,
                           @Query("from") from:Int,
                           @Query("to") to :Int,
                           @Query("type") type:Int,
                           @Query("lat") lat:Double,
                           @Query("lng") lng:Double,
                           @Query("cat_id") cat_id:Int): Response<HomeResponse>

    @GET("Product/product_details/{id}")
    suspend fun productDetails(@Header("lang" ) lang:String ,
                               @Path("id") id:Int): Response<ProductDetailsResponse>

    @FormUrlEncoded
    @POST("Product/add_to_cart/{id}")
    suspend fun addToCart(@Header("lang" ) lang:String ,
                          @Path("id") id:Int, @Header("Authorization") Authorization:String, @Field("quantity") quantity:Int, @Field("color_id") color_id:Int, @Field("size_id") size_id:Int ) : Response<AddToCartResponse>

    @GET("Home/get_main_cat")
    suspend fun getFilterMainCategorie(@Header("lang" ) lang:String): Response<FilterMainCategorieResponse>

    @GET("Auth_private/my_cart")
    suspend fun getMyCart(@Header("lang" ) lang:String , @Header("Authorization") Authorization:String) : Response<MyCartResponse>

    @POST("Product/delete_from_cart/{id}")
    suspend fun deleteFromCart(@Header("lang" ) lang:String , @Path("id") id:Int , @Header("Authorization") Authorization:String) : Response<DeleteFromCartResponse>

    @POST("Order/make_order")
    suspend fun CheckoutCart(@Header("lang" ) lang:String , @Header("Authorization") Authorization:String) : Response<CheckoutCartResponse>

    @POST("Order/make_order")
    suspend fun saveShippingInfo(@Header("lang" ) lang:String , @Header("Authorization" ) Authorization:String) : Response<CheckoutShippingResponse>

    @GET("Auth_private/my_wishlist")
    suspend fun getMyWishlist(@Header("lang" ) lang:String , @Header("Authorization" ) Authorization:String) :Response<MyWishlistResponse>

    @GET("Product/get_products")
    suspend fun getAllProductByType(@Header("lang" ) lang:String , @Query("type") type: Int , @Header("Authorization" ) Authorization:String) : Response<ProductByTypeResponse>

    @POST("Product/wishlist/{id}")
    suspend fun addOrDeleteFromMyWishlist(@Header("lang" ) lang:String , @Path("id") id:Int, @Header("Authorization" ) Authorization:String) : Response<BaseResponse>

    @POST("Product/update_cart")
    suspend fun updateCart(@Header("lang" ) lang:String , @Header("Authorization") Authorization:String  ,
                           @Body pro : RequestBody  ):Response<UpdateCartResponse>


    @GET("Home/search_by_name")
    suspend fun searchByName(@Header("lang" ) lang:String , @Query("name") name:String) : Response<ProductByTypeResponse>

    @GET("Order/user_address")
    suspend fun showUserAddress(@Header("lang" ) lang:String , @Header("Authorization" ) Authorization:String) : Response<ShowUserAddressResponse>

    @FormUrlEncoded
    @POST("Order/order_address/{pro_id}")
    suspend fun orderAddressWithAddressId(@Header("lang" ) lang:String ,
                                          @Path("pro_id") pro_id:Int,
                                          @Field("address_id") address_id:Int,
                                          @Header("Authorization" ) Authorization:String): Response<CheckoutOrderAddressResponse>

    @FormUrlEncoded
    @POST("Order/order_address/{pro_id}")
    suspend fun orderAddressWithoutAddressId(@Header("lang" ) lang:String ,
                                             @Path("pro_id") pro_id:Int,
                                             @Header("Authorization" ) Authorization:String,
                                             @Field("lat") lat: Double,
                                             @Field("lng")lon:Double,
                                             @Field("address") address:String) : Response<CheckoutOrderAddressResponse>

    @FormUrlEncoded
    @POST("Order/add_payment_method/{pro_id}")
    suspend fun addPaymentMethod(@Header("lang" ) lang:String ,
                                 @Path("pro_id") id:Int ,
                                 @Header("Authorization" ) Authorization:String,
                                 @Field("payment_method") payment_method:Int ) :Response<PaymentMethodResponse>

    @FormUrlEncoded
    @POST("message/send_message")
    suspend fun sendMessageChat(@Header("lang" ) lang:String ,
                                @Header("Authorization" ) Authorization:String,@Field("message") message:String) : Response<BaseResponse>

    @GET("message/get_message")
    suspend fun getPerviousMessage(@Header("lang" ) lang:String , @Header("Authorization" ) Authorization:String):Response<PerviousChatResponse>

    @FormUrlEncoded
    @POST("rate_comment/save_my_rate/{pro_id}")
    suspend fun saveRate(@Header("lang" ) lang:String ,
                         @Path("pro_id") pro_id : Int ,
                         @Header("Authorization") Authorization:String ,
                         @Field("rate") rate:Int) : Response<BaseResponse>

    @FormUrlEncoded
    @POST("Auth_private/settings")
    suspend fun settingLang(
                            @Header("Authorization" ) Authorization:String,
                            @Field("lang") lang:String) : Response<SettingResponse>

    @FormUrlEncoded
    @POST("Auth_private/settings")
    suspend fun settingMessage(@Header("lang" ) lang:String ,
                               @Header("Authorization" ) Authorization:String,
                               @Field("message") message:Int) : Response<SettingResponse>

    @FormUrlEncoded
    @POST("Auth_private/settings")
    suspend fun settingNotification(@Header("lang" ) lang:String ,
                                    @Header("Authorization" ) Authorization:String,
                                    @Field("notification") notification:Int) : Response<SettingResponse>

    @FormUrlEncoded
    @POST("Auth_private/settings")
    suspend fun settingCurrency(@Header("lang" ) lang:String ,
                                @Header("Authorization" ) Authorization:String,
                                @Field("currency_id") currency_id:Int) : Response<SettingResponse>

    @POST("Auth_private/settings")
    suspend fun settingUser(@Header("lang" ) lang:String ,
                            @Header("Authorization" ) Authorization:String) : Response<SettingResponse>

    @GET("general/get_currencies")
    suspend fun getCurrencies(@Header("lang" ) lang:String):Response<CurrenciesResponse>

    @FormUrlEncoded
    @POST("Order/check_discount_code")
    suspend fun makeDiscount(@Header("lang" ) lang:String ,
                             @Header("Authorization") Authorization:String ,
                             @Field("code") code:Int) : Response<MakeDiscountResponse>

    @GET("Auth_private/get_notification")
    suspend fun getNotification(@Header("lang" ) lang:String , @Header("Authorization" ) Authorization:String):Response<NotificationResponse>


    @FormUrlEncoded
    @POST("Auth_private/add_address")
    suspend fun addSingleAddress(@Header("lang" ) lang:String ,
                                 @Header("Authorization" ) Authorization:String,
                                 @Field("lat") lat: Double,
                                 @Field("lng")lon:Double,
                                 @Field("address") address:String) : Response<SingleAddressResponse>

    @FormUrlEncoded
    @POST("Order/add_order_once")
    suspend fun addOrderOnce(@Header("lang" ) lang:String ,
                             @Header("Authorization" ) Authorization:String,
                             @Field("payment_method") payment_method:Int,
                             @Field("address_id") address_id:Int,
                             @Field("code_id") code_id:Int):Response<AddOrderOnceResponse>

    @GET("Order/my_order")
    suspend fun myOrders(@Header("lang" ) lang:String ,
                         @Header("Authorization" ) Authorization:String,@Query("page") page:Int) : Response<MyOrdersResponse>

    @GET("Order/single_order/{id}")
    suspend fun myOrdersProducts(@Header("lang" ) lang:String ,
                                 @Header("Authorization" ) Authorization:String,@Path("id") id:Int): Response<MyOrderProductsResponse>

    @GET("Auth_private/logout")
    suspend fun logoutFunction( @Header("lang" ) lang:String ,
                                @Header("Authorization") Authorization:String ): Response<BaseResponse>

}